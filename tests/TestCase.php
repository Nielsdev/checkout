<?php

namespace Tests;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use PDO;
use ProBiz\Includes\Language as LanguageIncludes;

class TestCase extends \Illuminate\Foundation\Testing\TestCase
{

    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../vendor/laravel/laravel/bootstrap/app.php';

        $app->register('Qup\Checkout\Providers\CheckoutProvider');
        $app->register('Tests\TestRoutesProvider');

        $app->loadEnvironmentFrom('/var/app/current/.env');
        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

        $this->initializeDatabase($app);
        $this->initializeProbiz();

        return $app;
    }

    /**
     * Sets up the Probiz library so that it knows which language and site id to use.
     * Note that the current values are hardcoded to use jumbo on the test database.
     */
    private function initializeProbiz()
    {
        DB::statement(DB::raw('SET probiz.site_ids = \'{1202033322553772035}\''));
        DB::statement(DB::raw('SET probiz.language_id = 1202033322939650049'));

        session()->put('Site_ID', '1202033322553772035');
        session()->put('Lang_ID', '1202033322939650049');
    }

    /**
     * Changes the default database connection parameters to work with the qup source
     * base.
     */
    private function initializeDatabase($app)
    {
        $app['config']->set('database.connections.pgsql.schema', 'jumbo');
        $app['config']->set('database.fetch', PDO::FETCH_ASSOC);
    }
}