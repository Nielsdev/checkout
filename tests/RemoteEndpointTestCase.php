<?php

namespace Tests;

use GuzzleHttp\Client;
use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\V2CheckoutEndpointsDriver;

abstract class RemoteEndpointTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Creates a pre-configured guzzle client that can be used to
     * execute remote endpoint tests. The target host can be
     * configured through the TESTS_TARGET_HOST environment variable.
     *
     * By default the client will target localhost:8080
     *
     * @return Client
     */
    public function makeClient()
    {
        $baseUri = 'http://' . $this->getTargetHost() . '/v1/';

        $client = new Client([
            'base_uri' => $baseUri,
            'http_errors' => false
        ]);

        return $client;
    }

    /**
     * Creates a Application Driver specifically for checkout related endpoints
     *
     * @return CheckoutEndpointsDriver
     */
    public function makeCheckoutEndpointsDriver()
    {
        return new CheckoutEndpointsDriver($this->makeClient());
    }

    /**
     * Creates a Application driver for version 2 of checkout related endpoints,
     * builds on the version 1 driver, and replaces certain methods with new endpoints.
     */
    public function makeV2CheckoutEndpointsDriver()
    {
        return new V2CheckoutEndpointsDriver($this->makeClient());
    }

    /**
     * Retrieve target host from environment variable
     *
     * @return mixed
     */
    private function getTargetHost()
    {
        return env('TESTS_TARGET_HOST', 'localhost:8080');
    }

    /**
     * Creates a session token to use for session based endpoints.
     *
     * @return \string[]
     */
    public function getSessionToken($response)
    {
        return $response->getHeader('Authorization');
    }

    public function assertContentType($expected, $response)
    {
        $this->assertEquals($expected, $response->getHeader('Content-Type')[0]);
    }

}