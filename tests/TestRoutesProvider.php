<?php

namespace Tests;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Routing\Router;

class TestRoutesProvider extends RouteServiceProvider
{
    protected $namespace = 'Qup\Checkout\Controllers';

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require __DIR__ . '/../config/routes.php';
        });
    }
}