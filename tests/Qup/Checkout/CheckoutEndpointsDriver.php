<?php

namespace Tests\Qup\Checkout;

/**
 * Application Driver for calling endpoints related to checkout
 */
class CheckoutEndpointsDriver
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var string
     */
    private $sessionToken;

    /**
     * CheckoutEndpointsDriver constructor.
     *
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
        $this->sessionToken = null;
    }

    protected function request($method, $url, $options = [])
    {
        $optionsWithSession = $options;
        if ($this->sessionToken) {
            $optionsWithSession['headers'] = [
                'Authorization' => $this->sessionToken
            ];
        }

        $response = $this->client->request($method, $url, $optionsWithSession);

        if (!empty($response->getHeader('Authorization'))) {
            $newToken = $response->getHeader('Authorization')[0];

            if ($newToken != null && $newToken != $this->sessionToken) {
                $this->sessionToken = $newToken;
            }
        }

        return $response;
    }

    /**
     * @return string
     */
    public function getSessionToken()
    {
        return substr($this->sessionToken, 7);
    }

    /**
     * Creates a order by putting the given tickets into the shopping cart.
     *
     * @param $tickets
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function createOrder($tickets)
    {
        return $this->request('PUT', 'carts/current/cart', [
            'json' => [
                'tickets' => $tickets,
                'insurance' => false
            ]
        ]);
    }

    /**
     * Put vouchers in the cart
     *
     * @param $vouchers
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function putVouchers($vouchers)
    {
        return $this->request('PUT', 'carts/current/vouchers', [
            'json' => $vouchers
        ]);
    }

    /**
     * Get all the vouchers in the cart
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getVouchers()
    {
        return $this->request('GET', 'carts/current/vouchers');
    }

    /**
     * Retrieves the status of the current order.
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getOrderStatus()
    {
        return $this->request('GET', 'carts/current/order/status');
    }

    /**
     * Retrieves the receipt of the current order.
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getReceipt()
    {
        return $this->request('GET', 'carts/current/receipt');
    }

    /**
     * Puts the given user data on the current order.
     *
     * @param $userData
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function putUserData($userData)
    {
        return $this->request('PUT', 'carts/current/userdata', [
            'json' => $userData
        ]);
    }

    /**
     * Retrieves the user data on the current order
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getUserData()
    {
        return $this->request('GET', 'carts/current/userdata');
    }

    /**
     * Sets the payment method on the current order.
     *
     * @param integer $paymentMethod The ID of the payment method.
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function putPaymentMethod($paymentMethod)
    {
        return $this->request('PUT', 'carts/current/paymentmethod', [
            'json' => [
                'type_id' => $paymentMethod
            ]
        ]);
    }

    /**
     * Puts the condition.
     *
     * @param $conditionId
     * @param $accepted
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function putCondition($conditionId, $accepted)
    {
        return $this->request('PUT', 'carts/current/conditions/' . $conditionId, [
            'json' => [
                'condition' => $accepted
            ]
        ]);
    }

    /**
     * Retrieves the conditions.
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getConditions()
    {
        return $this->request('GET', 'carts/current/conditions');
    }

    /**
     * Puts the subscription.
     *
     * @param $subscriptionId
     * @param $subscribed boolean
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function putSubscription($subscriptionId, $subscribed)
    {
        return $this->request('PUT', 'carts/current/subscriptions/' . $subscriptionId, [
            'json' => [
                'subscription' => $subscribed
            ]
        ]);
    }

    /**
     * Retrieves the subscriptions.
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getSubscriptions()
    {
        return $this->request('GET', 'carts/current/subscriptions');
    }

    /**
     * Retrieves the payment method set on the current order
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getPaymentMethod()
    {
        return $this->request('GET', 'carts/current/paymentmethod');
    }

    /**
     * Retrieves a list of all payment methods the user can choose.
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getAvailablePaymentMethods()
    {
        return $this->request('GET', 'carts/current/paymentmethods');
    }

    /**
     * Initiates the payment using the previously set payment method.
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function confirmPayment()
    {
        return $this->request('POST', 'carts/current/paymentmethod/confirm');
    }

    public function postOgoneResponse($response)
    {
        return $this->request('POST', 'ogone/result', array(
           'form_params' => $response
        ));
    }
}