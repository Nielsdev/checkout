<?php

namespace Tests\Qup\Checkout;

/**
 * Sample data based on test database, can be used for the remote endpoint tests.
 *
 * @package Tests\Qup\Checkout
 */
class SampleData
{
    public static $idealPaymentMethod = 110;
    public static $conditionId = 1202037856848054527;
    public static $subscriptionId = 1202037710173243627;

    public static $unusedVoucher = '0756559DC0';
    public static $usedVoucher = '35B451A6C0';

    /**
     * Example list of tickets, containing only a single ticket.
     *
     * @return array
     */
    public static function tickets($id = '1401419664294152330')
    {
        return [
            ['id' => $id, 'quantity' => 1]
        ];
    }

    /**
     * Example user data
     *
     * @return object
     */
    public static function user()
    {
        return [
            "email" => "test@acme.org",
            "lastname" => "Doe",
            "firstname" => "John",
            "title" => "Mr.",
            "address" => [
                "city" => "Acme City",
                "housenumber" => "1",
                "postalcode" => "1234AB",
                "street" => "Acme Street",
                "streetsuffix" => ""
            ]
        ];
    }
}