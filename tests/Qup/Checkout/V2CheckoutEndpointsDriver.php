<?php

namespace Tests\Qup\Checkout;

class V2CheckoutEndpointsDriver extends CheckoutEndpointsDriver
{
    public function putUserData($userData)
    {
        return $this->request('PUT', '/v2/carts/current/userdata', [
            'json' => $userData
        ]);
    }

    public function getUserData()
    {
        return $this->request('GET', '/v2/carts/current/userdata');
    }


}