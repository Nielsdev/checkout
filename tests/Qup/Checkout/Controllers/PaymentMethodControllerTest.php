<?php

namespace Tests\Qup\Checkout\Controllers;

use GuzzleHttp\Client;
use Mockery\CountValidator\Exception;
use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\SampleData;
use Tests\RemoteEndpointTestCase;

class PaymentMethodControllerTest extends RemoteEndpointTestCase
{
    /**
     * @var CheckoutEndpointsDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = $this->makeCheckoutEndpointsDriver();
    }

    public function testGetEmptyPaymentMethod()
    {
        $firstPaymentMethod = $this->getFirstPaymentMethodId();
        $response = $this->driver->getPaymentMethod();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertEquals($firstPaymentMethod, $body->data->type_id);
    }

    public function testPutPaymentMethod()
    {
        $paymentMethod = $this->getSecondPaymentMethodId();

        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());
        $response = $this->driver->putPaymentMethod($paymentMethod);

        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->driver->getPaymentMethod();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertEquals($paymentMethod, $body->data->type_id);
    }

    public function testPutNonExistingPaymentMethod()
    {
        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());

        $response = $this->driver->putPaymentMethod(-1);

        $this->assertEquals(418, $response->getStatusCode());

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertFalse($body->success);
        $this->assertEquals(500, $body->internal_code);
    }

    public function testInitiatePaymentWithNonExistingPaymentMethod()
    {
        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());
        $this->driver->putPaymentMethod(-1);

        $response = $this->driver->confirmPayment();

        $this->assertEquals(418, $response->getStatusCode());

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertFalse($body->success);
        $this->assertEquals(500, $body->internal_code);
    }

    public function testInitiatePayment()
    {
        $paymentMethod = $this->getSecondPaymentMethodId();

        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());
        $this->driver->putPaymentMethod($paymentMethod);

        $response = $this->driver->confirmPayment();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data->url);
    }

    public function testInitiatePaymentWithoutSettingPayment()
    {
        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());

        $response = $this->driver->confirmPayment();

        $this->assertEquals(418, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertFalse($body->success);
        $this->assertEquals(500, $body->internal_code);
    }

    private function getFirstPaymentMethodId()
    {
        $response = $this->driver->getAvailablePaymentMethods();

        if ($response->getStatusCode() == 200) {
            $body = \GuzzleHttp\json_decode($response->getBody());

            return $body->data[0]->type_id;
        } else {
            throw new Exception('No payment method received...');
        }
    }

    private function getSecondPaymentMethodId()
    {
        $response = $this->driver->getAvailablePaymentMethods();

        if ($response->getStatusCode() == 200) {
            $body = \GuzzleHttp\json_decode($response->getBody());

            $typeIds = array();
            foreach ($body->data as $paymentMethod) {
                if (!in_array($paymentMethod->type_id, $typeIds)) {
                    array_push($typeIds, $paymentMethod->type_id);
                }
            }

            return $typeIds[1];
        } else {
            throw new Exception('No payment method received...');
        }
    }
}