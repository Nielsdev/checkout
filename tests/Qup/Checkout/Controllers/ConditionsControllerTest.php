<?php

namespace Tests\Qup\Checkout\Controllers;

use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\SampleData;
use Tests\RemoteEndpointTestCase;

class ConditionsControllerTest extends RemoteEndpointTestCase
{
    /**
     * @var CheckoutEndpointsDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = $this->makeCheckoutEndpointsDriver();
    }

    public function testGetConditions()
    {
        $response = $this->driver->getConditions();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertNotEmpty($body->data);
        $this->assertHasConditions($body);
    }

    public function testPutConditions()
    {
        $response = $this->driver->putCondition(SampleData::$conditionId, true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());
        $this->assertTrue($body->success);

        $response = $this->driver->getConditions();
        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertHasConditions($body);

        foreach ($body->data as $condition) {
            if ($condition->id == SampleData::$conditionId) {
                $this->assertTrue($condition->accepted);
            }
        }
    }

    private function assertHasConditions($body)
    {
        $data = $body->data;
        $conditionIds = [];
        foreach ($data as $condition) {
            $conditionIds[] = $condition->id;
        }

        $this->assertContains(SampleData::$conditionId, $conditionIds);
    }
}