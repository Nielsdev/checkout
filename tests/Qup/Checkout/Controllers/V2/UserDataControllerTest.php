<?php

namespace Tests\Qup\Checkout\Controllers\V2;

use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\SampleData;
use Tests\Qup\Checkout\V2CheckoutEndpointsDriver;
use Tests\RemoteEndpointTestCase;

class UserDataControllerTest extends RemoteEndpointTestCase
{
    /**
     * @var V2CheckoutEndpointsDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = $this->makeV2CheckoutEndpointsDriver();
    }

    public function testGetUserData()
    {
        $response = $this->driver->getUserData();

        $this->assertSuccessResponse($response);

        $body = $this->parseResponse($response);

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data);
        $this->assertNotNull($body->data->userData);
        $this->assertNotEmpty($body->data->subscriptions);
        $this->assertEmpty($body->data->conditions);
    }

    public function testPuttingUserData()
    {
        $user = SampleData::user();

        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->putUserData($user);

        $this->assertSuccessResponse($response);

        $body = $this->parseResponse($response);

        $this->assertTrue($body->success);
        $this->assertSameUserData($user, $body);
        $this->assertEmpty($body->data->conditions);
        $this->assertNotEmpty($body->data->subscriptions);
    }

    public function testPuttingUserDataAndConditions()
    {
        $user = SampleData::user();

        $user['conditions'] = [
            ['id' => SampleData::$conditionId, 'accept' => true]
        ];

        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->putUserData($user);

        $this->assertSuccessResponse($response);

        $body = $this->parseResponse($response);

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data);
        $this->assertSameUserData($user, $body);
        $this->assertHasConditions($body);
    }

    public function testPuttingUserDataAndSubscriptions()
    {
        $firstSubscription = $this->getSubscriptions()[0];

        $user = SampleData::user();
        $user['subscriptions'] = [
            ['id' => $firstSubscription, 'subscribe' => false]
        ];

        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->putUserData($user);

        $this->assertSuccessResponse($response);

        $body = $this->parseResponse($response);

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data);
        $this->assertSameUserData($user, $body);
        $this->assertHasSubscription($firstSubscription, $body);
    }

    public function testPuttingUserDataSubscriptionsAndConditions()
    {
        $defaultSubscriptions = $this->getSubscriptions();
        $firstSubscription = $defaultSubscriptions[0];

        $user = SampleData::user();
        $user['conditions'] = [
            ['id' => SampleData::$conditionId, 'accept' => true]
        ];
        $user['subscriptions'] = [
            ['id' => $firstSubscription, 'subscribe' => false]
        ];

        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->putUserData(
            $user
        );

        $this->assertSuccessResponse($response);

        $body = $this->parseResponse($response);

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data);
        $this->assertSameUserData($user, $body);
        $this->assertHasConditions($body);
        $this->assertHasSubscription($firstSubscription, $body);
    }

    public function testPuttingUserDataShouldPersist()
    {
        $defaultSubscriptions = $this->getSubscriptions();
        $firstSubscription = $defaultSubscriptions[0];

        $user = SampleData::user();
        $user['conditions'] = [
            ['id' => SampleData::$conditionId, 'accept' => true]
        ];
        $user['subscriptions'] = [
            ['id' => $firstSubscription, 'subscribe' => false]
        ];

        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->putUserData(
            $user
        );

        $this->assertSuccessResponse($response);

        $body = $this->parseResponse($response);

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data);
        $this->assertSameUserData($user, $body);
        $this->assertHasConditions($body);
        $this->assertHasSubscription($firstSubscription, $body);

        $response = $this->driver->getUserData();

        $this->assertSuccessResponse($response);

        $body = $this->parseResponse($response);

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data);
        $this->assertSameUserData($user, $body);
        $this->assertHasConditions($body);
        $this->assertHasSubscription($firstSubscription, $body);
    }

    private function assertSameUserData($expected, $responseBody)
    {
        $userData = $responseBody->data->userData;
        $expected = (object) $expected;
        $expected->address = (object) $expected->address;

        $this->assertEquals($expected->email, $userData->email);
        $this->assertEquals($expected->lastname, $userData->lastname);
        $this->assertEquals($expected->firstname, $userData->firstname);
        $this->assertEquals($expected->title, $userData->title);
        $this->assertEquals($expected->address->city, $userData->address->city);
        $this->assertEquals($expected->address->housenumber, $userData->address->housenumber);
        $this->assertEquals($expected->address->postalcode, $userData->address->postalcode);
        $this->assertEquals($expected->address->street, $userData->address->street);
        $this->assertEquals($expected->address->streetsuffix, $userData->address->streetsuffix);
    }

    private function assertHasConditions($responseBody)
    {
        $conditions = $responseBody->data->conditions;

        $conditionIds = [];
        foreach ($conditions as $conditionId => $accepted) {
            $conditionIds[] = $conditionId;
        }

        $this->assertContains(SampleData::$conditionId, $conditionIds);
    }

    private function getSubscriptions()
    {
        $userData = $this->driver->getUserData();
        $body = \GuzzleHttp\json_decode($userData->getBody());

        return $body->data->subscriptions;
    }

    private function assertHasSubscription($exptectedSubscription, $responseBody)
    {
        $actualSubscriptions = $responseBody->data->subscriptions;
        $this->assertContains($exptectedSubscription, $actualSubscriptions);
    }

    private function assertSuccessResponse($response)
    {
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);
    }

    private function parseResponse($response)
    {
        $body = \GuzzleHttp\json_decode($response->getBody());
        return $body;
    }

}