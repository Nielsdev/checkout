<?php

namespace Tests\Qup\Checkout\Controllers;

use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\SampleData;
use Tests\RemoteEndpointTestCase;

class CheckoutControllerTest extends RemoteEndpointTestCase
{
    /**
     * @var CheckoutEndpointsDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = $this->makeCheckoutEndpointsDriver();
    }

    public function testNewOrderStatus()
    {
        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->getOrderStatus();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertEquals('new', $body->data->status);
    }

    public function testOpenOrderStatus()
    {
        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());
        $this->driver->putPaymentMethod(SampleData::$idealPaymentMethod);
        $this->driver->confirmPayment();

        $orderStatusResponse = $this->driver->getOrderStatus();

        $this->assertEquals(200, $orderStatusResponse->getStatusCode());
        $this->assertContentType('application/json', $orderStatusResponse);

        $body = \GuzzleHttp\json_decode($orderStatusResponse->getBody());

        $this->assertTrue($body->success);
        $this->assertEquals('open', $body->data->status);
    }

    public function testRetrieveReceipt()
    {
        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());
        $this->driver->putPaymentMethod(SampleData::$idealPaymentMethod);

        $response = $this->driver->getReceipt();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertNotNull($body->data);

        $this->assertEquals(SampleData::tickets()[0]['id'], $body->data->products[0]->id);
        $this->assertEquals(SampleData::tickets()[0]['quantity'], $body->data->products[0]->quantity);

        $this->assertEquals(SampleData::user()['email'], $body->data->email);

        $this->assertEquals(SampleData::$idealPaymentMethod, $body->data->paymentmethod);
    }

    public function testPutUnusedVoucher()
    {
        $this->markTestSkipped('Test method cannot be executed twice, because of dependence on unused vouchers');

        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->putVouchers([SampleData::$unusedVoucher]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $voucherResult = $body->data[0];

        $this->assertEquals(SampleData::$unusedVoucher, $voucherResult->voucher);
        $this->assertEquals("valid", $voucherResult->status);
        $this->assertTrue($voucherResult->processed);
    }

    public function testPutUsedVoucher()
    {
        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->putVouchers([SampleData::$usedVoucher]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $voucherResult = $body->data[0];

        $this->assertEquals(SampleData::$usedVoucher, $voucherResult->voucher);
        $this->assertEquals("invalid", $voucherResult->status);
        $this->assertFalse($voucherResult->processed);
    }

    public function testGetEmptyVouchers()
    {
        $this->driver->createOrder(SampleData::tickets());
        $response = $this->driver->getVouchers();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertEmpty($body->data);
    }

    public function testPutUnusedVoucherAndRetrieve()
    {
        $this->markTestSkipped('Test method cannot be executed twice, because of dependence on unused vouchers');

        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putVouchers([SampleData::$unusedVoucher]);
        $response = $this->driver->getVouchers();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertNotEmpty($body->data);
        $this->assertContains(SampleData::$unusedVoucher, $body->data);
    }

}