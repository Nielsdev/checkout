<?php

namespace Tests\Qup\Checkout\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Tests\RemoteEndpointTestCase;

class CartCatalogControllerTestCase extends RemoteEndpointTestCase
{
    public function testCartCatalogueShouldReplyWithCatalog()
    {
        $response = $this->callEndpoint("GET", "cartcatalog/product/1202097027194817173");
        $this->assertSuccessResponse($response);
    }

    public function testCartCatelogWithDateShouldReplyWithCatalog()
    {
        $response = $this->callEndpoint("GET", "cartcatalog/product/1202097027194817173/date/2016-01-01");
        $this->assertSuccessResponse($response);
    }

    public function testCartCatalogWithNonExistingProduct()
    {
        $response = $this->callEndpoint("GET", "cartcatalog/product/1");
        $this->assertEmptyResponse($response);
    }

    public function testCartCatalogWithNonValidID()
    {
        $response = $this->callEndpoint("GET", "cartcatalog/product/non-valid-id");
        $this->assertErrorResponse($response);
    }

    /**
     * @param ResponseInterface $response
     */
    private function assertErrorResponse($response)
    {
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @param ResponseInterface $response
     */
    private function assertEmptyResponse($response)
    {
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $response = $this->parseJson($response);

        $this->assertTrue($response->success);
        $this->assertEmpty($response->data->rateGroups);
        $this->assertEmpty($response->data->relatedProducts);
    }

    /**
     * @param ResponseInterface $response
     */
    private function assertSuccessResponse($response)
    {
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $result = $this->parseJson($response);

        $this->assertTrue($result->success);
        $this->assertNotEmpty($result->data->rateGroups);
        $this->assertNotEmpty($result->data->relatedProducts);
    }

    /**
     * @param $method
     * @param $url
     * @return ResponseInterface
     */
    private function callEndpoint($method, $url)
    {
        $client = $this->makeClient();
        return $client->request($method, $url);
    }

    /**
     * @param ResponseInterface $response
     * @return mixed
     */
    private function parseJson($response)
    {
        return \GuzzleHttp\json_decode($response->getBody());
    }
}