<?php

namespace Tests\Qup\Checkout\Controllers;

use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\SampleData;
use Tests\RemoteEndpointTestCase;

class SubscriptionsControllerTest extends RemoteEndpointTestCase
{
    /**
     * @var CheckoutEndpointsDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = $this->makeCheckoutEndpointsDriver();
    }

    public function testGetSubscriptions()
    {
        $response = $this->driver->getSubscriptions();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertNotEmpty($body->data);
        $this->assertHasSubscriptions($body);
    }

    public function testPutSubscriptions()
    {
        $response = $this->driver->putSubscription(SampleData::$subscriptionId, true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());
        $this->assertTrue($body->success);

        $response = $this->driver->getSubscriptions();
        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertHasSubscriptions($body);

        foreach ($body->data as $subscription) {
            if ($subscription->id == SampleData::$subscriptionId) {
                $this->assertTrue($subscription->subscribed);
            }
        }
    }

    private function assertHasSubscriptions($body)
    {
        $data = $body->data;
        $subscriptionIds = [];
        foreach ($data as $subscription) {
            $subscriptionIds[] = $subscription->id;
        }

        $this->assertContains(SampleData::$subscriptionId, $subscriptionIds);
    }
}