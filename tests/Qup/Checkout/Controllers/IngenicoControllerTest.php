<?php

namespace Tests\Qup\Checkout\Controllers;

use Namshi\JOSE\JWS;
use ProBiz\Payment\PaymentPlatformOgone;
use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\SampleData;
use Tests\RemoteEndpointTestCase;

class IngenicoControllerTest extends RemoteEndpointTestCase
{
    /**
     * @var CheckoutEndpointsDriver
     */
    private $driver;

    private $hashKey;

    private $environment;

    private $allowedHashParameters = [
        'AAVADDRESS',
        'AAVCHECK',
        'AAVZIP',
        'ACCEPTANCE',
        'ALIAS',
        'AMOUNT',
        'BIN',
        'BRAND',
        'CARDNO',
        'CCCTY',
        'CN',
        'COMPLUS',
        'CREATION_STATUS',
        'CURRENCY',
        'CVCCHECK',
        'DCC_COMMPERCENTAGE',
        'DCC_CONVAMOUNT',
        'DCC_CONVCCY',
        'DCC_EXCHRATE',
        'DCC_EXCHRATESOURCE',
        'DCC_EXCHRATETS',
        'DCC_INDICATOR',
        'DCC_MARGINPERCENTAGE',
        'DCC_VALIDHOURS',
        'DIGESTCARDNO',
        'ECI',
        'ED',
        'ENCCARDNO',
        'FXAMOUNT',
        'FXCURRENCY',
        'IP',
        'IPCTY',
        'NBREMAILUSAGE',
        'NBRIPUSAGE',
        'NBRIPUSAGE_ALLTX',
        'NBRUSAGE',
        'NCERROR',
        'NCERRORCARDNO',
        'NCERRORCN',
        'NCERRORCVC',
        'NCERRORED',
        'ORDERID',
        'PAYID',
        'PM',
        'SCO_CATEGORY',
        'SCORING',
        'STATUS',
        'SUBBRAND',
        'SUBSCRIPTION_ID',
        'TRXDATE',
        'VC'
    ];


    protected function setUp()
    {
        $this->driver = $this->makeCheckoutEndpointsDriver();
        $this->hashKey = env('TESTS_OGONE_HASH_KEY', '');
    }

    public function testSuccessfulTransactionResponse()
    {
        $order = $this->createSampleOrder();

        $response = $this->driver->postOgoneResponse(
            $this->createSuccessResponse($order)
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $response = $this->driver->getOrderStatus();
        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertObjectHasAttribute('data', $body);
        $this->assertObjectHasAttribute('status', $body->data);
        $this->assertEquals('succeeded', $body->data->status);
    }

    public function testUnsuccessfulTransactionResponse()
    {
        $order = $this->createSampleOrder();

        $this->driver->postOgoneResponse(
            $this->createFailureResponse($order)
        );

        $response = $this->driver->getOrderStatus();
        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertObjectHasAttribute('data', $body);
        $this->assertObjectHasAttribute('status', $body->data);
        // we keep the transaction open even though the payment
        // provider indicated that the user refused the payment
        $this->assertEquals('open', $body->data->status);
    }

    public function testIncompleteTransactionResponse()
    {
        $order = $this->createSampleOrder();

        $response = $this->driver->postOgoneResponse(
            $this->createIncompleteResponse($order)
        );

        $this->assertEquals(418, $response->getStatusCode());

        $response = $this->driver->getOrderStatus();
        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertObjectHasAttribute('data', $body);
        $this->assertObjectHasAttribute('status', $body->data);
        // we keep the transaction open even though the payment
        // provider indicated that the user refused the payment
        $this->assertEquals('failed', $body->data->status);
    }

    private function createSampleOrder()
    {
        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData(SampleData::user());
        $this->driver->putPaymentMethod(SampleData::$idealPaymentMethod);
        $this->driver->confirmPayment();

        $receiptResponse = $this->driver->getReceipt();

        $body = \GuzzleHttp\json_decode($receiptResponse->getBody());
        return $body->data;
    }

    private function createSuccessResponse($order)
    {
        $response = array(
            'orderID' => $this->getOrderId(),
            'amount'  => $order->totals->grandtotal,
            'PM'      => 'pm',
            'BRAND'   => 'brand',
            'PAYID'   => 'id',
            'STATUS'  => PaymentPlatformOgone::STATUS_PAID
        );

        $response['SHASIGN'] = $this->createHash($response);

        return $response;
    }

    private function createFailureResponse($order)
    {
        $response = array(
            'orderID' => $this->getOrderId(),
            'amount'  => $order->totals->grandtotal,
            'PM'      => 'pm',
            'BRAND'   => 'brand',
            'PAYID'   => 'id',
            'STATUS'  => PaymentPlatformOgone::STATUS_REFUSED
        );

        $response['SHASIGN'] = $this->createHash($response);

        return $response;
    }

    private function createIncompleteResponse($order)
    {
        $response = array(
            'orderID' => $this->getOrderId(),
            'amount'  => $order->totals->grandtotal - 1,
            'PM'      => 'pm',
            'BRAND'   => 'brand',
            'PAYID'   => 'id',
            'STATUS'  => PaymentPlatformOgone::STATUS_PAID
        );

        $response['SHASIGN'] = $this->createHash($response);

        return $response;
    }

    private function createHash($parameters)
    {
        // array keys to uppercase
        $parameters = array_change_key_case($parameters, CASE_UPPER);
        // order parameters aphabetically by key
        ksort($parameters);

        $sString = '';
        foreach ($parameters as $mKey => $mValue) {
            if (!in_array($mKey, $this->allowedHashParameters)) {
                continue;
            }
            if (strlen($mValue) == 0) {
                // only non-empty params should be hashed
                continue;
            }
            $sString .= strtoupper($mKey) . '=' . $mValue . $this->hashKey;
        }
        return strtoupper(hash('sha512', $sString));
    }

    private function getOrderId()
    {
        $token = JWS::load($this->driver->getSessionToken());
        return $token->getPayload()['activeTransactionId'];
    }
}