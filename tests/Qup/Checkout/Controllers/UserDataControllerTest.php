<?php

namespace Tests\Qup\Checkout\Controllers;

use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\Qup\Checkout\SampleData;
use Tests\RemoteEndpointTestCase;

class UserDataControllerTestCase extends RemoteEndpointTestCase
{
    /**
     * @var CheckoutEndpointsDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = $this->makeCheckoutEndpointsDriver();
    }

    public function testGetEmptyUserDataOnCart()
    {
        $response = $this->driver->getUserData();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertEmptyUserData($body);
    }

    public function testPutUserData()
    {
        $sampleUserData = (object)[
            "email" => "test@acme.org",
            "lastname" => "Doe",
            "firstname" => "John",
            "title" => "Mr.",
            "address" => (object)[
                "city" => "Acme City",
                "housenumber" => "1",
                "postalcode" => "1234AB",
                "street" => "Acme Street",
                "streetsuffix" => ""
            ]
        ];

        $response = $this->driver->putUserData($sampleUserData);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertSameUserData($sampleUserData, $body->data);
    }

    public function testPutAndRetrieveUserData()
    {
        $sampleUserData = (object)[
            "email" => "test@acme.org",
            "lastname" => "Doe",
            "firstname" => "John",
            "title" => "Mr.",
            "address" => (object)[
                "city" => "Acme City",
                "housenumber" => "1",
                "postalcode" => "1234AB",
                "street" => "Acme Street",
                "streetsuffix" => ""
            ]
        ];

        $this->driver->createOrder(SampleData::tickets());
        $this->driver->putUserData($sampleUserData);
        $response = $this->driver->getUserData();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $body = \GuzzleHttp\json_decode($response->getBody());

        $this->assertTrue($body->success);
        $this->assertSameUserData($sampleUserData, $body->data);
    }

    /**
     * @param Response $response
     */
    private function assertEmptyUserData($body)
    {
        $this->assertNull($body->data->email);
        $this->assertNull($body->data->title);
        $this->assertNull($body->data->firstname);
        $this->assertNull($body->data->suffix);
        $this->assertNull($body->data->lastname);
        $this->assertNull($body->data->fullname);
        $this->assertNull($body->data->phone);

        // Address information
        $this->assertNull($body->data->address->street);
        $this->assertNull($body->data->address->streetsuffix);
        $this->assertNull($body->data->address->housenumber);
        $this->assertNull($body->data->address->housenumbersuffix);
        $this->assertNull($body->data->address->postalcode);
        $this->assertNull($body->data->address->city);
        $this->assertEmpty($body->data->address->country);
    }

    private function assertSameUserData($expected, $actual)
    {
        $expected = (object) $expected;
        $expected->address = (object) $expected->address;

        $this->assertEquals($expected->email, $actual->email);
        $this->assertEquals($expected->lastname, $actual->lastname);
        $this->assertEquals($expected->firstname, $actual->firstname);
        $this->assertEquals($expected->title, $actual->title);
        $this->assertEquals($expected->address->city, $actual->address->city);
        $this->assertEquals($expected->address->housenumber, $actual->address->housenumber);
        $this->assertEquals($expected->address->postalcode, $actual->address->postalcode);
        $this->assertEquals($expected->address->street, $actual->address->street);
        $this->assertEquals($expected->address->streetsuffix, $actual->address->streetsuffix);
    }
}