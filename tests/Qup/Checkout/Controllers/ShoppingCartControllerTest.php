<?php

namespace Tests\Qup\Checkout\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use PHPUnit_Framework_TestCase;
use Tests\Qup\Checkout\CheckoutEndpointsDriver;
use Tests\RemoteEndpointTestCase;

class ShoppingCartControllerTestCase extends RemoteEndpointTestCase
{
    /**
     * @var CheckoutEndpointsDriver
     */
    private $driver;

    protected function setUp()
    {
        $this->driver = $this->makeCheckoutEndpointsDriver();
    }

    public function testAddProductToShoppingCart()
    {
        $productId = '1401419664294152330';

        $response = $this->driver->createOrder([
                ['id' => $productId, 'quantity' => 1]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $result = \GuzzleHttp\json_decode($response->getBody());

        $this->assertEquals(true, $result->success);
        $this->assertNotEmpty($result->data->products);
        $this->assertEquals($productId, $result->data->products[0]->id);
    }

    public function testAddMultiQuantityProductToShoppingCart()
    {
        $productId = '1401419664294152330';
        $productQuantity = 5;

        $response = $this->driver->createOrder([
            ['id' => $productId, 'quantity' => $productQuantity]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $result = \GuzzleHttp\json_decode($response->getBody());

        $this->assertEquals(true, $result->success);
        $this->assertNotEmpty($result->data->products);
        $this->assertCount(1, $result->data->products);
        $this->assertEquals($productId, $result->data->products[0]->id);
        $this->assertEquals($productQuantity, $result->data->products[0]->quantity);
    }

    public function testAddMultipleProductsToCart()
    {
        $firstProduct = '1401419664294152330';
        $secondProduct = '1403574211225586146';

        $response = $this->driver->createOrder([
            ['id' => $firstProduct, 'quantity' => 1],
            ['id' => $secondProduct, 'quantity' => 1]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $result = \GuzzleHttp\json_decode($response->getBody());

        $this->assertEquals(true, $result->success);
        $this->assertNotEmpty($result->data->products);
        $this->assertCount(2, $result->data->products);
        $this->assertEquals($firstProduct, $result->data->products[0]->id);
        $this->assertEquals(1, $result->data->products[0]->quantity);
    }

    public function testAddNonExistingProductToShoppingCart()
    {
        $response = $this->driver->createOrder([
            ['id' => '1', 'quantity' => 1]
        ]);

        $this->assertEquals(418, $response->getStatusCode());
        $this->assertContentType('application/json', $response);

        $result = \GuzzleHttp\json_decode($response->getBody());

        $this->assertEquals(false, $result->success);
        $this->assertEquals(500, $result->internal_code);
    }
}