<?php

namespace Qup\Checkout\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Email
 * @package Qup\Checkout\Models
 */
class Email extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['site_id', 'email', 'origin_id', 'bounce_count', 'options'];

    /**
     * Get the subscriptions associated with this email
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subscriptions()
    {
        return $this->belongsToMany(\Qup\Checkout\Models\Subscription::class)->withTimestamps();
    }
}