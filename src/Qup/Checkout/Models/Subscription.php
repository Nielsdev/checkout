<?php

namespace Qup\Checkout\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscription
 * @package Qup\Checkout\Models
 */
class Subscription extends Model
{
    /**
     * Get the emails subscribed to this subscription
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function email()
    {
        return $this->belongsToMany(\Qup\Checkout\Models\Email::class)->withTimestamps();
    }
}