<?php

namespace Qup\Checkout\Models;

use Illuminate\Database\Eloquent\Model;

class DateTimeRankSubProduct extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * shopLocationMainProduct
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shopLocationMainProduct()
    {
        return $this->belongsTo(\Qup\Checkout\Models\ShopLocationMainProduct::class);
    }
}