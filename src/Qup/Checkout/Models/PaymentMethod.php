<?php

namespace Qup\Checkout\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['method', 'psp', 'type', 'parent_method', 'title', 'name', 'abbreviation', 'external_id', 'external_type', 'options'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}