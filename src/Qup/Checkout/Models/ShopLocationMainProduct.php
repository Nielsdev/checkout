<?php

namespace Qup\Checkout\Models;

use Illuminate\Database\Eloquent\Model;

class ShopLocationMainProduct extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Returns dateTimeRankSubProducts 
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dateTimeRankSubProducts()
    {
        return $this->hasMany(\Qup\Checkout\Models\DateTimeRankSubProduct::class);
    }

    /**
     * Returns relatedProducts
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function relatedProducts()
    {
        return $this->belongsToMany(
            \Qup\Checkout\Models\ShopLocationMainProduct::class,
            'shop_location_main_product_shop_location_main_product',
            'shop_location_main_product_id',
            'related_product_id'
        )->withPivot('options');
    }
}