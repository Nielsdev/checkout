<?php

namespace Qup\Checkout\Models;

use ProBiz\Core\Collection\ObjectCollection;
use ProBiz\Core\NowObject;
use ProBiz\Checkout\CheckoutSubscription as ProBizCheckoutSubscription;

class CheckoutSubscriptions
{
    use NowObject;

    /**
     * @var integer
     */
    private $siteId = null;

    /**
     * @var array
     */
    private $actionIds = [];

    /**
     * @var array
     */
    private $shopIds = [];

    /**
     * @var array
     */
    private $locationIds = [];

    /**
     * @var array
     */
    private $shopLocationMainProductIds = [];

    /**
     * @var boolean
     */
    private $changedIds = true;

    /**
     * @var ObjectCollection
     */
    public $available;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * Initialize member objects
     */
    private function initialize()
    {
        $this->available = $this->createSubscriptionCollection();
    }

    /**
     * Creates and returns an objectcollection of CheckoutSubscription objects
     * @return \ProBiz\Core\Collection\ObjectCollection of \ProBiz\Checkout\CheckoutSubscription
     */
    private function createSubscriptionCollection()
    {
        $oCollection = new ObjectCollection(ProBizCheckoutSubscription::class);
        $oCollection->setListener(ObjectCollection::EVENT_INITIALIZE, 'onInitializeSubscriptions', $this);
        return $oCollection;
    }

    /**
     * Populates the subscription collection based on the current context
     * @see \ProBiz\Core\Subscription
     */
    public function onInitializeSubscriptions()
    {
        $this->update();
    }

    /**
     * Returns this checkout process ID (based upon session)
     *
     * @return integer
     */
    public function getID()
    {
        /**
         * @todo this has to be determined elsewhere.
         */
        return session('_token');
    }

    /**
     * Resets the IDs
     */
    public function resetIds()
    {
        $this->siteId = null;
        $this->actionIds = [];
        $this->shopIds = [];
        $this->locationIds = [];
        $this->shopLocationMainProductIds = [];

        $this->changedIds = true;
    }

    /**
     * Sets the site ID
     * @param integer $value
     */
    public function setSiteId($value)
    {
        if ($value != $this->siteId) {
            $this->changedIds = true;
        }
        $this->siteId = $value;
    }

    /**
     * Gets the site ID
     * @return integer
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * Add an action ID
     * @param integer $value
     */
    public function addActionId($value)
    {
        $this->actionIds = $this->addId($this->actionIds, $value);
    }

    /**
     * Add an shop ID
     * @param integer $value
     */
    public function addShopId($value)
    {
        $this->shopIds = $this->addId($this->shopIds, $value);
    }

    /**
     * Add an location ID
     * @param integer $value
     */
    public function addLocationId($value)
    {
        $this->locationIds = $this->addId($this->locationIds, $value);
    }

    /**
     * Add an ShopLocationMainProduct ID
     * @param integer $value
     */
    public function addShopLocationMainProductId($value)
    {
        $this->shopLocationMainProductIds = $this->addId($this->shopLocationMainProductIds, $value);
    }

    /**
     * Add an ID
     * @param array $collection
     * @param integer $value
     * @return array
     */
    protected function addId($collection, $value)
    {
        if (empty($value)) {
            return $collection;
        }
        if (in_array($value, $collection)) {
            return $collection;
        }
        $collection[] = $value;
        $this->changedIds = true;
        return $collection;
    }

    /**
     * Loads the subscriptions for the current items.
     * @param ObjectCollection|null $subscribed
     */
    public function update($subscribed = null)
    {
        if (!$this->changedIds) {
            return;
        }

        $this->available->clear();

        $subscriptionItems = [
            ProBizCheckoutSubscription::TYPE_SITE     => $this->siteId,
            ProBizCheckoutSubscription::TYPE_SHOP     => $this->shopIds,
            ProBizCheckoutSubscription::TYPE_LOCATION => $this->locationIds,
            ProBizCheckoutSubscription::TYPE_PRODUCT  => $this->shopLocationMainProductIds,
            ProBizCheckoutSubscription::TYPE_ACTION   => $this->actionIds,
        ];

        $subscriptions = ProBizCheckoutSubscription::loadSubscriptionsForItems($subscriptionItems);

        if (empty($subscriptions)) {
            return;
        }

        $subscribedIds = $subscribed instanceof ObjectCollection ? $subscribed->toIDArray() : [];

        foreach ($subscriptions as $subscription) {
            if (!$subscription->isActiveForDate($this->getNow())) {
                // skip it
                continue;
            }
            if ($subscription->optionsBits->checkOptions(ProBizCheckoutSubscription::OPTION_PRECHECKED)
                || in_array($subscription->getID(), $subscribedIds)
            ) {
                $subscription->setSubscribed(true);
            }

            $this->available->add($subscription);
        }
    }

    /**
     * Get the subscriptions subscribed to
     * @param integer $options
     * @return ObjectCollection
     */
    public function getSubscribed($options = ProBizCheckoutSubscription::OPTION_EMAIL)
    {
        $subscribed = new ObjectCollection();
        foreach ($this->available as $subscription) {
            if ($options && !$subscription->optionsBits->checkOptions($options)) {
                continue;
            }
            if ($subscription->isSubscribed()) {
                $subscribed->add($subscription);
            }
        }
        return $subscribed;
    }

    /**
     * Get the subscriptions for which email addresses are to be subscribed to
     * @return ObjectCollection
     */
    public function getSubscribedForEmailAddress()
    {
        return $this->getSubscribed(ProBizCheckoutSubscription::OPTION_EMAIL);
    }

    /**
     * Get the subscriptions for which order are to be subscribed to
     * @return ObjectCollection
     */
    public function getSubscribedForOrder()
    {
        return $this->getSubscribed(ProBizCheckoutSubscription::OPTION_ORDER);
    }

}