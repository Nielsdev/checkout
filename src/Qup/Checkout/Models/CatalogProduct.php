<?php

namespace Qup\Checkout\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CatalogProduct
 * We have to extend DateTimeRankSubProduct simply, becuase we need to be able to convert
 * to one by using "loadFromModel", everything "on-top" is defined here.
 */
class CatalogProduct extends DateTimeRankSubProduct
{

}