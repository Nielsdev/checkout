<?php

namespace Qup\Checkout\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentPlatform extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['psp', 'type', 'title', 'name', 'abbreviation', 'transaction_url', 'options', 'site_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
    */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    
}