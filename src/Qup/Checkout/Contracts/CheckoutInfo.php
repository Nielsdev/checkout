<?php

namespace Qup\Checkout\Contracts;

interface CheckoutInfo
{
    /**
     * Sets the paymentPlatformId
     *
     * @param integer $paymentPlatformId
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setPaymentPlatformId($paymentPlatformId);

    /**
     * Retrieves the payment platform id
     *
     * @return integer
     */
    public function getPaymentPlatformId();

    /**
     * Sets the paymentMethodId
     *
     * @param integer $paymentMethodId
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setPaymentMethodId($paymentMethodId);

    /**
     * Returns the payment method id
     * @return number
     */
    public function getPaymentMethodId();

    /**
     * Sets the insurance
     *
     * @param boolean $insurance
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setInsurance($insurance);

    /**
     * Returns the insurance
     *
     * @return boolean
     */
    public function getInsurance();

    /**
     * Sets the active transactionId
     *
     * @param integer $transactionId
     */
    public function setActiveTransactionId($transactionId);

    /**
     * Retrieves the active transactionId
     *
     * @return integer
     */
    public function getActiveTransactionId();

    /**
     * Sets the alias needed to call externally configured payment method settings
     *
     * @param string $alias
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setPaymentAlias($alias);

    /**
     * Returns the alias needed to call externally configured payment method settings
     * @return string
     */
    public function getPaymentAlias();

}