<?php

namespace Qup\Checkout\Controllers;

use App\Models\CheckoutCondition;
use \Illuminate\Http\Request;
use \Illuminate\Http\Response;
use Qup\Checkout\Models\ContentDocument;
use Qup\Checkout\Repositories\CheckoutConditionRepository;

abstract class ConditionsController extends BaseCheckoutController
{
    /**
     * @var string
     */
    public static $ERROR_MESSAGE_CONDITION_NOT_REQUIRED = 'Condition not present';

    /**
     * Get all reuired conditions
     *
     * @param CheckoutConditionRepository $checkoutConditions
     * @return Response
     */
    public function getConditions(CheckoutConditionRepository $checkoutConditions)
    {
        $result = [];

        /**
         * @var CheckoutCondition[] $conditions
         */
        $conditions = $checkoutConditions->getAllConditions();

        foreach ($conditions as $condition) {
            $content_document_id = $condition->content_document_id;
            if (!empty($content_document_id)) {
                $doc = ContentDocument::find($content_document_id);
                $content_document_id = !empty($doc) ? $this->getMappedFrontendContentID($doc->code) : null;
            }

            $result[] = [
                'id'        => $condition->id,
                'title'     => $condition->title,
                'tooltip'   => $condition->tooltip,
                'contentid' => $content_document_id,
                'accepted'  => $checkoutConditions->isAccepted($condition->id)
            ];
        }

        return static::createSuccessResponse($result);
    }

    /**
     * Awful hack
     * @param string $contentid
     * @return string Mapped content ID
     */
    protected function getMappedFrontendContentID($contentid)
    {
        switch (strtolower($contentid)) {
            case 'voorwaarden':
            case 'handelsbetingelser':
                return 'general-conditions';
            case 'privacy':
            case 'persondatapolitik':
                return 'privacy-statement';
            case 'actievoorwaarden':
                return 'promotion-conditions';
            default:
                return $contentid;
        }
    }

    /**
     * Get condition by conditionId
     *
     * @param CheckoutConditionRepository $checkoutConditions
     * @param $conditionId
     * @return Response
     */
    public function getCondition(CheckoutConditionRepository $checkoutConditions, $conditionId)
    {

        if (!$checkoutConditions->has($conditionId)) {
            \Log::warning('Requested condition '.$conditionId.' not present, ignore silently');
            // fail silently instead
            return static::createSuccessResponse(true);
            //return static::createErrorResponse($this::$ERROR_MESSAGE_CONDITION_NOT_REQUIRED, '404', 404);
        }

        $result = $checkoutConditions->isAccepted($conditionId);

        return static::createSuccessResponse($result);
    }

    /**
     * Change status of given Condition
     *
     * @param Request $request
     * @param CheckoutConditionRepository $checkoutConditions
     * @param $conditionID
     * @return Response
     */
    public function putCondition(Request $request, CheckoutConditionRepository $checkoutConditions, $conditionId)
    {
        if (!$checkoutConditions->has($conditionId)) {
            \Log::warning('Posted condition '.$conditionId.' not present, ignore silently');
            return static::createSuccessResponse(true);
            //return static::createErrorResponse($this::$ERROR_MESSAGE_CONDITION_NOT_REQUIRED, '404', 404);
        }

        $checkoutConditions->setAccepted($conditionId, $request->get('condition'));

        $checkoutConditions->save();

        return static::createSuccessResponse(true);
    }
}