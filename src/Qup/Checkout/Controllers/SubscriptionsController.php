<?php

namespace Qup\Checkout\Controllers;

use \Illuminate\Http\Request;
use \Qup\Checkout\Definitions\CartSubscription;
use Illuminate\Container\Container;
use Qup\Checkout\Repositories\CheckoutSubscriptionRepository;

abstract class SubscriptionsController extends BaseCheckoutController
{
    /**
     * Get all available subscriptions for the current checkout
     *
     * @param CheckoutSubscriptionRepository $checkoutSubscriptions
     * @return \Symfony\Component\HttpFoundation\Response The subscriptions.
     */
    public function getSubscriptions(CheckoutSubscriptionRepository $checkoutSubscriptions)
    {
        $result = [];

        foreach ($checkoutSubscriptions->getAllSubscriptions() as $subscription) {
            $result[] = CartSubscription::create(
                $subscription->id,
                $subscription->title,
                $subscription->tooltip,
                false,
                ($subscription->options & CheckoutSubscriptionRepository::OPTION_PRECHECKED == CheckoutSubscriptionRepository::OPTION_PRECHECKED),
                $subscription->content_document_id,
                $checkoutSubscriptions->isSubscribed($subscription->id)
            );
        }

        // Return result
        return static::createSuccessResponse($result);
    }

    /**
     * Get the optin-status for the supplied subscription id
     *
     * @param CheckoutSubscriptionRepository $checkoutSubscriptions
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response The subscription state.
     */
    public function getSubscription(CheckoutSubscriptionRepository $checkoutSubscriptions, $id)
    {
        if (!$checkoutSubscriptions->has($id)) {
            \Log::warning('Posted subscription '.$id.' not present, ignore silently');
            // fail silently instead
            return static::createSuccessResponse(true);
        }

        $result = ['subscription'=>$checkoutSubscriptions->isSubscribed($id)];

        // Return result
        return static::createSuccessResponse($result);
    }

    /**
     * Set the optin-status for the supplied subscription id
     *
     * @param Container $container
     * @param Request $request
     * @param CheckoutSubscriptionRepository $checkoutSubscriptions
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response The subscription state.
     */
    public function putSubscription(Container $container, Request $request, CheckoutSubscriptionRepository $checkoutSubscriptions, $id)
    {
        if (!$checkoutSubscriptions->has($id)) {
            \Log::warning('Posted subscription '.$id.' not present, ignore silently');
            // fail silently instead
            return static::createSuccessResponse(true);
        }

        $checkoutSubscriptions->setSubscribed($id, $request->get('subscription'));

        $checkoutSubscriptions->save();

        // Call it through the service container to meet all dependencies
        return $container->call([$this, 'getSubscription'], [$id]);
    }
}