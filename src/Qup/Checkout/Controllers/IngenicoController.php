<?php

namespace Qup\Checkout\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Container\Container;
use \Illuminate\Http\Request;
use ProBiz\Cache\ObjectCacheController;
use ProBiz\Order\OrderFactory;
use ProBiz\Order\Transaction\TransactionFactory;
use ProBiz\Payment\PaymentPlatformOgone;
use Qup\Checkout\Contracts\CheckoutInfo;
use Symfony\Component\HttpFoundation\Response;

abstract class IngenicoController extends BaseCheckoutController
{
    /**
     * Handles the ingenico server-to-server callback.
     *
     * @param Request $request
     * @param PaymentPlatformOgone $platform
     * @return \Illuminate\Http\Response
     */
    public function postOgoneResult(Container $container, Request $request, PaymentPlatformOgone $platform)
    {
        if (!$request->has('orderID')) {
            return self::createErrorResponse('Something went wrong.', Response::HTTP_BAD_REQUEST, self::CODE_ERR_UNKNOWN_ERROR);
        }

        $ogoneTransactionId = $request->get('orderID');
        $transactionId = PaymentPlatformOgone::convertOgoneToProBiz($ogoneTransactionId);

        \ObjectLock::id($transactionId)->lockAndRetry(null, 'PT20S', 'Lock transaction (ingenico)', 20, 1);

        try {
            $platform->handleStatusUpdate($request->all(), false);
        }
        catch (\Exception $ex) {
            \Log::error('Failed handling ingenico callback with exception '.$ex->getMessage(). ', params (' . implode(', ',$request->all()));
            \ObjectLock::id($request->get('orderID'))->release();
            return self::createErrorResponse('Something went wrong.', Response::HTTP_BAD_REQUEST, self::CODE_ERR_UNKNOWN_ERROR);
        }

        $transaction = TransactionFactory::load($transactionId);
        $orderId = $transaction->getOrderID();
        $order = OrderFactory::load($orderId);

        // Scope order.
        $container->singleton(\Qup\Checkout\Order\Contracts\OrderInstance::class, function() use ($order) {
            return $order;
        });

        \Event::fire(new \Qup\Checkout\Events\TransactionStatusChanged($transaction, $order));

        \ObjectLock::id($request->get('orderID'))->release();

        return self::createSuccessResponse('Update processed', Response::HTTP_OK);
    }

    /**
     * Handles the ogone populate feedback.
     *
     * @param Request $request
     * @param CheckoutInfo $info
     * @param PaymentPlatformOgone $platform
     * @throws \UnderflowException
     * @return \Illuminate\Http\Response
     */
    public function getOgonePopulate(Request $request, CheckoutInfo $info, PaymentPlatformOgone $platform)
    {
        $paymentPlatformResult = $platform->handlePopulationResult($request->all());

        if (empty($paymentPlatformResult)) {
            return self::createErrorResponse('Invalid ogone callback supplied.', Response::HTTP_BAD_REQUEST, self::CODE_ERR_VALIDATION);
        }

        $info->setPaymentAlias($paymentPlatformResult->getAlias());

        return self::createSuccessResponse(null, Response::HTTP_NO_CONTENT);
    }
}