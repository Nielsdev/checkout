<?php

namespace Qup\Checkout\Controllers;

use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Contracts\Validation\ValidationException;
use ProBiz\Cache\ObjectCacheController;
use Qup\Checkout\Order\Contracts\OrderNew;

abstract class UserDataController extends BaseCheckoutController
{

    public function getUserData(Request $request, OrderNew $order)
    {
        $info = $order->instance()->orderInfo;

        $countryCode = '';
        if (!empty($info->getBillingCountryID())) {
            try {
                $country = ObjectCacheController::get(ObjectCacheController::ITEM_COUNTRY, $info->getBillingCountryID());
                $countryCode = $country->getIso3166a2();
            } catch (\Exception $e) {
                // noop
            }
        }

        return static::createSuccessResponse([
            'email' => $info->getBillingEmail(),
            'title' => $info->getBillingTitle(),
            'firstname' => $info->getBillingFirstName(),
            'suffix' => $info->getBillingSuffix(),
            'lastname' => $info->getBillingLastName(),
            'fullname' => $info->getBillingFullName(),
            'phone' => $info->getBillingPhone(),
            'address' => [
                'street' => $info->getBillingStreet(),
                'streetsuffix' => $info->getBillingStreetSuffix(),
                'housenumber' => $info->getBillingHouseNumber(),
                'housenumbersuffix' => $info->getBillingHouseNumberSuffix(),
                'postalcode' => $info->getBillingZipcode(),
                'city' => $info->getBillingCity(),
                'country' => $countryCode,
            ],
        ]);
    }
    /**
     * Adds userData to the orderInfo
     *
     * @param Request $request
     * @param OrderNew $order
     */
    public function putUserData(Request $request, OrderNew $order, Container $container)
    {
        $data = $request->all();
        $validator = $this->getPutUserDataValidator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $info = $order->instance()->orderInfo;

        $info->setBillingEmail($data['email']);

        if ($request->has('title')) {
            $info->setBillingTitle($data['title']);
        }
        if ($request->has('firstname')) {
            $info->setBillingFirstName($data['firstname']);
        }
        if ($request->has('middlename')) {
            $info->setBillingSuffix($data['middlename']);
        }
        if ($request->has('lastname')) {
            $info->setBillingLastName($data['lastname']);
        }
        if ($request->has('suffix')) {
            $info->setBillingSuffix($data['suffix']);
        }
        if ($request->has('fullname')) {
            $info->setBillingFullName($data['fullname']);
        }
        if ($request->has('phone')) {
            $info->setBillingPhone($data['phone']);
        }
        if ($request->has('address.street')) {
            $info->setBillingStreet($data['address']['street']);
        }
        if ($request->has('address.streetsuffix')) {
            $info->setBillingStreetSuffix($data['address']['streetsuffix']);
        }
        if ($request->has('address.housenumber')) {
            $info->setBillingHouseNumber($data['address']['housenumber']);
        }
        if ($request->has('address.housenumbersuffix')) {
            $info->setBillingHouseNumberSuffix($data['address']['housenumbersuffix']);
        }
        if ($request->has('address.postalcode')) {
            $info->setBillingZipcode($data['address']['postalcode']);
        }
        if ($request->has('address.city')) {
            $info->setBillingCity($data['address']['city']);
        }
        if ($request->has('address.country')) {
            $info->setBillingCountryID($this->getCountryId($data['address']['country']));
        }

        if ($request->has('system.ipaddress')) {
            $info->setIPAddress($data['system']['ipaddress']);
        }
        if ($request->has('system.tracking_id')) {
            $info->setGoogleSessionID($data['system']['tracking_id']);
        }
        if ($request->has('system.useragent')) {
            $info->setUserAgent($data['system']['useragent']);
        }

        // Call it through the service container to meet all dependencies
        return $container->call([$this, 'getUserData']);
    }

    /**
     * Determine the country ID for the supplied code
     * @param string $iso3166a2
     * @return integer|null
     */
    protected function getCountryId($iso3166a2)
    {
        if (empty($iso3166a2)) {
            return null;
        }

        try {
            $country = ObjectCacheController::get(ObjectCacheController::ITEM_COUNTRY, strtoupper(trim($iso3166a2)));
            return $country->getID();
        } catch (\Exception $e) {
            // noop
        }
        return null;
    }

    /**
     * Makes then returns the Validator for the putProducts request
     *
     * @param array $data
     * @return Validator
     */
    protected function getPutUserDataValidator($data)
    {
        return \Validator::make($data, [
            'email' => 'required|email',
            'lastname' => 'required_without:fullname|string',
            'fullname' => 'required_without:lastname|string',
            'address.postalcode' => 'required'
        ]);
    }
}