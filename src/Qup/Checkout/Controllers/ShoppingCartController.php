<?php

namespace Qup\Checkout\Controllers;

use \Illuminate\Contracts\Validation\Validator;
use \Illuminate\Http\Request;
use \Illuminate\Contracts\Validation\ValidationException;
use \Illuminate\Http\Response;
use \ProBiz\Cache\ObjectCacheController;
use \Qup\Checkout\Catalog\Contracts\CartCatalog;
use \ProBiz\Order\OrderLineHandler;
use \Qup\Checkout\Events\CartContentsChanged;
use \Qup\Checkout\Order\Contracts\ShoppingCart;
use \Illuminate\Container\Container;
use \Qup\Checkout\Contracts\CheckoutInfo;
use \ProBiz\Order\Insurancecost\Calculator as InsuranceCostCalculator;
use \Qup\Checkout\Order\Contracts\OrderInstance;
use \Qup\Promotions\Contracts\Input as InputContract;
use \Qup\Promotions\Contracts\Promotion as PromotionContract;
use \ProBiz\IO\Log\ErrorLog;

abstract class ShoppingCartController extends BaseCheckoutController
{
    /**
     * Adds a product to cart
     *
     * @param Request $request
     * @param PromotionContract $promotion The promotion.
     * @param InputContract $input The promotion input.
     * @param CheckoutInfo $info
     * @param ShoppingCart $orderResolver
     * @param CartCatalog $catalog
     * @param Container $container
     * @throws ValidationException
     */
    protected function putProducts(Request $request, PromotionContract $promotion, InputContract $input, CheckoutInfo $info, ShoppingCart $orderResolver, Container $container)
    {
        try {
            $order = $orderResolver->instance();
        } catch (\Exception $e) {
            ErrorLog::logError('Error retrieving order instance for putProducts', $e);
            throw $e;
        }

        foreach ($request->get('tickets') as $product) {

            $id = $product['id'];
            $quantity = $product['quantity'];
            try {
                $dateTimeRankSubProduct = ObjectCacheController::get(ObjectCacheController::ITEM_DATETIMERANKSUBPRODUCT, $id);
            } catch (\Exception $e) {
                ErrorLog::logError('Error retrieving DTRSP id '.$id.' from catalog', $e);
                throw $e;
            }
            try {
                OrderLineHandler::handleOrderLineFromDateSubProduct($order, $dateTimeRankSubProduct, $quantity);
            } catch (\Exception $e) {
                ErrorLog::logError('Error handling orderline from DTRSP id '.$id, $e);
                throw $e;
            }
        }
    }

    /**
     * Puts the cart data in a combined request.
     *
     * @param Request $request
     * @param Container $container
     * @return Response
     */
    public function putCart(Request $request, PromotionContract $promotion, InputContract $input, CheckoutInfo $info, ShoppingCart $orderResolver, Container $container)
    {
        $validator = $this->getPutCartValidator($request->all());
        $order = $orderResolver->instance();

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // Handle the puts.
        $this->putProducts($request, $promotion, $input, $info, $orderResolver, $container);
        $this->putInsurance($request, $info, $container);

        // Fire CartContentsChanged event if the contents where changed.
        \Event::fire(new CartContentsChanged($order, $info, $input));

        // Return receipt.
        return $container->call([$this, 'getReceipt']);
    }

    /**
     * Sets the insurance then returns the current insured state.
     *
     * @param Request $request
     * @param CheckoutInfo $info
     * @return \Illuminate\Http\Response The response.
     */
    protected function putInsurance(Request $request, CheckoutInfo $info, Container $container)
    {
        // Indicates whether or not the Cart has insurance
        $info->setInsurance($request->get('insurance'));
    }

    /**
     * Creates an estimate of insurance costs based on current order composition
     *
     * @param Order $order
     * @return \Illuminate\Http\Response The response.
     */
    public function getInsuranceEstimate(OrderInstance $order)
    {
        return self::createSuccessResponse([
            'costs' => InsuranceCostCalculator::getCostsForShoppingCart($order)
        ]);
    }

    /**
     * Makes then returns the Validator for the putProducts request
     *
     * @param array $data
     * @return Validator
     */
    protected function getPutCartValidator($data)
    {
        return \Validator::make($data, [
            'tickets' => 'is_valid_cart_put'
        ]);
    }
}