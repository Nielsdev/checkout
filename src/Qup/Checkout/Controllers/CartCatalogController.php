<?php

namespace Qup\Checkout\Controllers;

use App\Definitions\ProductDetails;
use App\Http\Controllers\ProductsController;
use \Qup\Checkout\Catalog\CartCatalog;
use \Qup\Checkout\Definitions\Contracts\CartCatalog as CartCatalogDefinition;
use Illuminate\Container\Container;

class CartCatalogController extends BaseCheckoutController
{
    /**
     * Returns the cart catalog.
     *
     * @param CartCatalog $catalog
     * @param Container $container the service container
     * @param integer $product_id
     * @param string $date
     * @param string $time
     * @return \Illuminate\Http\Response
     */
    public function getCartCatalog(CartCatalog $catalog, Container $container, $product_id, $date = null, $time = null)
    {
        $catalog->setData([
            'shopLocationMainProducts' => [$product_id],
            'date' => $date,
            'time' => $time
        ]);

        // Bind it.
        $container->singleton(\Qup\Checkout\Catalog\Contracts\CartCatalog::class, function() use ($catalog) {
            return $catalog;
        });

        return $container->call([$this, 'getCartCatalogProducts']);
    }

    /**
     * Returns the cart catalog products.
     *
     * @param CartCatalogDefinition $products
     * @return \Illuminate\Http\Response
     */
    public function getCartCatalogProducts(CartCatalogDefinition $products)
    {
        return self::createSuccessResponse($products);
    }
}