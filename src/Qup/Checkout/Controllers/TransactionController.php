<?php

namespace Qup\Checkout\Controllers;

use Illuminate\Container\Container;
use Illuminate\Http\Response;
use ProBiz\Order\Transaction\TransactionFactory;
use ProBiz\Order\Transaction\TransactionHandler;
use ProBiz\Order\Transaction\TransactionStatus;

use Qup\Checkout\Contracts\CheckoutInfo;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Contracts\OrderSucceeded;
use Qup\Checkout\Order\OrderFactory;

abstract class TransactionController extends BaseCheckoutController
{
    /**
     * Retrieves the transaction status.
     *
     * @param OrderInstance $order
     * @param CheckoutInfo $info
     * @return Response
     */
    public function getTransactionStatus(CheckoutInfo $info, Container $container)
    {
        $activeTransactionId = $info->getActiveTransactionId();
        \ObjectLock::id($activeTransactionId)->lockAndRetry(null, 'PT20S', 'Lock transaction', 20, 1);

        $transaction = TransactionFactory::load($activeTransactionId);

        if (!$transaction) {
            \ObjectLock::id($activeTransactionId)->release();
            return self::createErrorResponse('No active transaction found', Response::HTTP_NOT_FOUND, self::CODE_ERR_ITEM_NOT_FOUND);
        }

        $orderId = $transaction->getOrderID();
        $order = \ProBiz\Order\OrderFactory::load($orderId);

        \App::singleton(\Qup\Checkout\Order\Contracts\OrderInstance::class, function() use ($order) {
            return $order;
        });

        if ($order instanceof \ProBiz\Order\OrderSucceeded) {
            \ObjectLock::id($activeTransactionId)->release();
            return self::createSuccessResponse('succeeded');
        }

        // Update transaction
        TransactionHandler::updateOpenTransactions($order);

        $status = 'created';
        if (in_array($transaction->getStatus(), TransactionStatus::getSucceededStatuses())) {
            // Convert order
            $container->make(OrderSucceeded::class);

            // Set status string
            $status = 'succeeded';
        } elseif (in_array($transaction->getStatus(), TransactionStatus::getFailedStatuses())) {
            $status = 'failed';
        } elseif (in_array($transaction->getStatus(), TransactionStatus::getOpenStatuses())) {
            $status = 'open';
        }

        \ObjectLock::id($activeTransactionId)->release();

        return self::createSuccessResponse($status);
    }
}