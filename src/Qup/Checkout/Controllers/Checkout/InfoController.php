<?php

namespace Qup\Checkout\Controllers\Checkout;

use Illuminate\Http\Response;
use ProBiz\Checkout\CheckoutSubscription as ProBizCheckoutSubscription;
use Qup\Checkout\Controllers\BaseCheckoutController;
use Qup\Checkout\Definitions\CartSubscription;
use Qup\Checkout\Models\CheckoutSubscriptions;
use Qup\Checkout\Repositories\CheckoutConditionRepository;

/**
 * Class InfoController
 * @package Qup\Checkout\Controllers\Checkout
 */
class InfoController extends BaseCheckoutController
{

    /**
     * @var CheckoutConditionRepository
     */
    private $checkoutConditionRepository;

    /**
     * InfoController constructor.
     *
     * @param CheckoutConditionRepository $checkoutConditionRepository
     */
    public function __construct(CheckoutConditionRepository $checkoutConditionRepository)
    {
        $this->checkoutConditionRepository = $checkoutConditionRepository;
    }

    /**
     * Get all available subscriptions and required conditions
     * @return Response
     */
    public function index()
    {
        return static::createSuccessResponse([
            'subscriptions' => $this->getSubscriptions(new CheckoutSubscriptions),
            'conditions' => $this->checkoutConditionRepository->getAllConditions()
        ]);
    }


    /**
     * Get all available subscriptions for the current checkout
     * @param CheckoutSubscriptions $checkoutSubscriptions
     * @return array The subscriptions.
     */
    private function getSubscriptions(CheckoutSubscriptions $checkoutSubscriptions)
    {
        $result = [];

        foreach ($checkoutSubscriptions->available as $subscription) {
            if (!$subscription->optionsBits->checkOptions(ProBizCheckoutSubscription::OPTION_VISIBLE)) {
                // do not add invisible subscription to resultset, but do keep it in available
                continue;
            }
            $result[] = CartSubscription::createFromObject($subscription);
        }

        // Return result
        return $result;
    }
}
