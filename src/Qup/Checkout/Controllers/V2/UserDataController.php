<?php

namespace Qup\Checkout\Controllers\V2;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Illuminate\Contracts\Validation\ValidationException;
use ProBiz\Cache\ObjectCacheController;
use Qup\Checkout\Order\Contracts\OrderNew;
use Qup\Checkout\Repositories\CheckoutConditionRepository;
use Qup\Checkout\Repositories\CheckoutSubscriptionRepository;
use Validator;

/**
 * Class UserDataController
 * @package Qup\Checkout\Controllers\V2
 */
abstract class UserDataController extends BaseCheckoutController
{

    /**
     * @param Container $container
     * @return JsonResponse
     */
    public function getData(Container $container, CheckoutConditionRepository $checkoutConditions)
    {
        return static::createSuccessResponse([
            'userData' => $container->call([$this, 'getUserData']),
            'subscriptions' => $container->call([$this, 'getActiveSubscriptions']),
            'conditions' => $checkoutConditions->getAcceptedConditions()
        ]);
    }

    /**
     * @param OrderNew $order
     * @return array
     */
    public function getUserData(OrderNew $order)
    {
        $info = $order->instance()->orderInfo;

        $countryCode = '';
        if (!empty($info->getBillingCountryID())) {
            try {
                $country = ObjectCacheController::get(ObjectCacheController::ITEM_COUNTRY, $info->getBillingCountryID());
                $countryCode = $country->getIso3166a2();
            } catch (\Exception $e) {

            }
        }

        return [
            'email' => $info->getBillingEmail(),
            'title' => $info->getBillingTitle(),
            'firstname' => $info->getBillingFirstName(),
            'suffix' => $info->getBillingSuffix(),
            'lastname' => $info->getBillingLastName(),
            'fullname' => $info->getBillingFullName(),
            'phone' => $info->getBillingPhone(),
            'address' => [
                'street' => $info->getBillingStreet(),
                'streetsuffix' => $info->getBillingStreetSuffix(),
                'housenumber' => $info->getBillingHouseNumber(),
                'housenumbersuffix' => $info->getBillingHouseNumberSuffix(),
                'postalcode' => $info->getBillingZipcode(),
                'city' => $info->getBillingCity(),
                'country' => $countryCode,
            ]
        ];
    }

    /**
     * Awful hack
     * @param string $contentid
     * @return string Mapped content ID
     */
    protected function getMappedFrontendContentID($contentid)
    {
        switch (strtolower($contentid)) {
            case 'voorwaarden':
            case 'handelsbetingelser':
                return 'general-conditions';
            case 'privacy':
            case 'persondatapolitik':
                return 'privacy-statement';
            case 'actievoorwaarden':
                return 'promotion-conditions';
            default:
                return $contentid;
        }
    }

    /**
     * Adds userData to the orderInfo
     *
     * @param Request $request
     * @param OrderNew $order
     */
    public function putData(Container $container, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'lastname' => 'required_without:fullname|string',
            'fullname' => 'required_without:lastname|string',
            'address.postalcode' => 'required'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return static::createSuccessResponse([
            'userData' => $container->call([$this, 'putUserData']),
            'subscriptions' => $container->call([$this, 'putSubscriptions']),
            'conditions' => $container->call([$this, 'putConditions'])
        ]);
    }

    /**
     * @param Container $container
     * @param Request $request
     * @param CheckoutConditionRepository $checkoutConditions
     * @return array
     */
    public function putConditions(CheckoutConditionRepository $checkoutConditions, Container $container, Request $request)
    {
        if ($request->has('conditions')) {
            foreach ($request->input('conditions') as $condition) {
                $checkoutConditions->setAccepted($condition['id'], $condition['accept']);
            }
        }

        $checkoutConditions->save();

        return $checkoutConditions->getAcceptedConditions();
    }

    /**
     * @param Container $container
     * @param Request $request
     * @param CheckoutSubscriptionRepository $checkoutSubscriptions
     * @return array
     */
    public function putSubscriptions(CheckoutSubscriptionRepository $checkoutSubscriptions, Container $container, Request $request)
    {
        if ($request->has('subscriptions')) {
            foreach ($request->input('subscriptions') as $subscription) {
                if ($checkoutSubscriptions->has($subscription['id'])) {
                    $checkoutSubscriptions->setSubscribed($subscription['id'], $subscription['subscribe']);
                }
            }
        }

        $checkoutSubscriptions->save();

        return $container->call([$this, 'getActiveSubscriptions']);
    }

    /**
     * @param Container $container
     * @param Request $request
     * @param OrderNew $order
     * @return array
     */
    public function putUserData(Container $container, Request $request, OrderNew $order)
    {
        $info = $order->instance()->orderInfo;

        $info->setBillingEmail($request->input('email'));

        if ($request->has('title')) {
            $info->setBillingTitle($request->input('title'));
        }
        if ($request->has('firstname')) {
            $info->setBillingFirstName($request->input('firstname'));
        }
        if ($request->has('middlename')) {
            $info->setBillingSuffix($request->input('middlename'));
        }
        if ($request->has('lastname')) {
            $info->setBillingLastName($request->input('lastname'));
        }
        if ($request->has('suffix')) {
            $info->setBillingSuffix($request->input('suffix'));
        }
        if ($request->has('fullname')) {
            $info->setBillingFullName($request->input('fullname'));
        }
        if ($request->has('phone')) {
            $info->setBillingPhone($request->input('phone'));
        }
        if ($request->has('address.street')) {
            $info->setBillingStreet($request->input('address.street'));
        }
        if ($request->has('address.streetsuffix')) {
            $info->setBillingStreetSuffix($request->input('address.streetsuffix'));
        }
        if ($request->has('address.housenumber')) {
            $info->setBillingHouseNumber($request->input('address.housenumber'));
        }
        if ($request->has('address.housenumbersuffix')) {
            $info->setBillingHouseNumberSuffix($request->input('address.housenumbersuffix'));
        }

        $info->setBillingZipcode($request->input('address.postalcode'));

        if ($request->has('address.city')) {
            $info->setBillingCity($request->input('address.city'));
        }
        if ($request->has('address.country')) {
            $info->setBillingCountryID(
                $this->getCountryId($request->input('address.country'))
            );
        }

        if ($request->has('system.ipaddress')) {
            $info->setIPAddress($request->input('system.ipaddress'));
        }
        if ($request->has('system.tracking_id')) {
            $info->setGoogleSessionID($request->input('system.tracking_id'));
        }
        if ($request->has('system.useragent')) {
            $info->setUserAgent($request->input('system.useragent'));
        }

        return $container->call([$this, 'getUserData']);
    }

    /**
     * Determine the country ID for the supplied code
     * @param string $iso3166a2
     * @return integer|null
     */
    protected function getCountryId($iso3166a2)
    {
        if (empty($iso3166a2)) {
            return null;
        }

        try {
            $country = ObjectCacheController::get(ObjectCacheController::ITEM_COUNTRY, strtoupper(trim($iso3166a2)));
            return $country->getID();
        } catch (\Exception $e) {
            // noop
        }
        return null;
    }

    /**
     * @param CheckoutSubscriptionRepository $checkoutSubscriptions
     * @return array.
     */
    public function getActiveSubscriptions(CheckoutSubscriptionRepository $checkoutSubscriptions)
    {
        $activeSubscriptions = [];

        foreach ($checkoutSubscriptions->getAllSubscriptions() as $subscription) {
            $activeSubscriptions[] = $subscription->id;
        }

        return $activeSubscriptions;
    }
}
