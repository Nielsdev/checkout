<?php

namespace Qup\Checkout\Controllers\V2;

use Illuminate\Http\Response;
use ProBiz\Checkout\CheckoutSubscription as ProBizCheckoutSubscription;
use Qup\Checkout\Controllers\BaseCheckoutController;
use Qup\Checkout\Definitions\CartSubscription;
use Qup\Checkout\Models\CheckoutSubscriptions;
use Qup\Checkout\Repositories\CheckoutConditionRepository;
use Qup\Checkout\Repositories\CheckoutSubscriptionRepository;

/**
 * Class InfoController
 * @package Qup\Checkout\Controllers\Checkout
 */
class InfoController extends BaseCheckoutController
{
    /**
     * @var CheckoutConditionRepository
     */
    private $checkoutConditionRepository;

    /**
     * @var CheckoutSubscriptionRepository
     */
    private $checkoutSubscriptionRepository;

    /**
     * InfoController constructor.
     *
     * @param CheckoutConditionRepository $checkoutConditionRepository
     * @param CheckoutSubscriptionRepository $checkoutSubscriptionRepository
     */
    public function __construct(CheckoutConditionRepository $checkoutConditionRepository, CheckoutSubscriptionRepository $checkoutSubscriptionRepository)
    {
        $this->checkoutConditionRepository = $checkoutConditionRepository;
        $this->checkoutSubscriptionRepository = $checkoutSubscriptionRepository;
    }

    /**
     * Get all available subscriptions and required conditions
     * @return Response
     */
    public function index()
    {
        return static::createSuccessResponse([
            'subscriptions' => $this->getSubscriptions(),
            'conditions' => $this->checkoutConditionRepository->getAllConditions()
        ]);
    }


    /**
     * Get all available subscriptions for the current checkout
     *
     * @return CartSubscription[] The subscriptions.
     */
    private function getSubscriptions()
    {
        $result = [];

        foreach ($this->checkoutSubscriptionRepository->getSubscribedSubscriptions() as $subscription) {
            $result[] = CartSubscription::create(
                $subscription->id,
                $subscription->title,
                $subscription->tooltip,
                false,
                ($subscription->options & CheckoutSubscriptionRepository::OPTION_PRECHECKED == CheckoutSubscriptionRepository::OPTION_PRECHECKED),
                $subscription->content_document_id,
                $this->checkoutSubscriptionRepository->isSubscribed($subscription->id)
            );
        }

        // Return result
        return $result;
    }
}
