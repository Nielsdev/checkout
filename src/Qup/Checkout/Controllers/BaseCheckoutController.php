<?php

namespace Qup\Checkout\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use ProBiz\Order\OrderTotals;
use Qup\Checkout\Definitions\Contracts\CartProducts;
use Qup\Checkout\Definitions\OrderStatus;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Contracts\CheckoutInfo;
use Qup\Promotions\Contracts\Input as InputContract;
use Qup\Promotions\FunTrips\Discounts\DiscountScenario;

abstract class BaseCheckoutController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const CODE_OK                    = 0;
    const CODE_ERR_INVALID_SESSION   = 100;
    const CODE_ERR_VALIDATION        = 101;
    const CODE_ERR_ITEM_NOT_FOUND    = 102;
    const CODE_ERR_PARENT_NOT_FOUND  = 103;
    const CODE_ERR_INVALID_STATE     = 104;
    const CODE_ERR_RESOURCE_CREATION = 105;
    const CODE_ERR_UNKNOWN_ERROR     = 500;

    /**
     * Creates a Validation error response
     *
     * @param array $errors
     * @return \Illuminate\Http\Response The response.
     */
    public static function createValidationErrorResponse($errors)
    {
        return static::createErrorResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY, self::CODE_ERR_VALIDATION);
    }

    /**
     * Returns an error response
     *
     * @param array $errors The errors.
     * @param integer $status The http status response code.
     * @param integer $code The internal status response code.
     * @return \Illuminate\Http\Response The response.
     */
    public static function createErrorResponse($errors, $status, $code)
    {
        $result = [
            'internal_code'=>$code,
            'success'=>false,
            'errors'=>$errors,
        ];
        return response()->json($result, $status);
    }

    /**
     * Returns the checkout receipt
     *
     * @param OrderInstance $order
     * @param InputContract $input
     * @param CartProducts $products
     * @param CheckoutInfo $checkoutInfo
     * @return array.
     */
    protected function getReceiptData($order, $input, $products, $checkoutInfo)
    {
        $totals = OrderTotals::getTotals($order);
        $info = $order->orderInfo;

        // Init status vars
        $unused_digital_vouchers = false;
        $max_redeemable_vouchers = $input->getMaxVouchers();

        // Check paper vouchers
        $vouchers = $input->getPaperVouchers();
        $paper_vouchers = [];
        foreach ($vouchers as $code) {
            $assigned_code = $order->actionCodes->getWith('getCode', $code);
            $processed     = $assigned_code && $assigned_code->getOrderLineID();

            $paper_vouchers[] = [
                'code' => $code,
                'processed' => $processed,
            ];
        }

        // Check digital vouchers
        $vouchers = $input->getDigitalVouchers();
        $digital_vouchers = [];
        foreach ($vouchers as $code) {
            $assigned_code = $order->actionCodes->getWith('getCode', $code);
            $processed     = $assigned_code && $assigned_code->getOrderLineID();

            $digital_vouchers[] = [
                'code' => $code,
                'processed' => $processed,
            ];
            if (!$processed) {
                $unused_digital_vouchers = true;
            }
        }

        $assigned_vouchers = count($input->getVouchers());

        // Return
        return [
            'products' => $products,
            'order_id' => $order->getFrontendID(),
            'paper_vouchers' => $paper_vouchers,
            'digital_vouchers' => $digital_vouchers,
            'totals' => [
                'subtotal' => $totals->getTotal(OrderTotals::TYPE_PRODUCTS),
                'discount' => $totals->getTotal(OrderTotals::TYPE_DISCOUNTS),
                'bookingcosts' => $totals->getTotal(OrderTotals::TYPE_BOOKING),
                'insurancecosts' => $totals->getTotal(OrderTotals::TYPE_INSURANCES),
                'grandtotal' => $totals->getTotal(OrderTotals::TYPE_GRAND)
            ],
            'email' => $info->getBillingEmail(),
            'paymentmethod' => $checkoutInfo->getPaymentMethodId(),
            'max_discount_achieved' => ($assigned_vouchers === false) ? false : ($max_redeemable_vouchers <= $assigned_vouchers),
            'max_redeemable_vouchers' => $max_redeemable_vouchers,
            'unused_digital_vouchers' => $unused_digital_vouchers,
        ];
    }

    /**
     * @param OrderInstance $order
     * @param InputContract $input
     * @param CartProducts $products
     * @param CheckoutInfo $checkoutInfo
     * @return \Illuminate\Http\Response The response.
     */
    public function getReceipt(OrderInstance $order, InputContract $input, CartProducts $products, CheckoutInfo $checkoutInfo){
        return self::createSuccessResponse($this->getReceiptData($order, $input, $products, $checkoutInfo));
    }

    /**
     * Returns the current order's status
     *
     * @param OrderInstance $order
     * @param OrderStatus $status The definition
     * @return \Illuminate\Http\Response The response.
     */
    public function getOrderStatus(OrderInstance $order, OrderStatus $status)
    {
        $status->setStatus($order->getStatus());

        return self::createSuccessResponse($status);
    }

    /**
     * Returns a success response
     *
     * @param mixed $data The data to be send in the response.
     * @param integer $status The http status response code.
     * @param integer $code The internal status response code.
     * @return \Illuminate\Http\Response The response.
     */
    public static function createSuccessResponse($data, $status = Response::HTTP_OK, $code = 0)
    {
        $result = [
            'internal_code'=>$code,
            'success'=>true,
            'data'=>$data,
        ];
        return response()->json($result, $status);
    }
}
