<?php

namespace Qup\Checkout\Controllers;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Response;
use ProBiz\Cache\ObjectCacheController;
use ProBiz\Order\Transaction\TransactionHandler;
use ProBiz\Payment\PaymentPlatformImplementation;
use ProBiz\Payment\PaymentPlatformParameter;
use Qup\Checkout\Catalog\PaymentCatalog;
use Qup\Checkout\Contracts\CheckoutInfo;
use Qup\Checkout\Contracts\PaymentMethods;
use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Qup\Checkout\Order\Contracts\OrderOpen;

abstract class PaymentMethodController extends BaseCheckoutController
{
    /**
     * Returns all available payment methods.
     */
    public function getPaymentMethods(PaymentMethods $paymentMethods)
    {
        return self::createSuccessResponse($paymentMethods);
    }

    /**
     * Returns the currently selected payment method.
     * @param PaymentMethods $paymentMethods
     * @param CheckoutInfo $checkoutInfo
     */
    public function getPaymentMethod(PaymentMethods $paymentMethods, CheckoutInfo $checkoutInfo)
    {
        $paymentMethodId = $checkoutInfo->getPaymentMethodId();

        foreach ($paymentMethods as $paymentMethod) {
            if ($paymentMethod->type_id == $paymentMethodId || $paymentMethod->subtype_id == $paymentMethodId) {
                return self::createSuccessResponse($paymentMethod);
            }
        }

        return self::createErrorResponse('PaymentMethod not set/found', Response::HTTP_NOT_FOUND, self::CODE_ERR_ITEM_NOT_FOUND);
    }

    /**
     * Selects the supplied payment method
     *
     * @param Request $request
     * @param CheckoutInfo $checkoutInfo
     * @param Container $container
     */
    public function putPaymentMethod(Request $request, PaymentCatalog $catalog, CheckoutInfo $checkoutInfo, Container $container)
    {
        $validator = $this->getPutPaymentMethodValidator($request->all());

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $method = $catalog->paymentMethods->getWith('getMethod', $request->get('type_id'));
        if (empty($method)) {
            throw new \InvalidArgumentException('Supplied method does not exist');
        }

        $checkoutInfo->setPaymentMethodId($method->getMethod());
        $checkoutInfo->setPaymentPlatformId($method->getPSP());

        // Call it through the service container to meet all dependencies
        return $container->call([$this, 'getPaymentMethod']);
    }

    /**
     * Confirms the payment
     *
     * @param Request $request
     * @param OrderOpen $order
     */
    public function postPaymentMethod(Request $request, OrderOpen $order, CheckoutInfo $info, PaymentCatalog $catalog)
    {
        // If we are here, conversion has been succeeded and we have a transaction ready.
        // Now all we have to do is return the URL that should be redirected to.

        $orderInstance = $order->instance();

        $activeTransactions = TransactionHandler::getActiveDebitTransactions($orderInstance->transactions);

        $transaction = $activeTransactions->begin();

        // @todo determine what the fallback should be, or add errorhandling
        $url = '';

        if (!empty($transaction)) {
            // opens transaction, return HTML for 3D secure
            //$html = $transaction->pay();
            // but we don return anything yet, so
            $transaction->pay();
            //
            $url = $transaction->getPaymentPageURL($orderInstance);
        }

        return self::createSuccessResponse(['url' => $url]);
    }

    /**
     * Makes then returns the Validator for the putVouchers request
     *
     * @param array $data
     * @return Validator
     */
    protected function getPutPaymentMethodValidator($data)
    {
        return \Validator::make($data, [
            'type_id' => 'required|in_payment_catalog|integer'
        ]);
    }

    /**
     * Handles the data supplied in redirects from external payment method configuration form submits (Ogone Hosted Tokenization page HTP)
     * @param Request $request
     * @param CheckoutInfo $checkoutInfo
     */
    public function parsePaymentMethodConfigurationResult(Request $request, CheckoutInfo $checkoutInfo)
    {
        $paymentPlatform = ObjectCacheController::get(ObjectCacheController::ITEM_PAYMENT_PLATFORM, $checkoutInfo->getPaymentPlatformId());

        $paymentPlatformParameter = new PaymentPlatformParameter();
        $paymentPlatformParameter->setRequest($request->input());

        $paymentPlatformResult = $paymentPlatform->callback(PaymentPlatformImplementation::EVENT_PAYMENTMETHOD_HANDLE_SUBMIT, $paymentPlatformParameter);

        $checkoutInfo->setPaymentAlias($paymentPlatformResult->getAlias());

        if ($paymentPlatformResult->getPaymentMethod()) {
            $checkoutInfo->setPaymentMethodId($paymentPlatformResult->getPaymentMethod());
        }

    }
}