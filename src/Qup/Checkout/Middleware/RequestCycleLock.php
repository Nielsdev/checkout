<?php

namespace Qup\Checkout\Middleware;

use Illuminate\Http\Request;
use \Closure;

class RequestCycleLock
{
    /**
     * @var string
     */
    private $orderId;

    /**
     * RequestCycleLock constructor.
     */
    public function __construct()
    {
        $this->orderId = session('order_id');
    }

    /**
     * Handles the RequestCycleLock
     *
     * @param Request $request
     * @param Closure $next
     * @return Closure
     */
    public function handle(Request $request, Closure $next)
    {
        if (empty($this->orderId)) {
            return $next($request);
        }

        \ObjectLock::id($this->orderId)->lockAndRetry(null, 'PT30S', '', 10, 1);

        $response = $next($request);

        \ObjectLock::id($this->orderId)->release();

        return $response;
    }
}