<?php

namespace Qup\Checkout\Middleware;

use \Closure;
use Illuminate\Http\Response;
use ProBiz\Core\DateTime;
use ProBiz\Order\OrderBase;
use \Qup\Checkout\Order\OrderFactory;
use \Illuminate\Http\Request;
use Qup\Checkout\Repositories\CheckoutInfoRepository;
use Qup\Checkout\Repositories\PromotionRepository;
use Qup\Promotions\Contracts\Input;


class OrderData
{

    /**
     * @var CheckoutInfoRepository
     */
    private $checkoutInfoRepository;

    /**
     * @var PromotionRepository
     */
    private $promotionRepository;

    /**
     * OrderData constructor.
     *
     * @param CheckoutInfoRepository $checkoutInfoRepository
     * @param PromotionRepository $promotionRepository
     */
    public function __construct(CheckoutInfoRepository $checkoutInfoRepository, PromotionRepository $promotionRepository)
    {
        $this->checkoutInfoRepository = $checkoutInfoRepository;
        $this->promotionRepository = $promotionRepository;
    }

    /**
     * Loads the order data for the supplied request
     *
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        $orderId = session('order_id');

        $order = $this->getOrderInstance($orderId);
        $request->session()->put('order_id', $order->getID());

        \App::singleton(\Qup\Checkout\Order\Contracts\OrderInstance::class, function() use ($order, $request) {
            return $order;
        });

        \App::singleton(\Qup\Checkout\Contracts\CheckoutInfo::class, function() {
            return $this->checkoutInfoRepository->fromSession();
        });

        \App::singleton(Input::class, function() {
            return $this->promotionRepository->fromSession();
        });

        \App::singleton(\Qup\Checkout\Catalog\Contracts\CartCatalog::class, \Qup\Checkout\Catalog\CartCatalog::class);

        $response =  $next($request);

        // We don't have to save when we are dealing with a get.
        if ($request->getMethod() == Request::METHOD_GET) {
            return $response;
        }

        $order = \App::make(\Qup\Checkout\Order\Contracts\OrderInstance::class);
        $order->save();

        // Load this, in case it has not been loaded before, then it would lose track of this part session.
        \App::make(\Qup\Checkout\Contracts\CheckoutInfo::class);
        \App::make(Input::class);

        $this->checkoutInfoRepository->save();
        $this->promotionRepository->save();

        return $response;
    }

    /**
     * Retrieves the current order instance, or creates a new ShoppingCart
     *
     * @param integer $orderId
     * @return OrderBase
     */
    protected function getOrderInstance($orderId)
    {
        if (!empty($orderId)) {
            try {
                $order = \ProBiz\Order\OrderFactory::load($orderId);

                return $order;
            } catch (\Exception $e) {
                // Gracefully allow, and make a new order
                \Log::debug('Trying to load expired/cleaned-up order ID='.$orderId);
            }
        }

        return OrderFactory::createNew();
    }
}