<?php

namespace Qup\Checkout;

use Illuminate\Http\Request;
use Qup\Checkout\Events\CheckoutInfoChanged;
class CheckoutInfo implements \Qup\Checkout\Contracts\CheckoutInfo
{
    /**#@+
     * @var integer
     */
    private $paymentPlatformId = null;
    private $paymentMethodId = null;
    private $transactionId = null;
    /**#@-*/

    /**#@+
     * @var string
     */
     private $alias = '';
    /**#@-*/

    /**
     * @var boolean
     */
    private $insurance = false;

    /**
     * @var string
     */
    private $wallet_secret = '';

    /**
     * Creates an instance of CheckoutInfo
     */
    public function __construct()
    {

    }

    /**
     * Sets the paymentPlatformId
     *
     * @param integer $paymentPlatformId
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setPaymentPlatformId($paymentPlatformId)
    {
        $this->paymentPlatformId = $paymentPlatformId;

        return $this;
    }

    /**
     * Retrieves the payment platform id
     *
     * @return integer
     */
    public function getPaymentPlatformId()
    {
        return $this->paymentPlatformId;
    }

    /**
     * Sets the paymentMethodId
     *
     * @param integer $paymentMethodId
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setPaymentMethodId($paymentMethodId)
    {
        $this->paymentMethodId = $paymentMethodId;

        return $this;
    }

    /**
     * Returns the payment method id
     * @return number
     */
    public function getPaymentMethodId()
    {
        return $this->paymentMethodId;
    }

    /**
     * Sets the active transactionId
     *
     * @param integer $transactionId
     */
    public function setActiveTransactionId($transactionId)
    {
        $this->transactionId = (int)$transactionId;
    }

    /**
     * Retrieves the active transactionId
     *
     * @return integer
     */
    public function getActiveTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Sets the insurance
     *
     * @param boolean $insurance
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setInsurance($insurance)
    {
        if ($this->insurance != $insurance) {
            $this->insurance = (bool)$insurance;
        }

        return $this;
    }

    /**
     * Returns the insurance
     *
     * @return boolean
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Sets the wallet secret
     *
     * @param string $secret
     */
    public function setWalletSecret($secret)
    {
        $this->wallet_secret = $secret;
    }

    /**
     * Returns the wallet sercret
     *
     * @return string
     */
    public function getWalletSecret()
    {
        return $this->wallet_secret;
    }

    /**
     * Sets the alias needed to call externally configured payment method settings
     *
     * @param string $alias
     * @return \Qup\Checkout\CheckoutInfo
     */
    public function setPaymentAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * Returns the alias needed to call externally configured payment method settings
     * @return string
     */
    public function getPaymentAlias()
    {
        return $this->alias;
    }

    /**
     * Converts CheckoutInfo to array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'paymentMethod' => $this->paymentMethodId,
            'paymentPlatform' => $this->paymentPlatformId,
            'paymentAlias' => $this->alias,
            'walletLogin' => $this->wallet_secret,
            'insurance' => $this->insurance
        ];
    }
}