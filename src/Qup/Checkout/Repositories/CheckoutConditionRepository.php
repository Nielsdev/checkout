<?php

namespace Qup\Checkout\Repositories;

use App\Models\CheckoutCondition;
use Illuminate\Http\Request;

class CheckoutConditionRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $conditions;

    /**
     * @var array
     */
    private $acceptedConditions;

    /**
     * @var Request
     */
    private $request;

    /**
     * CheckoutConditionRepository constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->conditions = $this->createCheckoutConditionCollection();

        $this->fromSession();
    }

    /**
     * Saves the current conditions
     */
    public function save()
    {
        $session = $this->request->session();

        $session->put('conditions', $this->acceptedConditions);
    }

    /**
     * Returns all conditions that have been accepted.
     *
     * @return array
     */
    public function getAcceptedConditions()
    {
        return $this->acceptedConditions;
    }

    /**
     * Indicated whether or not a condition has been accepted.
     *
     * @param $conditionId
     * @return bool
     */
    public function isAccepted($conditionId)
    {
        if (empty($this->acceptedConditions[$conditionId])) {
            return false;
        }

        return true;
    }

    /**
     * Returns whether or not the collection contains supplied conditionId.
     *
     * @param $conditionId
     * @return bool
     */
    public function has($conditionId)
    {
        return $this->conditions->has($conditionId);
    }

    /**
     * Sets whether or not a condition has been accepted.
     *
     * @throws \InvalidArgumentException
     * @param $conditionId
     * @param bool $value
     */
    public function setAccepted($conditionId, $value = true)
    {
        if (!$this->conditions->has($conditionId)) {
            throw new \InvalidArgumentException('Unexpected condition with ID=' . $conditionId);
        }

        $this->acceptedConditions[$conditionId] = $value;
    }

    /**
     * Creates a checkoutInfo from session
     */
    public function fromSession()
    {
        $session = $this->request->session();

        $this->acceptedConditions = (array)$session->get('conditions');
    }

    /**
     * Returns all applicable conditions.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllConditions()
    {
        return $this->conditions;
    }

    /**
     * Creates a CheckoutConditionCollection
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function createCheckoutConditionCollection()
    {
        return CheckoutCondition::all()->keyBy('id');
    }
}