<?php

namespace Qup\Checkout\Repositories;

use App\Models\CheckoutSubscription;
use Illuminate\Http\Request;

class CheckoutSubscriptionRepository
{
    /**
     * Options
     */
    const OPTION_VISIBLE = 2;
    const OPTION_PRECHECKED = 8;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $subscriptions;

    /**
     * @var int[]
     */
    private $subscribedSubscriptions;

    /**
     * CheckoutSubscriptionRepository constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->subscriptions = $this->createCheckoutSubscriptionCollection();

        $this->prepareCollection();

        $this->fromSession();
    }

    /**
     * Updates a subscription for current session.
     *
     * @param integer $subscriptionId
     * @param bool $subscribed
     */
    public function setSubscribed($subscriptionId, $subscribed = true)
    {
        if (!$this->subscriptions->has($subscriptionId)) {
            throw new \InvalidArgumentException('Unexpected subscription with ID=' . $subscriptionId);
        }

        if (!$subscribed && array_key_exists($subscriptionId, $this->subscribedSubscriptions)) {
            unset($this->subscribedSubscriptions[$subscriptionId]);
            return;
        }

        $this->subscribedSubscriptions[$subscriptionId] = $subscribed;
    }

    /**
     * Indicates whether or not a subscription has been subscribed to.
     *
     * @param $subscriptionId
     * @return bool
     */
    public function isSubscribed($subscriptionId)
    {
        return !empty($this->subscribedSubscriptions[$subscriptionId]);
    }

    /**
     * Saves the current subscriptions.
     */
    public function save()
    {
        $session = $this->request->session();

        $session->put('subscriptions', $this->subscribedSubscriptions);
    }

    /**
     * Creates a checkoutSubscription from session
     */
    public function fromSession()
    {
        $session = $this->request->session();

        $this->subscribedSubscriptions = (array)$session->get('subscriptions');
    }

    /**
     * Subscribes
     */
    protected function prepareCollection()
    {
        foreach ($this->subscriptions as $subscription)
        {
            if ($subscription->options & static::OPTION_PRECHECKED == static::OPTION_PRECHECKED) {
                $this->subscribedSubscriptions[$subscription->id] = true;
            }
        }
    }

    /**
     * Returns whether or not the collection contains supplied subscription.
     *
     * @param $subscriptionId
     * @return bool
     */
    public function has($subscriptionId)
    {
        return $this->subscriptions->has($subscriptionId);
    }

    /**
     * Returns all applicable conditions.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * Returns all conditions that have been accepted.
     *
     * @return int[]
     */
    public function getSubscribedSubscriptions()
    {
        return array_keys($this->subscribedSubscriptions);
    }

    /**
     * Creates a CheckoutConditionCollection
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function createCheckoutSubscriptionCollection()
    {
        $subscriptions = CheckoutSubscription::whereRaw(
            '(options & ' . static::OPTION_VISIBLE . ') = ' . static::OPTION_VISIBLE
        );

        $collection = $subscriptions->get()->keyBy('id');

        return $collection;
    }
}