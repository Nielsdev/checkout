<?php

namespace Qup\Checkout\Repositories;

use ProBiz\Order\Transaction\Transaction;
use ProBiz\Order\Transaction\TransactionNew;
use ProBiz\Order\Transaction\TransactionSucceeded;
use ProBiz\Payment\PaymentMethodFactory;
use ProBiz\Payment\PaymentPlatformImplementation;
use Qup\Checkout\Order\Contracts\OrderInstance;

class TransactionRepository
{
    /**
     * @var PaymentPlatformImplementation
     */
    private $paymentPlatform;

    /**
     * TransactionRepository constructor.
     *
     * @param PaymentPlatformImplementation $paymentPlatform
     */
    public function __construct()
    {
        /**
         * @see loadPaymentPlatformImplementation
         **/
    }

    /**
     * Due to issues with the (old) code in the class PaymentPlatformOgone that would directly query
     * the database upon construct, we can not use dependency injection through the constructor.
     * This would lead to database issues in a CI environment without the proper .env file.
     *
     * The following function will return the bound payment implementation.
     * @todo Fix the payment platform ogone to cease using the database upon construct, so this can be removed in favor of "real" depenency injection.
     *
     * @return PaymentPlatformImplementation
     */
    protected function loadPaymentPlatformImplementation()
    {
        return \App::make(PaymentPlatformImplementation::class);
    }

    /**
     * Partially refunds an order transaction.
     *
     * @param OrderInstance $order
     * @param TransactionSucceeded $originalTransaction
     * @param integer $amount
     * @param string $reason
     * @return integer|boolean
     */
    public function partiallyRefundTransaction (OrderInstance $order, TransactionSucceeded $originalTransaction, $amount, $reason = '')
    {
        if (round($amount, 2) > round($originalTransaction->getAmount(), 2)) {
            throw new \InvalidArgumentException('Amount exceeds maximum amount in the transaction');
        }

        if (empty($this->paymentPlatform)) {
            $this->paymentPlatform = $this->loadPaymentPlatformImplementation();
        }

        $transaction = new TransactionNew();
        $transaction->create($order->getID(), Transaction::TYPE_CREDIT, $amount, $this->paymentPlatform->getPSP(), PaymentMethodFactory::METHOD_OGONE_CREDIT, null, '', $reason);
        $transaction->setTransactionID($originalTransaction->getID());
        $transaction->save();

        return $this->paymentPlatform->sendCreditTransaction($transaction);
    }

    /**
     * Handles supplied transaction
     *
     * @param OrderInstance $order The order this refund belongs to
     * @param TransactionSucceeded $originalTransaction
     * @param string $reason The refund reason
     */
    public function refundTransaction(OrderInstance $order, TransactionSucceeded $originalTransaction, $reason = '')
    {
        $this->partiallyRefundTransaction($order, $originalTransaction, $originalTransaction->getAmount(), $reason);
    }
}