<?php

namespace Qup\Checkout\Repositories;

use Illuminate\Http\Request;
use Qup\Promotions\FunTrips\Input;

class PromotionRepository
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Input
     */
    private $promotionInput;

    /**
     * CheckoutInfoRepository constructor.
     *
     * @param Request $request
     * @param Input $promotionInput
     */
    public function __construct(Request $request, Input $promotionInput)
    {
        $this->promotionInput = $promotionInput;
        $this->request = $request;
    }

    /**
     * Retrieves the data from session/jwt.
     */
    public function fromSession()
    {
        $session = $this->request->session();
        $promotionData = (object)$session->get('promotions');

        if (isset($promotionData->digitalVouchers)) {
            $this->promotionInput->setDigitalVouchers($promotionData->digitalVouchers);
        }

        if (isset($promotionData->paperVouchers)) {
            $this->promotionInput->setPaperVouchers($promotionData->paperVouchers);
        }

        if (isset($promotionData->voucherGroup)) {
            $this->promotionInput->setVoucherGroup($promotionData->voucherGroup);
        }

        if (isset($promotionData->maxVouchers)) {
            $this->promotionInput->setMaxVouchers($promotionData->maxVouchers);
        }

        return $this->promotionInput;
    }

    /**
     * Saves the data in the JWT
     */
    public function save()
    {
        $session = $this->request->session();

        $data = [];
        $data['digitalVouchers'] = $this->promotionInput->getDigitalVouchers();
        $data['paperVouchers'] = $this->promotionInput->getPaperVouchers();
        $data['voucherGroup'] = $this->promotionInput->getVoucherGroup();
        $data['maxVouchers'] = $this->promotionInput->getMaxVouchers();

        $session->put('promotions', $data);
    }
}