<?php

namespace Qup\Checkout\Repositories;

use Illuminate\Http\Request;
use Qup\Checkout\CheckoutInfo;

class CheckoutInfoRepository
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var CheckoutInfo
     */
    private $checkoutInfo;

    /**
     * CheckoutInfoRepository constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request, CheckoutInfo $checkoutInfo)
    {
        $this->request = $request;
        $this->checkoutInfo = $checkoutInfo;
    }

    /**
     * Saves the current checkoutInfo
     */
    public function save()
    {
        $session = $this->request->session();

        $session->put('insurance', $this->checkoutInfo->getInsurance());
        $session->put('paymentMethod', $this->checkoutInfo->getPaymentMethodId());
        $session->put('paymentPlatform', $this->checkoutInfo->getPaymentPlatformId());
        $session->put('paymentAlias', $this->checkoutInfo->getPaymentAlias());
        $session->put('walletLogin', $this->checkoutInfo->getWalletSecret());
        $session->put('activeTransactionId', $this->checkoutInfo->getActiveTransactionId());
    }

    /**
     * Creates a checkoutInfo from session
     *
     * @return CheckoutInfo
     */
    public function fromSession()
    {
        $session = $this->request->session();

        if ($session->has('insurance')) {
            $this->checkoutInfo->setInsurance($session->get('insurance'));
        }

        if ($session->has('paymentMethod')) {
            $this->checkoutInfo->setPaymentMethodId($session->get('paymentMethod'));
        }

        if ($session->has('paymentPlatform')) {
            $this->checkoutInfo->setPaymentPlatformId($session->get('paymentPlatform'));
        }

        if ($session->has('paymentAlias')) {
            $this->checkoutInfo->setPaymentAlias($session->get('paymentAlias'));
        }

        if ($session->has('walletLogin')) {
            $this->checkoutInfo->setWalletSecret($session->get('walletLogin'));
        }

        if ($session->has('activeTransactionId')) {
            $this->checkoutInfo->setActiveTransactionId($session->get('activeTransactionId'));
        }

        return $this->checkoutInfo;
    }

}