<?php

namespace Qup\Checkout\Catalog;

use ProBiz\Core\Collection\ObjectCollection;
use Qup\Checkout\Catalog\Contracts\CartCatalog;
use Qup\Checkout\Models\ShopLocationMainProduct;

class SalesCatalog
{
    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * @var ObjectCollection
     */
    private $collection;

    /**
     * SalesCatalog constructor.
     *
     * @param CartCatalog $catalog
     */
    public function __construct(CartCatalog $catalog)
    {
        $this->catalog = $catalog;
    }

    /**
     * Initializes the SalesCatalog.
     */
    private function initialize()
    {
        $this->collection = $this->createBaseCollection();
    }

    /**
     * Returns the "base" collection of all relevant Sales products.
     *
     * @return ObjectCollection
     */
    protected function createBaseCollection()
    {
        $collection = new ObjectCollection(\Qup\Checkout\Models\ShopLocationMainProduct::class);
        $collection->setListener(ObjectCollection::EVENT_INITIALIZE, 'onInitializeBaseCollection', $this, TRUE);

        return $collection;
    }

    /**
     * Initializes the base collection
     *
     * @param $event
     * @param $key
     * @param ObjectCollection $collection
     */
    public function onInitializeBaseCollection($event, $key, ObjectCollection $collection)
    {
        $relatedProducts = [];

        $mainProductsIds = $this->catalog->getShopLocationMainProductIds();

        foreach ($mainProductsIds as $mainProductId) {
            $mainProduct = ShopLocationMainProduct::find($mainProductId);

            foreach($mainProduct->relatedProducts as $relatedProduct) {
                $collection->add($relatedProduct);
            }
        }
    }

    /**
     * @return ObjectCollection
     */
    public function getCrossSellProducts()
    {
        /**
         * So, the deal here is when we are going to support multiple types,
         * We will simply build a new collection herein.
         *
         * For now..... @todo
         */
        $this->initialize();

        return $this->collection;
    }
}