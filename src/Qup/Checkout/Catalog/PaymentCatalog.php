<?php

namespace Qup\Checkout\Catalog;

use \ProBiz\Core\Collection\ObjectCollection;
use \ProBiz\Payment\PaymentPlatformFactory;
use \ProBiz\Payment\PaymentPlatformImplementation;
use \ProBiz\Payment\PaymentMethodImplementation;
use \Qup\Checkout\Events\PaymentPlatformsLoaded;
use \Qup\Checkout\Models\PaymentPlatform;
use Qup\Checkout\Events\PaymentMethodsLoaded;
use Qup\Checkout\Models\PaymentMethod;

class PaymentCatalog
{
    /**
     * Collection containing paymentMethods
     *
     * @var ObjectCollection
     */
    public $paymentMethods;
    public $paymentPlatforms;

    /**
     * Creates a new instance of PaymentCatalog
     */
    public function __construct()
    {
        $this->initialize();
    }

    private function initialize()
    {
        $this->paymentPlatforms = $this->createPaymentPlatformCollection();
        $this->paymentMethods = $this->createPaymentMethodCollection();
    }
    /**
     * Creates and returns the ObjectCollection that contains the PaymentMethod objects.
     *
     * @return ObjectCollection
     */
    private function createPaymentPlatformCollection()
    {
        $collection = new ObjectCollection(PaymentPlatformImplementation::class);
        $collection->setListener(ObjectCollection::EVENT_INITIALIZE, 'onInitializePaymentPlatforms', $this, true);

        return $collection;
    }

    /**
     * Creates and returns the ObjectCollection that contains the PaymentMethod objects.
     *
     * @return ObjectCollection
     */
    private function createPaymentMethodCollection()
    {
        $collection = new ObjectCollection(PaymentMethodImplementation::class);
        $collection->setListener(ObjectCollection::EVENT_INITIALIZE, 'onInitializePaymentMethods', $this, true);

        return $collection;
    }

    /**
     * Initializes the paymentPlatforms
     *
     * @param integer $event
     * @param mixed $key
     * @param ObjectCollection $sender
     */
    public function onInitializePaymentPlatforms($event, $key, ObjectCollection $sender)
    {
        $platforms = PaymentPlatform::all();

        \Event::fire(new PaymentPlatformsLoaded($platforms, $sender));
    }

    /**
     * Initializes the paymentMethods
     *
     * @param integer $event
     * @param mixed $key
     * @param ObjectCollection $sender
     */
    public function onInitializePaymentMethods($event, $key, ObjectCollection $sender)
    {
        $methods = [];

        foreach ($this->paymentPlatforms as $paymentPlatformImplementation) {
            foreach ($paymentPlatformImplementation->paymentMethods as $paymentMethodImplementation) {
                $methods[] = $paymentMethodImplementation;
            }
        }

        \Event::fire(new PaymentMethodsLoaded($methods, $sender));
    }
}