<?php

namespace Qup\Checkout\Catalog\Contracts;

interface CartCatalog
{
    /**
     * Returns the ShopLocationMainProducts collection
     *
     * @return \ProBiz\Core\Collection\ObjectCollection
    */
    public function getShopLocationMainProducts();
    
    /**
     * Sets the data for this context.
     * This array is validated by it's container.
     *
     * @param array $data
    */
    public function setData($data);
    
    /**
     * Returns the visit time
     *
     * @return string Containing the visit time
    */
    public function getVisitTime();
    
    /**
     * Returns the visit time
     *
     * @return string Containing the visit time
    */
    public function getVisitDate();
    
    /**
     * Retrieves all ShopLocationMainProductID
     *
     * @return array Containing all IDs
    */
    public function getShopLocationMainProductIds();

    /**
     * Returns whether or not the catalog contains supplied dateTimeRankSubProduct
     *
     * @param integer $dateTimeRankSubProductId
    */
    public function has($dateTimeRankSubProductId);
    
    /**
     * Retrieves and converts product with supplied ID
     *
     * @param integer $id
     * @return null|\MrTicket\Product\DateTimeRankSubProduct
    */
    public function getProduct($id);

    /**
     * @return mixed
     */
    public function cartProducts();

    /**
     * @return mixed
     */
    public function relatedProducts();
}