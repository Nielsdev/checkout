<?php

namespace Qup\Checkout\Catalog;

use MrTicket\Product\ShopLocationMainProduct;
use ProBiz\Core\DateTime;
use Qup\Checkout\Order\Contracts\OrderInstance;
use ProBiz\Order\OrderLineHandler;
use Qup\Checkout\Definitions\CartProduct;
use Qup\Checkout\Collections\CartProductCollection;
use ProBiz\Order\OrderLine;
use ProBiz\Cache\ObjectCacheController;

class OrderCatalog
{
    private $order;

    /**
     * Creates a new instance of OrderCatalog
     *
     * @param Order $order
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * Retrieves the cart Products in this order context.
     *
     * @return array containing cartProducts
     */
    public function getCartProducts()
    {
        $cartProducts = new CartProductCollection();
        $order = $this->order;

        foreach ($order->orderLines as $orderLine) {

            if ($orderLine->getType() != OrderLine::TYPE_PRODUCT) {
                continue;
            }

            if (!OrderLineHandler::isEffectivelyActive($orderLine, $order->orderLines)) {
                continue;
            }

            $shopId = $orderLine->getShopID();
            $mainProductId = $orderLine->getShopLocationMainProductId();

            $shop = ObjectCacheController::get(ObjectCacheController::ITEM_SHOP, $shopId);
            $mainProduct = ObjectCacheController::get(ObjectCacheController::ITEM_SHOPLOCATIONMAINPRODUCT, $mainProductId);

            /**
             * @var OrderLine $orderLine
             */
            $cartProduct = CartProduct::create(
                $orderLine->getDateTimeRankSubProductID(),
                $orderLine->getProductFulfillmentTitle(),
                $mainProduct->getDescription(),
                $orderLine->getProductRankTitle(),
                $shop->getTitle(),
                $orderLine->getProductLocationTitle(),
                $orderLine->getQuantity(),
                $orderLine->getUnitPriceInc(),
                $orderLine->getProductTime()->format(DateTime::TIME_ONLY),
                $mainProduct->optionsBits->checkOptions(ShopLocationMainProduct::OPTION_CROSSSELL),
                $orderLine->getShopLocationSubProductID()
            );

            $cartProducts->push($cartProduct);
        }

        return $cartProducts;
    }
}