<?php

namespace Qup\Checkout\Catalog;

use ProBiz\Core\Collection\ObjectCollection;
use Qup\Checkout\Events\DateTimeRankSubProductsLoaded;
use Qup\Checkout\Events\ShopLocationMainProductsLoaded;
use Qup\Checkout\Events\CatalogChanged;
use ProBiz\Core\DateTime;
use Qup\Checkout\Models\CatalogProduct;
use Qup\Checkout\Models\DateTimeRankSubProduct;
use Qup\Checkout\Models\ShopLocationMainProduct;
use Illuminate\Database\Eloquent\Collection;

class CartCatalog implements Contracts\CartCatalog, \IteratorAggregate, \Countable
{
    /**#@+
     * @var \ProBiz\Core\Collection\ObjectCollection
     */
    private $shopLocationMainProducts;
    private $catalogProducts;
    /**#@-*/

    /**
     * @var array
     */
    private $shopLocationMainProductIds = [];

    /**
     * @var \ProBiz\Core\DateTime
     */
    private $visitDate = null;

    /**
     * @var bool
     */
    private $initialized = false;

    /**
     * Creates an instance of CartCatalog
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * Initializes the CartCatalog
     */
    private function initialize()
    {
        $this->shopLocationMainProducts = $this->createShopLocationMainProductsCollection();
        $this->catalogProducts  = $this->createCatalogProductCollection();
    }

    /**
     * Creates and returns the ObjectCollection that contains the MainProduct objects.
     *
     * @return \ProBiz\Core\Collection\ObjectCollection
     */
    private function createShopLocationMainProductsCollection()
    {
        $collection = new ObjectCollection('\Qup\Checkout\Models\ShopLocationMainProduct');
        $collection->setListener(ObjectCollection::EVENT_INITIALIZE, 'onInitializeShopLocationMainProducts', $this, TRUE);

        return $collection;
    }

    /**
     * Handles populating the ShopLocationMainProducts
     *
     * @param integer $iEvent the event to be raised
     * @param mixed $mKey The key of the affected item
     * @param ObjectCollection $oSender The collection that raised the event
     */
    public function onInitializeShopLocationMainProducts($iEvent, $mKey, $oSender)
    {
        $collection = ShopLocationMainProduct::whereIn('id', $this->shopLocationMainProductIds)->get();
        \Event::fire(new ShopLocationMainProductsLoaded($collection));

        $oSender->addFromArray($collection->flatten()->all());
    }

    /**
     * Creates and returns the ObjectCollection that contains the CartCatalog objects.
     *
     * @return \ProBiz\Core\Collection\ObjectCollection
     */
    private function createCatalogProductCollection()
    {
        $collection = new ObjectCollection('\Qup\Checkout\Models\CatalogProduct');
        $collection->setListener(ObjectCollection::EVENT_INITIALIZE, 'onInitializeCatalogProducts', $this, TRUE);

        return $collection;
    }

    /**
     * Handles populating the DateTimeRankSubProducts
     *
     * @param integer $iEvent the event to be raised
     * @param mixed $mKey The key of the affected item
     * @param ObjectCollection $oSender The collection that raised the event
     */
    public function onInitializeCatalogProducts($iEvent, $mKey, $oSender)
    {
        $shopLocationMainProductIds = $this->shopLocationMainProducts->toIDArray();
        $builder = CatalogProduct::whereIn('shop_location_main_product_id', $shopLocationMainProductIds);

        if (!empty($this->getVisitDate())) {
            $builder->where(function($query) {
                $query->where('date', $this->getVisitDate())
                    ->orWhere('no_date', true);
            });
        }

        if (!empty($this->getVisitTime())) {
            $builder->where(function($query) {
                $query->where('time', $this->getVisitTime())
                    ->orWhere('no_time', true);
            });
        }

        $collection = $builder->orderBy('date')->get();

        \Event::fire(new DateTimeRankSubProductsLoaded($collection));

        $oSender->addFromArray($collection->flatten()->all());

        $this->initialized = true;
    }

    /**
     * Returns the ShopLocationMainProducts collection
     *
     * @return \ProBiz\Core\Collection\ObjectCollection
     */
    public function getShopLocationMainProducts()
    {
        return $this->shopLocationMainProducts;
    }

    /**
     * Sets the data for this context.
     * This array is validated by it's container.
     *
     * @param array $data
     */
    public function setData($data)
    {
        if ($this->shopLocationMainProductIds != $data['shopLocationMainProducts']) {
            $this->shopLocationMainProductIds = $data['shopLocationMainProducts'];
        }

        if ($data['date'] instanceof DateTime) {
            $date = $data['date'];
        } elseif (!empty($data['date']) && !empty($data['time'])) {
            $timestamp = $data['date'] . ' ' . $data['time'];
            $date = new DateTime($timestamp);
        } elseif (!empty($data['date'])) {
            $date = new DateTime($data['date']);
            $date->setDateOnly(true);
        } else {
            $date = new DateTime(DateTime::NOT_SET);
            $date->setDateOnly(true);
        }

        $this->visitDate = $date;

        $this->initialize();

        $this->catalogProducts->count();

        return $this;
    }

    /**
     * Returns the visit time
     *
     * @return string Containing the visit time
     */
    public function getVisitTime()
    {
        if ($this->visitDate->isDateOnly()) {
            return null;
        }

        return $this->visitDate->format(DateTime::TIME_ONLY);
    }

    /**
     * Returns the visit time
     *
     * @return string Containing the visit time
     */
    public function getVisitDate()
    {
        return $this->visitDate->format(DateTime::DATE_ONLY);
    }

    /**
     * Retrieves all ShopLocationMainProductID
     *
     * @return array Containing all IDs
     */
    public function getShopLocationMainProductIds()
    {
        return $this->shopLocationMainProductIds;
    }

    /**
     * Returns whether or not the catalog contains supplied dateTimeRankSubProduct
     *
     * @param integer $dateTimeRankSubProductId
     */
    public function has($dateTimeRankSubProductId)
    {
        return $this->catalogProducts->existsWithID($dateTimeRankSubProductId);
    }

    /**
     * Returns whether or not the CartCatalog is initialized.
     *
     * @return bool
     */
    public function isInitialized()
    {
        return $this->initialized;
    }

    /**
     * Retrieves and converts product with supplied ID
     *
     * @param integer $id
     * @return null|\MrTicket\Product\DateTimeRankSubProduct
     */
    public function getProduct($id)
    {
        $product = $this->catalogProducts->getWithID($id);

        if ($product == null) {
            return null;
        }

        $dateTimeRankSubProduct = new \MrTicket\Product\DateTimeRankSubProduct();
        $dateTimeRankSubProduct->loadFromModel($product);

        return $dateTimeRankSubProduct;
    }

    /**
     * @return mixed
     */
    public function count()
    {
        return $this->catalogProducts->count();
    }

    /**
     * @return mixed
     */
    public function relatedProducts()
    {
        $related = $this->catalogProducts->getAllWithAttribute('related', true);

        return $related;
    }

    /**
     * @return mixed
     */
    public function cartProducts()
    {
        $catalog = $this->catalogProducts->getAllWithAttribute('related', false);

        return $catalog;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return  new \ArrayIterator($this->catalogProducts->toArray());
    }
}