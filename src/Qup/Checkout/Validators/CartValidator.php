<?php

namespace Qup\Checkout\Validators;

use Illuminate\Validation\Validator;

class CartValidator
{
    /**
     * Validates the cart PUT call
     *
     * @param string $attribute
     * @param mixed $value
     * @param string $parameters
     * @param Validator $validator
     */
    public function isValidCartPut($attribute, $value, $parameters, $validator)
    {
        // Reset other rules.
        $validator->setRules([
            'insurance' => 'sometimes|boolean'
        ]);

        $validator->each($attribute, [
            'id' => 'required|in_catalog',
            'quantity' => 'required|integer'
        ]);

        return $validator->passes();
    }
}