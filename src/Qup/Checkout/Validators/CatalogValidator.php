<?php

namespace Qup\Checkout\Validators;

class CatalogValidator
{
    /**
     * Validates whether or not the current CartCatalog has supplied DateTimeRankSubProduct.
     * 
     * @param string $attribute
     * @param mixed $value
     * @param string $parameters
     * @param string $validator
     */
    public function isDateTimeRankSubProductsInCatalog($attribute, $value, $parameters, $validator)
    {
        return \CartCatalog::has($value);
    }

    /**
     * Validates whether or not the supplied PaymentMethod is available.
     *
     * @param Instance\CartCatalog $catalog
     * @param string $attribute
     * @param mixed $value
     * @param string $parameters
     * @param string $validator
     */
    public function isPaymentMethodInCatalog($attribute, $value, $parameters, $validator)
    {
        return true;
    }
}