<?php

namespace Qup\Checkout\Containers;

use Illuminate\Contracts\Validation\ValidationException;
use MrTicket\Product\DateTimeRankSubProduct;
use Qup\Checkout\Catalog\Contracts\CartCatalog;

class CartCatalogContainer implements \JsonSerializable
{
    /**
     * Variable containing the cart catalog
     *
     * @var CartCatalog
     */
    private $catalog;

    /**
     * Creates a new instance of CartCatalogContainer
     */
    public function __construct(CartCatalog $catalog)
    {
        $this->catalog = $catalog;
    }

    /**
     * Returns the current instance
     * 
     * @return CartCatalog
     */
    public function instance()
    {
        return $this->catalog;
    }

    /**
     * Sets the data array
     * 
     * @param array $data
     * @throws ValidationException When not all required arguments are supplied.
     * @return CartCatalogContainer This instance.
     */
    public function with($data = [])
    {
        $this->validateData($data);

        $this->catalog->setData($data);

        return $this;
    }

    /**
     * Validates if enough information has been supplied to built the catalog
     * 
     * @param array $data
     * @throws ValidationException When not all required arguments are supplied.
     */
    private function validateData($data)
    {
        $data= array_filter($data);

        $validator = \Validator::make($data, [
            'shopLocationMainProducts' => 'required|array|exists:shop_location_main_products,id',
            'date' => 'sometimes|required|date_format:Y-m-d',
            'time' => 'sometimes|required|date_format:H:i'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * Retrieves a DateTimeRankSubProduct
     * 
     * @param integer $dateTimeRankSubProductId
     * @return DateTimeRankSubProduct
     */
    public function get($dateTimeRankSubProductId)
    {
        $dateTimeRankSubProductModel = $this->getModel($dateTimeRankSubProductId);

        if (empty($dateTimeRankSubProductModel)) {
            return null;
        }

        $dateTimeRankSubProduct = new DateTimeRankSubProduct();
        $dateTimeRankSubProduct->loadFromModel($dateTimeRankSubProductModel);

        return $dateTimeRankSubProduct;
    }

    /**
     * Retrieves a DateTimeRankSubProduct
     * 
     * @param integer $dateTimeRankSubProductId
     * @return DateTimeRankSubProduct
     */
    public function getModel($dateTimeRankSubProductId)
    {
        return $this->catalog->dateTimeRankSubProducts->getWithID($dateTimeRankSubProductId);
    }

    /**
     * Returns the serialized catalog
     */
    public function jsonSerialize()
    {
        return $this->catalog->jsonSerialize();
    }

    /**
     * All unknown calls are forwarded to the actual CheckoutProcess instance.
     *
     * @param string $method
     * @param array $args
     */
    public function __call($method, $args = [])
    {
        /**
         * @todo Revise and replace with call_user_func_array replacement helper.
         */
        return call_user_func_array([$this->catalog, $method], $args);
    }
}