<?php

namespace Qup\Checkout\Tasks;

use ProBiz\Fulfillment\FulfillmentOrder;

class ProcessTicketFulfillmentOrders extends ProcessFulfillmentOrders
{
    /**
     * Registers the triggers.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    protected function registerTriggers(\Illuminate\Events\Dispatcher $events)
    {
        parent::registerTriggers($events);

        $events->listen('assign.barcodes', \Qup\Fulfillment\Listeners\AssignBarcodesToOrder::class);
        $events->listen('create.ticket', \Qup\Fulfillment\Listeners\StartOrderFulfillment::class);
        $events->listen('send.ticketmail', \Qup\Checkout\Order\Mails\Listeners\SendTicketMail::class);
    }

    /**
     * Processes the fulfillmentOrder
     *
     * @param FulfillmentOrder $fulfillmentOrder
     * @return boolean
     */
    protected function process(FulfillmentOrder $fulfillmentOrder)
    {
        try {
            // Assign barcodes
            $this->events->fire('assign.barcodes');

            // Create ticket
            $this->events->fire('create.ticket');
        } catch (\Exception $ex) {
            // @todo proper logging
            return false;
        }

        return true;
    }
}