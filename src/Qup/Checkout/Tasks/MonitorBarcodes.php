<?php

namespace Qup\Checkout\Tasks;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use ProBiz\Cache\ObjectCacheController;
use ProBiz\Core\Setting\SettingsFactory;
use ProBiz\Core\Site\SiteSettings;
use Qup\Checkout\Order\Mails\Request\BarcodeAlertMailRequest;
use Qup\MailClient\Request\MailMessage;
use Qup\MailClient\Request\MailRequest;
use Qup\Queue\Dispatcher;

/**
 * Class MonitorBarcodes
 * @package Qup\Checkout\Tasks
 */
class MonitorBarcodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'barcodes:monitor';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Monitors barcodes and delivers email of below threshold groups';

    /**
     * @var BarcodeAlertMailRequest
     */
    private $request;

    /**
     * MonitorBarcodes constructor.
     *
     * @param BarcodeAlertMailRequest $request
     */
    public function __construct(BarcodeAlertMailRequest $request)
    {
        parent::__construct();
        $this->request = $request;
    }

    /**
     *  Handles this task
     */
    public function handle()
    {

        $barcodeGroups = \DB::connection('pgsql_barcode')->table('tbl_barcode_groups')
            ->whereNotNull('capacity_warning_threshold')
            ->where('capacity_warning_threshold', '>', 0)
            ->whereNull('deleted_at')
            ->get();

        $barcodeGroupsBelowThreshold = $this->getBarcodeGroupsBelowThreshold($barcodeGroups);

        if (empty($barcodeGroupsBelowThreshold)) {
            return;
        }

        $this->sendEmail($barcodeGroupsBelowThreshold);
    }

    /**
     * @param array[] $barcodeGroups
     *
     * @return array[]
     */
    private function getBarcodeGroupsBelowThreshold($barcodeGroups)
    {
        $barcodeGroupsBelowThreshold = [];

        foreach ($barcodeGroups as $barcodeGroup) {

            $currentBarcodeTotal = \DB::connection('pgsql_barcode')->table('tbl_barcodes')
                ->where('barcode_group_id', $barcodeGroup["id"])
                ->where('used', false)
                ->where('blocked', false)
                ->whereNull('deleted_at')
                ->whereNull('order_line_id')
                ->count();

            if ($currentBarcodeTotal > $barcodeGroup["capacity_warning_threshold"]) {
                continue;
            }

            $barcodeGroupsBelowThreshold[$barcodeGroup["name"]] = $currentBarcodeTotal;
        }

        return $barcodeGroupsBelowThreshold;
    }

    /**
     * Send an email detailing actionable barcode groups
     *
     * @param array[] $actionableBarcodes
     */
    private function sendEmail($actionableBarcodes)
    {
        $siteSettings = ObjectCacheController::get(ObjectCacheController::ITEM_SETTINGS, SettingsFactory::TYPE_SITE);
        $recipient = $siteSettings->getValueIfSet(SiteSettings::ITEM_EMAIL_MONITORING, SiteSettings::CATEGORY_SHOP_GENERAL, 'barcodealarm@queueup.eu');

        $placeholders = [
            'actionable_barcodes' => $actionableBarcodes
        ];

        // Create job
        $message = new MailMessage(
            MailMessage::TYPE_BARCODE_ALERT,
            'BARCODE',
            'en_US',
            'I have barcodegroups that need some loving',
            $placeholders,
            $recipient,
            'Barcode Watchers',
            "barcodealarm@queueup.eu",
            'Cody your friendly barcode assistant'
        );

        // Send message
        $dispatcher = App::make(Dispatcher::class);
        $dispatcher->dispatch($message, MailRequest::CHANNEL_SEND_MAIL);
    }

}