<?php

namespace Qup\Checkout\Tasks;

use Illuminate\Console\Command;
use Illuminate\Container\Container;
use ProBiz\Order\OrderFactory;
use ProBiz\Order\OrderStatus;
use ProBiz\Order\Transaction\TransactionHandler;
use ProBiz\Order\Transaction\TransactionStatus;
use Qup\Checkout\Order\Contracts\OrderOpen;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

class ProcessOpenOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes all open orders';

    /**
     * @var Container
     */
    private $container;

    /**
     * CleanupOrders constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct();

        $this->container = $container;
    }

    /**
     * Handles this task
     */
    public function handle()
    {
        $site = \DB::select('SELECT get_site_id()');

        // Set current site scope (probiz compat)
        session()->put('Site_ID', $site[0]['get_site_id']);

        $this->handleOpenOrders();
    }

    /**
     * Handles the failing of orders
     */
    protected function handleOpenOrders()
    {
        $orders = $this->getOpenOrders();

        foreach ($orders as $order) {
            try {
                $orderInstance = $this->bindOrder($this->container, $order['id']);
            }
            catch (\Exception $ex) {
                // No load, no glory. skip.
                continue;
            }

            $this->processExternalStatus($orderInstance);

            \Log::debug(sprintf('Order %d has been processed.', $order['id']));
        }
    }

    /**
     * Retrieves the external status.
     *
     * @param $order
     */
    protected function processExternalStatus($order)
    {
        foreach ($order->transactions as $transaction) {
            for ($i = 0; $i < 3; $i++) {
                TransactionHandler::updateActiveTransactions($order);

                if (!in_array($transaction->getStatus(), TransactionStatus::getOpenStatuses())) {
                    \Event::fire(new \Qup\Checkout\Events\TransactionStatusChanged($transaction, $order));
                    continue 2;
                }

                sleep(2);
            }
        }
    }
    /**
     * @param Container $container
     * @param $orderId
     * @throws NotInPrerequisiteStateException
     */
    protected function bindOrder(Container $container, $orderId)
    {
        $order = OrderFactory::load($orderId);

        if (!($order instanceof OrderOpen)) {
            throw new NotInPrerequisiteStateException('OrderOpen expected');
        }

        $container->singleton(\Qup\Checkout\Order\Contracts\OrderInstance::class, function() use($order) {
            return $order;
        });

        return $order;
    }

    /**
     * Retrieves the expired orders.
     *
     * @return mixed
     */
    protected function getOpenOrders()
    {
        $sessionLength = config('jwt.ttl');

        $date = new \DateTime();
        $date_interval = new \DateInterval('PT' . $sessionLength . 'M');
        $date->sub($date_interval);

        $ordersBuilder = \DB::table('orders')->where('updated_at', '<', $date->format(\DateTime::ISO8601));
        $ordersBuilder->where('status', [OrderStatus::OPEN]);

        return $ordersBuilder->get(['id']);
    }
}