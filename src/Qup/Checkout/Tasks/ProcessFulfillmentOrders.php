<?php

namespace Qup\Checkout\Tasks;

use Illuminate\Console\Command;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use ProBiz\Fulfillment\FulfillmentOrder;
use ProBiz\Fulfillment\FulfillmentOrderStatus;
use ProBiz\Fulfillment\FulfillmentOrderStatusMutation;
use ProBiz\Fulfillment\Ticket\TicketFulfillmentOrder;
use ProBiz\Order\OrderFactory;
use ProBiz\Order\OrderStatus;
use Qup\Checkout\Order\Contracts\OrderFailed;
use Symfony\Component\Process\Exception\InvalidArgumentException;

abstract class ProcessFulfillmentOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fulfillment:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes fulfillment orders that are not fulfilled yet';

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Events
     */
    protected $events;

    /**
     * @var int[]
     */
    private $unprocessable_statuses = [
        FulfillmentOrderStatus::COMPLETED,
        FulfillmentOrderStatus::CANCELLED,
        FulfillmentOrderStatus::REFUSED
    ];

    /**
     * CleanupOrders constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container, \Illuminate\Events\Dispatcher $events)
    {
        parent::__construct();

        $this->registerTriggers($events);
        $this->container = $container;
        $this->events = $events;
    }

    /**
     * Registers the triggers.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    protected function registerTriggers(\Illuminate\Events\Dispatcher $events)
    {
        $events->listen('create.fulfillment', \Qup\Fulfillment\Listeners\CreateOrderFulfillment::class);
//        $events->listen('assign.barcodes', \Qup\Fulfillment\Listeners\AssignBarcodesToOrder::class);
//        $events->listen('create.ticket', \Qup\Fulfillment\Listeners\StartOrderFulfillment::class);
//        $events->listen('send.ticketmail', \Qup\Checkout\Order\Mails\Listeners\SendTicketMail::class);
    }

    /**
     * Handles this task
     */
    public function handle()
    {
        $site = \DB::select('SELECT get_site_id()');

        // Set current site scope (probiz compat)
        session()->put('Site_ID', $site[0]['get_site_id']);

        $this->handleExpiredFulfillmentOrders();
    }

    /**
     * Handles the failing of orders
     */
    protected function handleExpiredFulfillmentOrders()
    {
        $orders = $this->getExpiredFulfillmentOrders();

        foreach ($orders as $order) {
            $order = $this->bindOrder($this->container, $order['id']);

            $firstFulfillmentOrder = $order->fulfillmentOrders->begin();
            if (empty($firstFulfillmentOrder)) {
                // Missing fulfillment order, let's attempt to correct this.
                $this->events->fire('create.fulfillment');
            }

            $firstFulfillmentOrder = $order->fulfillmentOrders->begin();
            if (empty($firstFulfillmentOrder)) {

                \Log::error(sprintf('Unable to create FulfillmentOrder for order =  %d', $order['id']));
                continue;
            }

            foreach ($order->fulfillmentOrders as $fulfillmentOrder) {
                // @todo proper logging
                if (in_array($fulfillmentOrder->getStatus(), $this->unprocessable_statuses)) {
                    continue;
                }

                $this->restartOrderFulfillment($fulfillmentOrder);
            }
        }
    }

    /**
     * Attempts to reset the fulfillmentOrder status to CREATED
     *
     * @param FulfillmentOrder $fulfillmentOrder
     * @throws InvalidArgumentException
     */
    protected function resetFulfillmentOrderStatus(FulfillmentOrder $fulfillmentOrder)
    {
        $mutation = FulfillmentOrderStatusMutation::createMutation($fulfillmentOrder->getOrderID(), $fulfillmentOrder->getID(), FulfillmentOrderStatus::CREATED);

        $fulfillmentOrder->handleStatusMutation($mutation, true, 'Restarting order fulfillment through ' . __CLASS__);
    }

    /**
     * Restarts the OrderFulfillment.
     *
     * @param FulfillmentOrder $fulfillmentOrder
     */
    protected function restartOrderFulfillment(FulfillmentOrder $fulfillmentOrder)
    {
        try {
            $this->resetFulfillmentOrderStatus($fulfillmentOrder);
        } catch (\InvalidArgumentException $e) {
            // @todo proper logging
            \Log::error($e);
            return;
        }

        if (!$this->process($fulfillmentOrder)) {
            // @todo proper logging
            \Log::error('It failed, nice message');
        }
    }

    /**
     * Processes the FulfillmentOrder
     *
     * @param FulfillmentOrder $fulfillmentOrder
     * @return boolean
     */
    abstract protected function process(FulfillmentOrder $fulfillmentOrder);

    /**
     * Binds the order.
     *
     * @param Container $container
     * @param $orderId
     */
    protected function bindOrder(Container $container, $orderId)
    {
        $order = OrderFactory::load($orderId);

        $container->singleton(\Qup\Checkout\Order\Contracts\OrderInstance::class, function() use($order) {
            return $order;
        });

        return $order;
    }

    /**
     * Retrieves the expired orders.
     *
     * @return mixed
     */
    protected function getExpiredFulfillmentOrders()
    {
        $sessionLength = config('jwt.ttl');

        $date = new \DateTime();
        $date_interval = new \DateInterval('PT' . $sessionLength . 'M');
        $date->sub($date_interval);

        $ordersBuilder = \DB::table('orders')->whereIn('ticket_status', [OrderStatus::FULFILLMENT_REQUIRED]);
        $ordersBuilder->where('updated_at', '<', $date->format(\DateTime::ISO8601));

        return $ordersBuilder->get(['id']);
    }
}