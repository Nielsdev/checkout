<?php

namespace Qup\Checkout\Tasks;

use App\Console\Commands\OrderMaintenanceBase;
use Carbon\Carbon;
use Illuminate\Container\Container;
use ProBiz\Order\OrderStatus;

class CleanupOrders extends OrderMaintenanceBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:abandon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Abandons the applicable orders';

    /**
     * CleanupOrders constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Handles this task
     */
    public function handle()
    {
        $orders = $this->getExpiredOrders();

        foreach ($orders as $order) {
            try {
                $order = self::handleOrderId($order['id']);
                $order->abandon();
            }
            catch (\Exception $ex) {
                \Log::error(sprintf('Unable to release order ID=%d', $order['id']));
                continue;
            }

            $order->delete();

            \Log::debug(sprintf('Order %d has been failed & deleted.', $order->getID()));
        }
    }

    /**
     * Retrieves the expired orders.
     *
     * @return mixed
     */
    protected function getExpiredOrders()
    {
        $sessionLength = 20;

        $date = Carbon::now();
        $date = $date->subMinutes($sessionLength);

        $beginDate = Carbon::createFromDate(2017, 03, 21);

        $ordersBuilder = \DB::table('tbl_orders')->where('updated_at', '<', $date->toIso8601String());
        $ordersBuilder->whereNull('deleted_at');
        $ordersBuilder->where('created_at', '>', $beginDate->toIso8601String());

        // ignore Open orders as well, because these are checked by ProcessOrders
        $ordersBuilder->whereNotIn('status', [OrderStatus::SUCCEEDED, OrderStatus::FAILED, OrderStatus::OPEN]);

        return $ordersBuilder->get(['id']);
    }
}