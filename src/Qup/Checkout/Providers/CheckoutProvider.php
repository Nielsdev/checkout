<?php

namespace Qup\Checkout\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use ProBiz\Payment\PaymentPlatformImplementation;
use ProBiz\Payment\PaymentPlatformOgone;

class CheckoutProvider extends EventServiceProvider
{
    /**
     * @var array
     */
    protected $commands = [
        'Qup\Checkout\Tasks\ProcessOpenOrders',
        'Qup\Checkout\Tasks\ProcessTicketFulfillmentOrders',
        'Qup\Checkout\Tasks\MonitorBarcodes'
    ];

    /**
     * Registers events
     * @see \Illuminate\Foundation\Support\Providers\EventServiceProvider::register()
     */
    public function register()
    {
        $this->commands($this->commands);

        // CheckoutInfo needs to be pre-registered, because the subscribers rely on it.
        $this->app->singleton(\Qup\Checkout\Order\Contracts\OrderInstance::class, \ProBiz\Order\ShoppingCart::class);
        $this->app->singleton(\Qup\Checkout\Contracts\CheckoutInfo::class, \Qup\Checkout\CheckoutInfo::class);
        $this->app->singleton(\Qup\Checkout\Catalog\PaymentCatalog::class, \Qup\Checkout\Catalog\PaymentCatalog::class);
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerAccessors();
        $this->registerValidation();
        $this->registerContextualBindings();

        \Event::subscribe(\Qup\Checkout\Listeners\ChangeCheckoutInfo::class);
        \Event::subscribe(\Qup\Checkout\Listeners\ProductAvailabilityChecker::class);
        \Event::subscribe(\Qup\Checkout\Listeners\LoadRelatedProducts::class);
        \Event::subscribe(\Qup\Checkout\Listeners\MarkRelatedProducts::class);
        \Event::subscribe(\Qup\Checkout\Listeners\PaymentAvailabilityChecker::class);
        \Event::subscribe(\Qup\Checkout\Listeners\HandleInsuranceCosts::class);
        \Event::subscribe(\Qup\Checkout\Listeners\RefundOrder::class);
        \Event::subscribe(\Qup\Checkout\Order\Requirements\ShoppingCartRequirements::class);
        \Event::subscribe(\Qup\Checkout\Order\Requirements\OrderNewRequirements::class);
        \Event::subscribe(\Qup\Checkout\Order\Requirements\OrderOpenRequirements::class);
        \Event::subscribe(\Qup\Checkout\Order\Requirements\OrderSuccessRequirements::class);
        \Event::subscribe(\Qup\Checkout\Order\Requirements\OrderFailedRequirements::class);
        \Event::subscribe(\Qup\Checkout\Listeners\OrderStatusFromTransaction::class);
        \Event::subscribe(\Qup\Checkout\Listeners\OrderStatusFailureMailNotifications::class);
    }

    /**
     * Registers the binds
     */
    protected function registerAccessors()
    {
        // Register the cart catalog
        $this->app->singleton('catalog', 'Qup\Checkout\Containers\CartCatalogContainer');
    }

    /**
     * Registers the custom validation rules
     */
    protected function registerValidation()
    {
        \Validator::extend('in_catalog', 'Qup\Checkout\Validators\CatalogValidator@isDateTimeRankSubProductsInCatalog');
        \Validator::extend('in_payment_catalog', 'Qup\Checkout\Validators\CatalogValidator@isPaymentMethodInCatalog');
        \Validator::extend('is_valid_cart_put', 'Qup\Checkout\Validators\CartValidator@isValidCartPut');
    }

    /**
     * Registers the contextual binds.
     */
    protected function registerContextualBindings()
    {
        // Since contextual binds are not "really" working.
        // I am binding serveral typehints to the orderHandler methods
        // This should work the same, just less fancy syntax.

        $this->app->bind(\Qup\Checkout\Order\Contracts\ShoppingCart::class, \Qup\Checkout\Order\Resolvers\ShoppingCartResolver::class);
        $this->app->bind(\Qup\Checkout\Order\Contracts\OrderNew::class, \Qup\Checkout\Order\Resolvers\OrderNewResolver::class);
        $this->app->bind(\Qup\Checkout\Order\Contracts\OrderOpen::class, \Qup\Checkout\Order\Resolvers\OrderOpenResolver::class);
        $this->app->bind(\Qup\Checkout\Order\Contracts\OrderSucceeded::class, \Qup\Checkout\Order\Resolvers\OrderSuccessResolver::class);
        $this->app->bind(\Qup\Checkout\Order\Contracts\OrderFailed::class, \Qup\Checkout\Order\Resolvers\OrderFailedResolver::class);

        $this->app->singleton(PaymentPlatformImplementation::class, PaymentPlatformOgone::class);

        $this->app->bind(\Qup\Checkout\Definitions\Contracts\CartProducts::class, function($app) {
            $orderCatalog = $app->make(\Qup\Checkout\Catalog\OrderCatalog::class);
            return $orderCatalog->getCartProducts();
        });

        $this->app->bind(\Qup\Checkout\Definitions\Contracts\CatalogProducts::class, \Qup\Checkout\Definitions\Resolvers\CatalogProductResolver::class);
        $this->app->bind(\Qup\Checkout\Definitions\Contracts\CatalogRateGroups::class, \Qup\Checkout\Definitions\Resolvers\CatalogRateGroupResolver::class);
        $this->app->bind(\Qup\Checkout\Definitions\Contracts\CatalogRanks::class, \Qup\Checkout\Definitions\Resolvers\CatalogRanksResolver::class);
        $this->app->bind(\Qup\Checkout\Definitions\Contracts\CatalogRelatedProducts::class, \Qup\Checkout\Definitions\Resolvers\CatalogRelatedProductsResolver::class);
        $this->app->bind(\Qup\Checkout\Definitions\Contracts\CartCatalog::class, \Qup\Checkout\Definitions\Resolvers\CartCatalogResolver::class);
        $this->app->bind(\Qup\Checkout\Definitions\Contracts\CatalogInsurable::class, \Qup\Checkout\Definitions\Resolvers\CatalogInsurableResolver::class);

        $this->app->bind(\Qup\Checkout\Contracts\PaymentMethods::class, function($app) {
            $paymentCatalog = $app->make(\Qup\Checkout\Catalog\PaymentCatalog::class);
            return \Qup\Checkout\Definitions\Resolvers\CatalogPaymentMethodResolver::getCatalogPaymentMethods($paymentCatalog);
        });
    }
}