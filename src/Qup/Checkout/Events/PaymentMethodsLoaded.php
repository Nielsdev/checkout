<?php

namespace Qup\Checkout\Events;

use \Illuminate\Database\Eloquent\Collection;
use \ProBiz\Core\Collection\ObjectCollection;

class PaymentMethodsLoaded
{
    /**
     * @var ObjectCollection
     */
    public $collection;
    public $methods;

    /**
     * Creates an instance of the PaymentMethodsLoaded event
     * 
     * @param array $methods
     * @param ObjectCollection $collection 
     */
    public function __construct($methods, ObjectCollection $collection)
    {
        $this->collection = $collection;
        $this->methods = $methods;
    }
}