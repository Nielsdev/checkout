<?php

namespace Qup\Checkout\Events;

use ProBiz\Order\ShoppingCart;
use Qup\Checkout\CheckoutInfo;
use Qup\Promotions\Contracts\Input;

class CartContentsChanged
{
    /**
     * @var ShoppingCart
     */
    public $order;

    /**
     * @var CheckoutInfo
     */
    public $checkoutInfo;

    /**
     * @var Input
     */
    public $input;

    /**
     * Creates an instance of the CartContentsChanged event
     *
     * @param ShoppingCart $cart
     * @param CheckoutInfo $checkoutInfo
     * @param null $input
     */
    public function __construct(ShoppingCart $cart, CheckoutInfo $checkoutInfo, $input = null)
    {
        if (!empty($input) && !($input instanceof Input)) {
            throw new \InvalidArgumentException('$input needs to be of type '. Input::class .'", "'.get_class($input).'" given.');
        }

        $this->order = $cart;
        $this->checkoutInfo = $checkoutInfo;
        $this->input = $input;
    }
}