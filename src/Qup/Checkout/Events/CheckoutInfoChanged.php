<?php

namespace Qup\Checkout\Events;

use \Qup\Checkout\Catalog\CartCatalog;
use \Qup\Checkout\Contracts\CheckoutInfo;
use ProBiz\Order\ShoppingCart;

class CheckoutInfoChanged
{
    /**
     * @var CheckoutInfo
     */
    public $checkoutInfo;

    /**
     * @var Order
     */
    public $order;

    /**
     * Creates an instance of the CatalogChanged event
     * 
     * @param CartCatalog $catalog
     * @param ShoppingCart $cart
     */
    public function __construct(CheckoutInfo $info)
    {
        $this->checkoutInfo = $info;
        $this->order = \App::make(\Qup\Checkout\Order\Contracts\OrderInstance::class);
    }
}