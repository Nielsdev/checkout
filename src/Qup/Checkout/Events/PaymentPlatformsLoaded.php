<?php

namespace Qup\Checkout\Events;

use Illuminate\Database\Eloquent\Collection;
use ProBiz\Core\Collection\ObjectCollection;

class PaymentPlatformsLoaded
{
    /**
     * @var ObjectCollection
     */
    public $collection;
    public $platforms;

    /**
     * Creates an instance of the PaymentPlatformsLoaded event
     * 
     * @param ObjectCollection $platforms
     * @param ObjectCollection $collection 
     */
    public function __construct(Collection $platforms, ObjectCollection $collection)
    {
        $this->collection = $collection;
        $this->platforms = $platforms;
    }
}