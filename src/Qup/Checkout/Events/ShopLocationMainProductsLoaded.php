<?php

namespace Qup\Checkout\Events;

use Illuminate\Database\Eloquent\Collection;

class ShopLocationMainProductsLoaded
{
    /**
     * @var ObjectCollection
     */
    public $collection;

    /**
     * Creates an instance of the ShopLocationMainProductsLoaded event
     * 
     * @param ObjectCollection $collection containing ShopLocationMainProducts
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }
}