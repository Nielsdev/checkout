<?php

namespace Qup\Checkout\Events;

use ProBiz\Order\Transaction\Transaction;
use Qup\Checkout\Order\Contracts\OrderInstance;

class TransactionStatusChanged
{
    public $order;
    public $transaction;

    /**
     * Creates an instance of the TransactionStatusChanged event
     * 
     * @param Order $order
     */
    public function __construct(Transaction $transaction, OrderInstance $order)
    {
        $this->order = $order;
        $this->transaction = $transaction;
    }
}