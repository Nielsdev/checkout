<?php

namespace Qup\Checkout\Events;

use ProBiz\Order\Transaction\Transaction;
use Qup\Checkout\Order\Contracts\OrderInstance;

class OrderStatusChangeAfterTransactionFailed
{
    public $exception;
    public $order;
    public $transaction;

    /**
     * OrderStatusChangeFailed constructor.
     */
    public function __construct(\Exception $exception, OrderInstance $order, Transaction $transaction)
    {
        $this->exception = $exception;
        $this->order = $order;
        $this->transaction = $transaction;
    }
}