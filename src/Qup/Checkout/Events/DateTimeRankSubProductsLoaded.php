<?php

namespace Qup\Checkout\Events;

use Illuminate\Database\Eloquent\Collection;

class DateTimeRankSubProductsLoaded
{
    /**
     * @var ObjectCollection
     */
    public $collection;

    /**
     * Creates an instance of the DateTimeRankSubProducts event
     * 
     * @param Collection $collection containing DateTimeRankSubProducts
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }
}