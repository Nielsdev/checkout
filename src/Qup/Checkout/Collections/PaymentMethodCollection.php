<?php

namespace Qup\Checkout\Collections;

use Illuminate\Support\Collection;
use Qup\Checkout\Contracts\PaymentMethods;

class PaymentMethodCollection extends Collection implements PaymentMethods
{
    
}