<?php

namespace Qup\Checkout\Collections;

use Illuminate\Support\Collection;
use Qup\Checkout\Definitions\Contracts\CartProducts;

class CartProductCollection extends Collection implements CartProducts
{
    
}