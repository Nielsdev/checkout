<?php

namespace Qup\Checkout\Definitions;


class CartSubscription implements \JsonSerializable
{
    /**
     * @var string The title of the subscription
     */
    public $title;

    /**
     * @var string The tooltip to be shown (e.g. in an information balloon)
     */
    public $tooltip;

    /**
     * @var boolean Indicates whether or not the subscription is required.
     */
    public $required;

    /**
     * @var boolean Indicates whether or not the subscription should be pre-checked in the form.
     */
    public $prechecked;

    /**
     * @var string The content page ID with information that belongs to this subscription (can be directly called through /content/{content_id}
     */
    public $contentid;

    /**
     * @var integer The subscription id
     */
    public $id;

    /**
     * @var boolean
     */
    public $subscribed;

    /**
     * (non-PHPdoc)
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'tooltip' => $this->tooltip,
            'required' => $this->required,
            'prechecked' => $this->prechecked,
            'contentid' => $this->contentid,
            'subscribed' => $this->subscribed,
        ];
    }

    /**
     * Creates this definition
     *
     * @param string|null $title The title of the subscription
     * @param string|null $tooltip The tooltip to be shown (e.g. in an information balloon)
     * @param boolean $required Indicates whether or not the subscription is required.
     * @param boolean $prechecked Indicates whether or not the subscription should be pre-checked in the form.
     * @param string $contentid The content page ID with information that belongs to this subscription (can be directly called through /content/{content_id}
     * @param boolean $subscribed Indicates whether or not the current user has opted in
     * @return CartSubscription
     */
    public static function create(
        $id = null,
        $title = null,
        $tooltip = null,
        $required = false,
        $prechecked = false,
        $contentid = null,
        $subscribed = false
    ) {
        $item = new static();

        $item->id = $id;
        $item->title = $title;
        $item->tooltip = $tooltip;
        $item->required = $required;
        $item->prechecked = $prechecked;
        $item->contentid = $contentid;
        $item->subscribed = $subscribed;

        return $item;
    }
}
