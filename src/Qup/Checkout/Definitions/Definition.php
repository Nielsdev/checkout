<?php

namespace Qup\Checkout\Definitions;

use Illuminate\Contracts\Support\Arrayable;

abstract class Definition implements \JsonSerializable, Arrayable
{
    /**
     * Serializes current object
     * 
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Returns the definition as array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}