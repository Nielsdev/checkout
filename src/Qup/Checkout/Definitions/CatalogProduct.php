<?php

namespace Qup\Checkout\Definitions;

use \Qup\Checkout\Definitions\Definition;

class CatalogProduct extends Definition
{
    /**#@+
     * @var integer
     */
    public $id;
    public $rategroup_id;
    public $rank_id;
    public $date;
    public $time;
    public $stock;
    public $max_discount_level;
    public $max_discount_stock;
    /**#@-*/

    /**#@+
     * @var boolean
     */
    public $insurable;
    /**#@-*/

    /**#@+
     * @var float
     */
    public $price;
    public $max_discount;
    /**#@-*/
}