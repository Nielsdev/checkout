<?php

namespace Qup\Checkout\Definitions;

use Qup\Checkout\Definitions\Definition;

class CatalogRateGroup extends Definition
{
    /**#@+
     * @var integer
     */
    public $id;
    /**#@-*/

    /**#@+
     * @var string
     */
    public $name;
    public $summary;
    public $rategroup_class;
    /**#@-*/

    /**
     * @var boolean
     */
    public $description;
}