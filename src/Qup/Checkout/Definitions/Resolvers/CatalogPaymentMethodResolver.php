<?php

namespace Qup\Checkout\Definitions\Resolvers;

use Qup\Checkout\Catalog\PaymentCatalog;
use Qup\Checkout\Definitions\CatalogPaymentMethod;
use Qup\Checkout\Collections\PaymentMethodCollection;

class CatalogPaymentMethodResolver
{
    /**
     * Retrieves the Catalog Payment methods according to definition
     *
     * @param PaymentCatalog $cartCatalog
     * @return array containing CatalogPaymentMethods
     */
    public static function getCatalogPaymentMethods(PaymentCatalog $paymentCatalog)
    {
        $paymentMethods = new PaymentMethodCollection();

        foreach ($paymentCatalog->paymentMethods as $method) {
            $paymentMethod = CatalogPaymentMethod::createFromPaymentMethodImplementation($method);

            $paymentMethods->push($paymentMethod);
        }

        return $paymentMethods;
    }
}