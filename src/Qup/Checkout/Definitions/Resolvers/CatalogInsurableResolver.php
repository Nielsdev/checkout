<?php

namespace Qup\Checkout\Definitions\Resolvers;

use Qup\Checkout\Catalog\Contracts\CartCatalog;
use Qup\Checkout\Definitions\CatalogProduct;
use Qup\Checkout\Definitions\CatalogInsurable;
use Qup\Checkout\Models\CalendarProduct;

class CatalogInsurableResolver extends Resolver implements \Qup\Checkout\Definitions\Contracts\CatalogInsurable
{
    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * Creates a new instance of CartCatalogResolver
     * 
     * @param CartCatalog Injected singleton
     */
    public function __construct(CartCatalog $catalog, CatalogInsurable $definition)
    {
        $this->catalog = $catalog;
        $this->definition = $definition;
    }

    /**
     * Resolves the definition
     */
    public function resolve()
    {
        foreach ($this->catalog->relatedProducts() as $catalogProduct) {

            if($catalogProduct->insurable){
                $this->definition->insurable = true;
                break;
            }
        }
    }
}