<?php

namespace Qup\Checkout\Definitions\Resolvers;

use ProBiz\Cache\ObjectCacheController;
use Qup\Checkout\Catalog\Contracts\CartCatalog;
use Qup\Checkout\Definitions\CatalogRateGroup;
use Qup\Checkout\Definitions\CatalogRateGroups;

class CatalogRateGroupResolver extends Resolver implements \Qup\Checkout\Definitions\Contracts\CatalogRateGroups
{
    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * Creates a new instance of CartCatalogResolver
     *
     * @param CartCatalog Injected singleton
     */
    public function __construct(CartCatalog $cartCatalog, CatalogRateGroups $definition)
    {
        $this->catalog = $cartCatalog;
        $this->definition = $definition;
    }

    /**
     * Resolves the definition
     */
    public function resolve()
    {
        $keys = [];
        $itemList = [];

        foreach ($this->catalog as $dateTimeRankSubProduct) {

            if (in_array($dateTimeRankSubProduct->shop_location_sub_product_id, $keys)) {
                continue;
            }

            $subProd = ObjectCacheController::get(ObjectCacheController::ITEM_SHOPLOCATIONSUBPRODUCT, $dateTimeRankSubProduct->shop_location_sub_product_id);
            if (empty($subProd->getRateGroupClassID())) {
                // none supplied, zero f's given
                continue;
            }

            $rateGroupClass = ObjectCacheController::get(ObjectCacheController::ITEM_RATEGROUPCLASS, $subProd->getRateGroupClassID());

            $catalogRateGroup = new CatalogRateGroup();
            $catalogRateGroup->id   = $subProd->getID();
            $catalogRateGroup->name = $subProd->getTitle();
            $catalogRateGroup->summary = $subProd->getDescription();
            $catalogRateGroup->rategroup_class = $rateGroupClass->getTitle();
            $catalogRateGroup->crosssell = (!empty($dateTimeRankSubProduct->related));
            $catalogRateGroup->tag = $subProd->getPriceTag();

            $itemList[] = $catalogRateGroup;

            $keys[] = $subProd->getID();
        }

        $this->definition->rateGroups = $this->reorderList($itemList, 'name');
    }

    /**
     * Reorder the rate groups array by given key
     *
     * @param array $itemList
     * @param string $orderKey
     *
     * @return array
     */
    private function reorderList($itemList, $orderKey) {
        usort($itemList, function($a, $b) use ($orderKey) {
            if (property_exists($a, $orderKey)) {
                return strcasecmp($a->{$orderKey}, $b->{$orderKey});
            }
        });

        return $itemList;
    }

}
