<?php

namespace Qup\Checkout\Definitions\Resolvers;

use Qup\Checkout\Catalog\Contracts\CartCatalog;
use ProBiz\Cache\ObjectCacheController;
use Qup\Checkout\Definitions\CatalogRank;
use Qup\Checkout\Definitions\CatalogRanks;

class CatalogRanksResolver extends Resolver implements \Qup\Checkout\Definitions\Contracts\CatalogRanks
{
    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * Creates a new instance of CartCatalogResolver
     * 
     * @param CartCatalog Injected singleton
     */
    public function __construct(CartCatalog $cartCatalog, CatalogRanks $definition)
    {
        $this->catalog = $cartCatalog;
        $this->definition = $definition;
    }

    /**
     * Resolves the definition
     */
    public function resolve()
    {
        $keys = [];

        foreach ($this->catalog->cartProducts() as $catalogProduct) {

            if (in_array($catalogProduct->shop_rank_id, $keys)) {
                $catalogRank = $this->definition->ranks[$catalogProduct->shop_rank_id];
                // update stock
                if (!$catalogRank->available) {
                    $catalogRank->available = !empty($catalogProduct->stock);
                }

                continue;
            }

            $available = !empty($catalogProduct->stock);

            $rank = ObjectCacheController::get(ObjectCacheController::ITEM_SHOP_RANK, $catalogProduct->shop_rank_id);

            $catalogRank = new CatalogRank();
            $catalogRank->id = $rank->getID();
            $catalogRank->name = $rank->getTitle();
            $catalogRank->available = $available;
            $this->definition->ranks[$catalogRank->id] = $catalogRank;

            $keys[] = $rank->getID();
        }
    }
}
