<?php

namespace Qup\Checkout\Definitions\Resolvers;

use Qup\Checkout\Catalog\Contracts\CartCatalog;
use Qup\Checkout\Definitions\CatalogProducts;
use Qup\Checkout\Definitions\CatalogProduct;

class CatalogProductResolver extends Resolver implements \Qup\Checkout\Definitions\Contracts\CatalogProducts
{
    const STOCK_MAX = 10;
    const DISCOUNT_STOCK_MAX = 10;

    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * Creates a new instance of CartCatalogResolver
     *
     * @param CartCatalog $cartCatalog Injected singleton
     * @param CatalogProducts $definition
     */
    public function __construct(CartCatalog $cartCatalog, CatalogProducts $definition)
    {
        $this->catalog = $cartCatalog;
        $this->definition = $definition;
    }

    /**
     * Resolves the definition
     */
    public function resolve()
    {
        foreach ($this->catalog->cartProducts() as $catalogProduct) {

            $definition = new CatalogProduct();
            $definition->id = $catalogProduct->id;
            $definition->rategroup_id = $catalogProduct->shop_location_sub_product_id;

            $definition->rank_id = $catalogProduct->shop_rank_id;
            $definition->date = $catalogProduct->date;
            $definition->time = $catalogProduct->time;
            $definition->price = $catalogProduct->price_inc;

            if ($catalogProduct->code == 'NSCOMBI') {
                $definition->price = 46.85;
            }

            $definition->stock = $catalogProduct->stock===null ? self::STOCK_MAX : max(0, min($catalogProduct->stock, self::STOCK_MAX));
            $definition->max_discount = empty($catalogProduct->discount) ? 0 : $catalogProduct->discount;
            $definition->insurable = $catalogProduct->insurable;
            $definition->max_discount_level = empty($catalogProduct->discount_level) ?  0 : $catalogProduct->discount_level;
            // for performance reasons, discuont level * 10000 and corresponding discount stock are combined in a single integer so
            // complex joins in the DB view are circumvented; to get to the discount stock we need only the modulus of the value
            // Note that NULL is converted to a (high) integer in the DB view
            $definition->max_discount_stock = min(self::DISCOUNT_STOCK_MAX, $catalogProduct->discount_level_availability % 10000);

            $this->definition->subProducts[] = $definition;
        }
    }
}