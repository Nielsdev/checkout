<?php

namespace Qup\Checkout\Definitions\Resolvers;

use Qup\Checkout\Catalog\Contracts\CartCatalog;
use Qup\Checkout\Definitions\CatalogProduct;
use Qup\Checkout\Definitions\CatalogRelatedProducts;
use Qup\Checkout\Models\CalendarProduct;

class CatalogRelatedProductsResolver extends Resolver implements \Qup\Checkout\Definitions\Contracts\CatalogRelatedProducts
{
    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * Creates a new instance of CartCatalogResolver
     * 
     * @param CartCatalog Injected singleton
     */
    public function __construct(CartCatalog $catalog, CatalogRelatedProducts $definition)
    {
        $this->catalog = $catalog;
        $this->definition = $definition;
    }

    /**
     * Resolves the definition
     */
    public function resolve()
    {
        foreach ($this->catalog->relatedProducts() as $catalogProduct) {

            $definition = new CatalogProduct();
            $definition->id = $catalogProduct->id;
            $definition->rategroup_id = $catalogProduct->shop_location_sub_product_id;

            $definition->rank_id = $catalogProduct->shop_rank_id;
            $definition->date = $catalogProduct->date;
            $definition->time = $catalogProduct->time;
            $definition->price = $catalogProduct->price_inc;
            $definition->max_discount = $catalogProduct->discount;
            $definition->stock = $catalogProduct->stock;
            $definition->max_discount = $catalogProduct->discount;
            $definition->insurable = $catalogProduct->insurable;

            $this->definition->relatedProducts[] = $definition;
        }
    }
}