<?php

namespace Qup\Checkout\Definitions\Resolvers;

use Qup\Checkout\Definitions\Definition;
use Illuminate\Contracts\Support\Arrayable;

abstract class Resolver implements \JsonSerializable, Arrayable
{
    /**
     * @var CartCatalog
     */
    protected $definition;

    /**
     * This method should resolve the "transformed" items.
     */
    abstract public function resolve();

    /**
     * Returns the definition for serialization.
     * 
     * @return Definition
     */
    public function jsonSerialize()
    {
        $this->resolve();

        return $this->definition;
    }

    public function __toString()
    {
        return json_encode($this->jsonSerialize());
    }

    public function toArray()
    {
        return $this->definition->toArray();
    }
}