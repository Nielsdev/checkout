<?php

namespace Qup\Checkout\Definitions\Resolvers;

use Qup\Checkout\Definitions\Contracts\CatalogRateGroups;
use Qup\Checkout\Definitions\Contracts\CatalogProducts;
use Qup\Checkout\Definitions\Contracts\CatalogRanks;
use Qup\Checkout\Definitions\Contracts\CatalogRelatedProducts;
use Qup\Checkout\Definitions\Contracts\CatalogInsurable;

class CartCatalogResolver extends Resolver implements \Qup\Checkout\Definitions\Contracts\CartCatalog
{
    /**
     * @var CatalogProductResolver
     */
    private $catalogProductResolver;

    /**
     * @var CatalogRanksResolver
     */
    private $catalogRanksResolver;

    /**
     * @var CatalogRateGroupResolver
     */
    private $catalogRateGroupResolver;

    /**
     * @var CatalogRateGroupResolver
     */
    private $catalogRelatedProductsResolver;

    /**
     * @var CatalogInsurableResolver
     */
    private $catalogInsurableResolver;

    /**
     * Creates an instance of CartCatalogResolver
     */
    public function __construct(CatalogProducts $catalogProductResolver, CatalogRanks $catalogRanksResolver, CatalogRateGroups $catalogRateGroupResolver, CatalogRelatedProducts $catalogRelatedProductsResolver, CatalogInsurable $catalogInsurableResolver)
    {
        $this->catalogProductResolver = $catalogProductResolver;
        $this->catalogRanksResolver   = $catalogRanksResolver;
        $this->catalogRateGroupResolver = $catalogRateGroupResolver;
        $this->catalogRelatedProductsResolver = $catalogRelatedProductsResolver;
        $this->catalogInsurableResolver = $catalogInsurableResolver;
    }

    /**
     * {@inheritDoc}
     * @see \Qup\Checkout\Resolvers\Resolver::resolve()
     */
    public function resolve()
    {
        $products = json_encode($this->catalogProductResolver);
        $ranks    = json_encode($this->catalogRanksResolver);
        $rate_groups = json_encode($this->catalogRateGroupResolver);
        $related_products = json_encode($this->catalogRelatedProductsResolver);
        $insurable = json_encode($this->catalogInsurableResolver);

        $this->definition = array_merge(
            (array)json_decode($products),
            (array)json_decode($ranks),
            (array)json_decode($rate_groups),
            (array)json_decode($related_products),
            (array)json_decode($insurable)
        );
    }
}