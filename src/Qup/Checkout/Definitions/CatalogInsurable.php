<?php

namespace Qup\Checkout\Definitions;

use Qup\Checkout\Definitions\Definition;

class CatalogInsurable extends Definition
{
    /**#@+
     * @var boolean
     */
    public $insurable = false;
}