<?php

namespace Qup\Checkout\Definitions;

use \ProBiz\Payment\PaymentMethodImplementation;
use \Qup\Checkout\Definitions\Definition;

class CatalogPaymentMethod extends Definition
{
    const TYPE_IFRAME = 'iframe';
    const TYPE_NATIVE = 'native';

    /**#@+
     * @var string
     */
    public $name;
    public $type;
    public $url;
    /**#@-*/

    /**#@+
     * @var integer
     */
    public $type_id;
    public $subtype_id;
    /**#@-*/

    /**
     * Creates this definition
     *
     * @param string $type_id
     * @param string $subtype_id
     * @param string $name
     * @param string $type
     * @param string $url
     * @return CatalogPaymentMethod
     */
    public static function create(
        $type_id = null,
        $subtype_id = null,
        $name = null,
        $type = null,
        $url = null
    ) {
        $paymentMethod = new static();

        $paymentMethod->type_id = $type_id;
        $paymentMethod->subtype_id = $subtype_id;
        $paymentMethod->name = $name;
        $paymentMethod->type = $type;
        $paymentMethod->url = $url;

        return $paymentMethod;
    }

    /**
     * Creates this definition from the supplied ProBiz implementation
     * @param PaymentMethodImplementation $paymentMethod
     * @return CatalogPaymentMethod
     */
    public static function createFromPaymentMethodImplementation(PaymentMethodImplementation $paymentMethod)
    {
        $type = !empty($paymentMethod->getParentMethod()) ? $paymentMethod->getParentMethod() : $paymentMethod->getMethod();
        $sub_type = !empty($paymentMethod->getParentMethod()) ? $paymentMethod->getMethod() : null;

        return static::create($type, $sub_type, $paymentMethod->getTitle(), static::determineType($paymentMethod), $paymentMethod->getMethodPopulateURL());
    }

    /**
     * Determines the type from the supplied object
     * @param PaymentMethodImplementation $paymentMethod
     * @return string
     */
    private static function determineType(PaymentMethodImplementation $paymentMethod)
    {
        return empty($paymentMethod->getMethodPopulateURL()) ? static::TYPE_NATIVE : static::TYPE_IFRAME;
    }
}