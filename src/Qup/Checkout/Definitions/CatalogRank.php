<?php

namespace Qup\Checkout\Definitions;

use Qup\Checkout\Definitions\Definition;

class CatalogRank extends Definition
{
    /**#@+
     * @var integer
     */
    public $id;
    /**#@-*/

    /**#@+
     * @var string
     */
    public $name;
    public $summary;
    /**#@-*/

    /**
     * @var boolean
     */
    public $available;
}