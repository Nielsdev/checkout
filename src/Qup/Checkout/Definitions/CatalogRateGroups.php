<?php

namespace Qup\Checkout\Definitions;

use Qup\Checkout\Definitions\Definition;

class CatalogRateGroups extends Definition
{
    /**
     * Array containing CatalogRanks
     *
     * @var array
     */
    public $rateGroups = [];
}