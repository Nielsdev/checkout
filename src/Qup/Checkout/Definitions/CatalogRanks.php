<?php

namespace Qup\Checkout\Definitions;

use Qup\Checkout\Definitions\Definition;

class CatalogRanks extends Definition
{
    /**
     * Array containing CatalogRanks
     * 
     * @var array
     */
    public $ranks = [];
}