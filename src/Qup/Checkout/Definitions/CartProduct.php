<?php

namespace Qup\Checkout\Definitions;

class CartProduct implements \JsonSerializable
{
    /**#@+
     * @var string
     */
    public $name;
    public $description;
    public $rank;
    public $shop;
    public $location;
    public $time;
    /**#@-*/

    /**#@+
     * @var integer
     */
    public $id;
    public $quantity;
    public $rategroup;
    /**#@-*/

    /**#@+
     * @var float
     */
    public $price;
    /**#@-*/

    /**#@+
     * @var float
     */
    public $crosssell;
    /**#@-*/

    /**
     * (non-PHPdoc)
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'rank' => $this->rank,
            'shop' => $this->shop,
            'location' => $this->location,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'time' => $this->time,
            'crosssell' => $this->crosssell,
            'rategroup' => $this->rategroup
        ];
    }

    /**
     * Creates this definition
     * 
     * @param string $name
     * @param string $description
     * @param string $rank
     * @param string $shop
     * @param string $location
     * @param string $quantity
     * @param string $price
     * @param boolean $crosssell
     * @param integer $rategroup
     * @return self
     */
    public static function create(
        $id = null,
        $name = null,
        $description = null,
        $rank = null,
        $shop = null,
        $location = null,
        $quantity = null,
        $price = null,
        $time = null,
        $crosssell = false,
        $rategroup = null
    ) {
        $cartProduct = new static();

        $cartProduct->id = $id;
        $cartProduct->name = $name;
        $cartProduct->description = $description;
        $cartProduct->rank = $rank;
        $cartProduct->shop = $shop;
        $cartProduct->location = $location;
        $cartProduct->quantity = $quantity;
        $cartProduct->price = $price;
        $cartProduct->time = $time;
        $cartProduct->crosssell = $crosssell;
        $cartProduct->rategroup = $rategroup;

        return $cartProduct;
    }
}