<?php

namespace Qup\Checkout\Definitions;

use Qup\Checkout\Definitions\Definition;

class CatalogRelatedProducts extends Definition
{
    /**
     * Array containing CatalogProduct
     * @var array
     */
    public $relatedProducts = [];
}