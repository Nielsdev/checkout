<?php

namespace Qup\Checkout\Definitions;

class OrderStatus implements \JsonSerializable
{
    /**
     * @var string
     */
    private $status;

    /**
     * Serializes the object
     */
    public function jsonSerialize()
    {
        return [
            'status' => $this->status
        ];
    }

    /**
     * Sets the current order status
     * 
     * @param integer $status (@see OrderStatus constants)
     */
    public function setStatus($status)
    {
        switch ($status) {
            case \ProBiz\Order\OrderStatus::CART:
            case \ProBiz\Order\OrderStatus::CREATED:
                $this->status = 'new';
                break;
            case \ProBiz\Order\OrderStatus::OPEN:
                $this->status = 'open';
                break;
            case \ProBiz\Order\OrderStatus::FAILED:
                $this->status = 'failed';
                break;
            case \ProBiz\Order\OrderStatus::SUCCEEDED:
                $this->status = 'succeeded';
        }
    }
}