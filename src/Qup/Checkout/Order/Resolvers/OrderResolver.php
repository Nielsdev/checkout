<?php

namespace Qup\Checkout\Order\Resolvers;

use Illuminate\Contracts\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Qup\Checkout\Order\Contracts\OrderInstance;
use ProBiz\Order\ShoppingCart;
use ProBiz\Order\OrderNew;
use ProBiz\Order\OrderOpen;
use ProBiz\Order\OrderFailed;
use ProBiz\Order\OrderSucceeded;
use ProBiz\Order\OrderStatus;
use ProBiz\Order\OrderFactory;
use Qup\Checkout\Order\Exceptions\UnsupportedStatusTransitionException;
use Qup\Checkout\Order\OrderEvents;

abstract class OrderResolver
{
    /**
     * @var string
     */
    protected $before_event;
    protected $after_event;

    /**
     * @var string
     */
    protected $global_before_event = OrderEvents::BEFORE_STATUS_CHANGE_GLOBAL;
    protected $global_after_event = OrderEvents::AFTER_STATUS_CHANGE_GLOBAL;

    /**
     * @var Order
     */
    protected $instance;

    /**
     * @var integer
     */
    protected $previous_state;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var boolean
     */

    /**
     * Starts requested order conversion
     * 
     * @param Order $order
     */
    public function __construct(OrderInstance $order, Container $container, Dispatcher $dispatcher, Request $request)
    {
        $this->request = $request;
        $this->initialize($order, $container, $dispatcher, $request);
    }

    /**
     * Initializes the object.
     * 
     * @param Order $order
     */
    private function initialize(OrderInstance $order, Container $container, Dispatcher $dispatcher, Request $request)
    {
        // Store previous state
        $this->previous_state = $order->getStatus(OrderStatus::TYPE_ORDER);

        // Dispatch event
        $dispatcher->fire($this->global_before_event, [$order]);
        $dispatcher->fire($this->before_event, [$order]);

        // Resolve the order
        $order = $this->resolve($order, $container);
        
        if ($this->previous_state != $order->getStatus(OrderStatus::TYPE_ORDER)) {
            // Save right away, conversion has been done and an error further on would have too many repercussions
            $order->save();

            $order = $this->rebind($order->getID());
            $request->session()->put('order_id', $order->getID());
        }

        $dispatcher->fire($this->global_after_event, [$order]);
        $dispatcher->fire($this->after_event, [$order]);

        // Save the order instance
        $this->instance = $order;
    }

    /**
     * Calls the resolve callback
     * 
     * @param Order $order The current order state
     * @param Container $container
     * @return Order The resolved order
     */
    private function resolve(OrderInstance $order, Container $container)
    {
        $callback = $this->determineOrderCallback($order);
        $result = call_user_func_array([$this, $callback], [$order]);

        if (!$result) {
            throw new UnsupportedStatusTransitionException(
                sprintf('Resolver type (%s) is unable to handle orders from type (%s)',
                    get_class($this),
                    get_class($order)
                )
            );
        }
        if (!($result instanceof OrderInstance)) {
            throw new \LogicException(
                sprintf('Invalid order supplied from callback (%s) for class (%s)',
                    $callback,
                    get_class($this)
                )
            );
        }

        return $result;
    }

    /**
     * Rebinds the new order status
     * 
     * @param integer $orderId
     * @return Order
     */
    private function rebind($orderId)
    {
        $order = OrderFactory::load($orderId);

        \App::singleton(\Qup\Checkout\Order\Contracts\OrderInstance::class, function() use ($order) {
            return $order;
        });

        return $order;
    }

    /**
     * Determines what the callback function should be.
     * ! Please note, these are RESOLVER contracts and not the actual order classes !
     * 
     * @param Order $order
     * @return string The callback to be used.
     */
    private function determineOrderCallback(OrderInstance $order)
    {
        if ($order instanceof ShoppingCart) {
            return 'convertFromShoppingCart';
        }
        if ($order instanceof OrderNew) {
            return 'convertFromOrderNew';
        }
        if ($order instanceof OrderOpen) {
            return 'convertFromOrderOpen';
        }
        if ($order instanceof OrderFailed) {
            return 'convertFromOrderFailed';
        }
        if ($order instanceof OrderSucceeded) {
            return 'convertFromOrderSucceeded';
        }        
    }

    /**
     * Should return a Resolved order.
     *
     * @return Order
     */
    public final function instance()
    {
        return $this->instance;
    }

    /**
     * Returns the previous sate
     * 
     * @return integer
     */
    public function getPreviousState()
    {
        return $this->previous_state;
    }

    /**
     * Returns whether or not the converted order is a new one
     * This will be the case if there was no conversion possible.
     * This flag will still allow the API to notify the customer.
     * 
     * @return boolean
     */
    public function isNew()
    {
        return $this->is_new;
    }
}