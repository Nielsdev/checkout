<?php

namespace Qup\Checkout\Order\Resolvers;

use Qup\Checkout\Order\Contracts\OrderFailed;
use Qup\Checkout\Order\OrderEvents;

class OrderFailedResolver extends OrderResolver implements OrderFailed
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_FAILED;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_FAILED;

    /**
     * Returns current OrderFailed
     *
     * @param \ProBiz\Order\OrderOpen $order
     * @throws
     * @return \ProBiz\Order\OrderFailed
     */
    protected function convertFromOrderFailed(\ProBiz\Order\OrderFailed $order)
    {
        return $order;
    }

    /**
     * Converts OrderOpen to OrderFailed
     *
     * @param \ProBiz\Order\OrderOpen $order
     * @throws
     * @return \ProBiz\Order\OrderFailed
     */
    protected function convertFromOrderOpen(\ProBiz\Order\OrderOpen $order)
    {
        $order->cancel('Automatically converted by OrderFailedResolver');

        return $order;
    }

    /**
     * Converts OrderNew to OrderFailed
     *
     * @param \ProBiz\Order\OrderOpen $order
     * @throws
     * @return \ProBiz\Order\OrderFailed
     */
    protected function convertFromOrderNew(\ProBiz\Order\OrderNew $order)
    {
        $order->abandon('Automatically converted by OrderFailedResolver');

        return $order;
    }

    /**
     * Converts ShoppingCart to OrderFailed
     *
     * @param \ProBiz\Order\OrderOpen $order
     * @throws
     * @return \ProBiz\Order\OrderFailed
     */
    protected function convertFromShoppingCart(\ProBiz\Order\ShoppingCart $order)
    {
        $order->abandon('Automatically converted by OrderFailedResolver');

        return $order;
    }
}