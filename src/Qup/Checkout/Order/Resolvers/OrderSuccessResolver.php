<?php

namespace Qup\Checkout\Order\Resolvers;

use Qup\Checkout\Order\Contracts\OrderSucceeded;
use Qup\Checkout\Order\OrderEvents;

class OrderSuccessResolver extends OrderResolver implements OrderSucceeded
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_SUCCESS;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_SUCCESS;

    /**
     * Returns current OrderSucceeded
     *
     * @param \ProBiz\Order\OrderOpen $order
     * @throws
     * @return \ProBiz\Order\OrderSucceeded
     */
    protected function convertFromOrderSucceeded(\ProBiz\Order\OrderSucceeded $order)
    {
        return $order;
    }

    /**
     * Converts OrderOpen to OrderSucceeded
     *
     * @param \ProBiz\Order\OrderOpen $order
     * @throws
     * @return \ProBiz\Order\OrderSucceeded
     */
    protected function convertFromOrderOpen(\ProBiz\Order\OrderOpen $order)
    {
        $order->succeed(
            sprintf('Converted by OSR - [%s] %s', $this->request->method(), $this->request->fullUrl())
        );

        return $order;
    }
}