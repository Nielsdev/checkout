<?php

namespace Qup\Checkout\Order\Resolvers;

use ProBiz\Order\OrderNew;
use ProBiz\Order\Transaction\TransactionHandler;
use ProBiz\Order\Transaction\TransactionStatus;
use \Qup\Checkout\Order\Contracts\ShoppingCart;
use \Qup\Checkout\Order\Contracts\Order;
use \Qup\Checkout\Order\OrderEvents;

class ShoppingCartResolver extends OrderResolver implements ShoppingCart
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_CART;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_CART;

    /**
     * Converts current ShoppingCart
     *
     * @param \ProBiz\Order\ShoppingCart $order
     * @throws
     * @return \ProBiz\Order\ShoppingCart
     */
    protected function convertFromShoppingCart(\ProBiz\Order\ShoppingCart $order)
    {
        return $order;
    }

    /**
     * @param \ProBiz\Order\OrderSucceeded $order
     * @return \ProBiz\Order\ShoppingCart
     */
    protected function convertFromOrderSucceeded(\ProBiz\Order\OrderSucceeded $order)
    {
        // Release all existing locks.
        $order->release();

        return \Qup\Checkout\Order\OrderFactory::createNew();
    }

    /**
     * Converts current OrderNew to ShoppingCart
     * 
     * @param OrderNew $order
     * @throws
     * @return \ProBiz\Order\ShoppingCart
     */
    protected function convertFromOrderNew(\ProBiz\Order\OrderNew $order)
    {
        $order->convertToCart('Automatically converted by OrderResolver');

        return $order;
    }

    /**
     * Converts current OrderOpen to ShoppingCart
     * 
     * @param \ProBiz\Order\OrderOpen $order
     * @return \ProBiz\Order\ShoppingCart
     */
    protected function convertFromOrderOpen(\ProBiz\Order\OrderOpen $order)
    {
        // Update all transactions.
        TransactionHandler::updateOpenTransactions($order);

        foreach ($order->transactions as $transaction) {
            if (!in_array($transaction->getStatus(), TransactionStatus::getOpenStatuses()) && !in_array($transaction->getStatus(), TransactionStatus::getSucceededStatuses())) {
                continue;
            }

            // So there is open transactions, which we can't close.
            // This order can't be recovered, so we return a new one.
            \Log::debug('Open/succeeded transaction found in this order, spawning new shoppingCart');
            return \Qup\Checkout\Order\OrderFactory::createNew();
        }

        $order->convertToShoppingCart('Automatically converted from ShoppingCartResolver');

        return $order;
    }
}