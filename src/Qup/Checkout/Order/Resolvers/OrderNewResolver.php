<?php

namespace Qup\Checkout\Order\Resolvers;

use ProBiz\Order\Transaction\TransactionHandler;
use ProBiz\Order\Transaction\TransactionStatus;
use Qup\Checkout\Order\Contracts\OrderNew;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;
use Qup\Checkout\Order\Exceptions\UnsupportedStatusTransitionException;
use Qup\Checkout\Order\OrderEvents;

class OrderNewResolver extends OrderResolver implements OrderNew
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_NEW;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_NEW;

    /**
     * Returns current OrderNew
     *
     * @param \ProBiz\Order\OrderNew $order
     * @throws
     * @return \ProBiz\Order\OrderNew
     */
    protected function convertFromOrderNew(\ProBiz\Order\OrderNew $order)
    {
        return $order;
    }

    /**
     * Handle the conversion to OrderNew
     *
     * @param \ProBiz\Order\OrderOpen $order
     */
    protected function convertFromOrderOpen(\ProBiz\Order\OrderOpen $order)
    {
        // Update all transactions.
        TransactionHandler::updateOpenTransactions($order);

        foreach ($order->transactions as $transaction) {
            if (!in_array($transaction->getStatus(), TransactionStatus::getOpenStatuses()) && !in_array($transaction->getStatus(), TransactionStatus::getSucceededStatuses())) {
                continue;
            }

            // So there is open transactions, which we can't close.
            // This order can't be recovered, so we return a new one.
            throw new NotInPrerequisiteStateException('Unable to convert to OrderNew, completed/open transactions found');
        }

        $order->convertToNew('Automatically converted from OrderNewResolver');

        return $order;
    }

    /**
     * Converts current ShoppingCart to OrderNew
     *
     * @param \ProBiz\Order\ShoppingCart $order
     * @throws
     * @return \ProBiz\Order\OrderNew
     */
    protected function convertFromShoppingCart(\ProBiz\Order\ShoppingCart $order)
    {
        $order->convertToOrder('Automatically converted from OrderNewResolver');

        return $order;
    }
}