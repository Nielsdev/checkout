<?php

namespace Qup\Checkout\Order\Resolvers;

use Qup\Checkout\Order\Contracts\OrderOpen;
use Qup\Checkout\Order\OrderEvents;

class OrderOpenResolver extends OrderResolver implements OrderOpen
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_OPEN;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_OPEN;

    /**
     * Returns current OrderOpen
     *
     * @param \ProBiz\Order\OrderOpen $order
     * @throws
     * @return \ProBiz\Order\OrderOpen
     */
    protected function convertFromOrderOpen(\ProBiz\Order\OrderOpen $order)
    {
        return $order;
    }

    /**
     * Converts current OrderNew to OrderOpen
     *
     * @param \ProBiz\Order\OrderNew $order
     * @throws
     * @return \ProBiz\Order\OrderOpen
     */
    protected function convertFromOrderNew(\ProBiz\Order\OrderNew $order)
    {
        $order->open('Automatically converted by OrderOpenResolver');

        return $order;
    }
}