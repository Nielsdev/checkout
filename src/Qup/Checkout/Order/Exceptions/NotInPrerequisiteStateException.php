<?php

namespace Qup\Checkout\Order\Exceptions;

class NotInPrerequisiteStateException extends \Exception {}