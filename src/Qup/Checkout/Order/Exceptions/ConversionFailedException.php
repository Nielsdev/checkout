<?php

namespace Qup\Checkout\Order\Exceptions;

class ConversionFailedException extends \Exception {}