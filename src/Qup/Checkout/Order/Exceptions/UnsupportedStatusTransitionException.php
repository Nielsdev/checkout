<?php

namespace Qup\Checkout\Order\Exceptions;

class UnsupportedStatusTransitionException extends \Exception {}