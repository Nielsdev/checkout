<?php

namespace Qup\Checkout\Order;

abstract class OrderEvents
{
    /**
     * Status change events
     *
     * These events are fired BEFORE the conversion will take place.
     * These events will throw a NotInPrerequisiteStateException if the conversion is not allowed.
     *
     * Through the checkout.php configuration file you can set which sub-events should be called.
     */
    const BEFORE_STATUS_CHANGE_GLOBAL  = 'order.statuschange.before.global';      // Fired before every status switch.
    const BEFORE_STATUS_CHANGE_CART    = 'order.statuschange.before.cart';        // Fired before a switch towards ShoppingCart
    const BEFORE_STATUS_CHANGE_NEW     = 'order.statuschange.before.new';         // Fired before a switch towards OrderNew
    const BEFORE_STATUS_CHANGE_OPEN    = 'order.statuschange.before.open';        // Fired before a switch towards OrderOpen
    const BEFORE_STATUS_CHANGE_FAILED  = 'order.statuschange.before.failed';      // Fired before a switch towards OrderFailed
    const BEFORE_STATUS_CHANGE_SUCCESS = 'order.statuschange.before.success';     // Fired before a switch towards OrderSuccess

    /**
     * Status change events
     *
     * These events are fired AFTER the conversion will take place.
     * Typically these events will throw a ConversionFailedException if the conversion does not pass the test.
     *
     * Through the checkout.php configuration file you can set which sub-events should be called.
     */
    const AFTER_STATUS_CHANGE_GLOBAL  = 'order.statuschange.after.global';      // Fired after every status switch.
    const AFTER_STATUS_CHANGE_CART    = 'order.statuschange.after.cart';        // Fired after a switch towards ShoppingCart
    const AFTER_STATUS_CHANGE_NEW     = 'order.statuschange.after.new';         // Fired after a switch towards OrderNew
    const AFTER_STATUS_CHANGE_OPEN    = 'order.statuschange.after.open';        // Fired after a switch towards OrderOpen
    const AFTER_STATUS_CHANGE_FAILED  = 'order.statuschange.after.failed';      // Fired after a switch towards OrderFailed
    const AFTER_STATUS_CHANGE_SUCCESS = 'order.statuschange.after.success';     // Fired after a switch towards OrderSuccess
}