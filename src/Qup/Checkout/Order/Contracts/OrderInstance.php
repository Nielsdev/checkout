<?php

namespace Qup\Checkout\Order\Contracts;

use Qup\Database\ILockable;
use \stdClass;
use ProBiz\Fulfillment\FulfillmentOrder;
use ProBiz\IO\Database\SimpleDbObject;
use ProBiz\Order\OrderLine;
use ProBiz\Order\OrderStatus;
use ProBiz\Order\OrderTotals;
use ProBiz\Order\Transaction\Transaction;

interface OrderInstance extends ILockable
{
    public function getId();

    /**
     * Creates and loads an OrderLine from the supplied data.
     * EVENT_CREATE_CHILD_FROM_STATEMENT handler.
     * @param integer $iEvent the event to be raised
     * @param array $aItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return OrderLine
     * @throws \InvalidArgumentException
     */
    public function onCreateOrderLine($iEvent, $aItem, $oSender);
    
    /**
     * Creates and loads a FulfillmentOrder from the supplied data.
     * EVENT_CREATE_CHILD_FROM_STATEMENT handler.
     * @param integer $iEvent the event to be raised
     * @param array $aItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return FulfillmentOrder
     * @throws \InvalidArgumentException
    */
    public function onCreateFulfillmentOrder($iEvent, $aItem, $oSender);
    
    /**
     * Creates and loads a Transaction from the supplied data.
     * EVENT_CREATE_CHILD_FROM_STATEMENT handler.
     * @param integer $iEvent the event to be raised
     * @param array $aItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return Transaction
     * @throws \InvalidArgumentException
    */
    public function onCreateTransaction($iEvent, $aItem, $oSender);
    
    /**
     * Gets the grand total
     * @param integer $iType The VAT type to get, possible options are
     *               - OrderTotals::TAX_INC
     *               - OrderTotals::TAX_EX
     *               - OrderTotals::TAX_VAT
     *               - OrderTotals::TAX_VAT_LOW
     *               - OrderTotals::TAX_VAT_HIGH
     * @return float Returns the grand total
     * @throws \InvalidArgumentException If VAT type is not valid
    */
    public function getGrandTotal($iType = OrderTotals::TAX_INC);
    
    /**
     * Returns the frontend ID.
     * @return string
    */
    public function getFrontendID();
    
    /**
     * Retrieves Site ID
     * @return integer|null
    */
    public function getSiteID();
    
    /**
     * Retrieves ResellerSite ID
     * @return integer|null
     * @deprecated use getSiteID()
    */
    public function getResellerSiteID();
    
    /**
     * Returns the external ID
     * @return string
    */
    public function getExternalID();
    
    /**
     * Sets the external order ID.
     * @param string $newVal
    */
    public function setExternalID($newVal);
    
    /**
     * Sets the external order ID and immediately saves it to the database.
     * @param mixed $newVal
    */
    public function setAndSaveExternalID($newVal);
    
    /**
     * Returns the affiliate ID this order was placed through.
     * @return integer
    */
    public function getAffiliateID();
    
    /**
     * Returns the affiliate data registered when the order was placed.
     * @return string
    */
    public function getAffiliateData();
    
    /**
     * Returns the status of the supplied type.
     * @param integer $iStatusType (defaults to OrderStatus::TYPE_ORDER)
     * @return integer Status
     * @see OrderStatus::TYPE_*
     * @throws \InvalidArgumentException on unknown type
    */
    public function getStatus($iStatusType = OrderStatus::TYPE_ORDER);
    
    /**
     * Returns the supplied IDs of the supplied fulfillment order type.
     * @param integer $iFulfillmentOrderType (@see FulfillmentOrderFactory::TYPE_*)
     * @return array of integer IDs
    */
    public function getSupplierIDs($iFulfillmentOrderType);
    
    /**
     * Returns whether adding of an OrderLine is allowed.
     * @param integer $iEvent the event to be raised
     * @param mixed $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return boolean Allowed
    */
    public function onBeforeAddOrderLine($iEvent, $mItem, $oSender);
    
    /**
     * Handles adding of OrderLine
     * @param integer $iEvent the event to be raised
     * @param mixed $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
    */
    public function onAfterAddOrderLine($iEvent, $mItem, $oSender);
    
    /**
     * Returns whether removing of an OrderLine is allowed.
     * @param integer $iEvent the event to be raised
     * @param mixed $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return boolean Allowed
    */
    public function onBeforeRemoveOrderLine($iEvent, $mItem, $oSender);
    
    /**
     * Handle changing of OrderLine
     * @param integer $iEvent the event to be raised
     * @param mixed $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
    */
    public function onAfterChangeOrderLine($iEvent, $mItem, $oSender);
    
    /**
     * Returns whether adding of a Transaction is allowed.
     * @param integer $iEvent the event to be raised
     * @param mixed $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return boolean Allowed
    */
    public function onBeforeAddTransaction($iEvent, $mItem, $oSender);
    
    /**
     * Handles adding of a Transaction.
     * @param integer $iEvent the event raised
     * @param SimpleDbObject $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
    */
    public function onAfterAddTransaction($iEvent, $mItem, $oSender);
    
    /**
     * Handles changing of a Transaction.
     * @param integer $iEvent the event raised
     * @param mixed $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
    */
    public function onAfterChangeTransaction($iEvent, $mItem, $oSender);
    
    /**
     * Returns whether removing a Transaction is allowed.
     * @param integer $iEvent the event to be raised
     * @param SimpleDbObject $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return boolean Allowed
    */
    public function onBeforeRemoveTransaction($iEvent, $mItem, $oSender);
    
    /**
     * Returns whether adding of an OrderMail is allowed.
     * @param integer $iEvent the event to be raised
     * @param SimpleDbObject $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return boolean Allowed
    */
    public function onBeforeAddMail($iEvent, $mItem, $oSender);
    
    /**
     * Handles adding of an OrderMail
     * @param integer $iEvent the event to be raised
     * @param SimpleDbObject $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
    */
    public function onAfterAddMail($iEvent, $mItem, $oSender);
    
    /**
     * Returns whether removing an OrderMail is allowed.
     * @param integer $iEvent the event to be raised
     * @param mixed $mItem The affected item
     * @param stdClass $oSender The object that raised the event (Collection)
     * @return boolean Allowed
    */
    public function onBeforeRemoveMail($iEvent, $mItem, $oSender);
    
    /**
     * Register the supplied mail as sent.
     * @param integer $iType (@see OrderMail::TYPE_*)
     * @param string $sSender
     * @param string $sRecipient
     * @param string $sSubject (defaults to null)
     * @param integer $iItemID (defaults to null)
     * @param string $sContent (defaults to null)
     * @param integer|null $iLangID
    */
    public function sentMail($iType, $sSender, $sRecipient, $sSubject = null, $iItemID = null, $sContent = null, $iLangID = null);
    
    /**
     * Loads the item from the supplied array.
     * @param array $aData The data
     * @throws \LogicException on status mismatch
    */
    public function loadFromArray($aData);
    
    /**
     * Saves the object.
    */
    public function save();
    
    /**
     * Retrieves the actionIDs
     * @return int[]
    */
    public function getActionIDs();
    
    /**
     * Returns an array placeholders filled by this object
     * @param string $sPrefix Optional additional prefix
     * @return array Placeholders
    */
    public function getPlaceholders($sPrefix = '');
    
    /**
     * Deletes this order and selected child objects from the database.
     * @throws \BadMethodCallException on deleting unsaved item
    */
    public function delete();
}