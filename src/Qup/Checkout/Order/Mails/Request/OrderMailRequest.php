<?php

namespace Qup\Checkout\Order\Mails\Request;

class OrderMailRequest extends MailRequest
{
    public function getTemplate()
    {
        return 'emails.order_confirmation';
    }
}