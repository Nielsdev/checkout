<?php

namespace Qup\Checkout\Order\Mails\Request;


class BarcodeAlertMailRequest extends MailRequest
{

    /**
     * Gets the correct blade template for this mail request
     *
     * @return string
     */
    public function getTemplate()
    {
        return 'emails.barcode_alert';
    }
}