<?php

namespace Qup\Checkout\Order\Mails\Request;

use \Qup\Queue\SelfHandlingJob;

abstract class MailRequest extends SelfHandlingJob
{
    protected $placeholders;

    protected $recipient;
    protected $recipientName;
    protected $cc;
    protected $ccName;
    protected $from;
    protected $fromName;
    protected $subject;
    protected $attachments;

    protected $siteId;

    /**
     * MailRequest constructor.
     * @param array $placeholders
     */
    public function __construct($placeholders = [])
    {
        $this->placeholders = $placeholders;
    }

    /**
     * @param $recipient
     * @param $recipientName
     */
    public function setRecipient($recipient, $recipientName)
    {
        $this->recipient = $recipient;
        $this->recipientName = $recipientName;
    }

    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param $cc
     * @param $ccName
     */
    public function setCC($cc, $ccName)
    {
        $this->cc = $cc;
        $this->ccName = $ccName;
    }

    /**
     * @param $from
     * @param $fromName
     */
    public function setFrom($from, $fromName)
    {
        $this->from = $from;
        $this->fromName = $fromName;
    }

    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Sets the siteId of this request.
     *
     * @param $siteId
     */
    public function setSiteId($siteId)
    {
        $this->siteId = (int)$siteId;
    }

    /**
     * @param $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return array
     */
    public function getPlaceholders()
    {
        return $this->placeholders;
    }

    /**
     * @param array $placeholders
     */
    public function setPlaceholders($placeholders)
    {
        $this->placeholders = $placeholders;
    }

    public abstract function getTemplate();

    /**
     * @param $message
     */
    public function handleAttachments($message)
    {

    }

    /**
     *
     */
    public function execute()
    {
        session()->put('Site_ID', $this->siteId);

        // @todo fix
        $template = $this->getTemplate();

        $html = view($template, $this->placeholders);

        \Mail::send($template, $this->placeholders, function ($message) {
            $message->from($this->from, $this->fromName);
            $message->sender($this->from, $this->fromName);
            $message->to($this->recipient, $this->recipientName);
            $message->replyTo($this->from, $this->fromName);
            $message->setContentType('text/html');

            $this->handleAttachments($message);

            $message->subject($this->subject);
        });
    }
}