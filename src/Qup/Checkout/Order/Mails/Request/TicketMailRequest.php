<?php

namespace Qup\Checkout\Order\Mails\Request;

use ProBiz\Fulfillment\Ticket\Ticket;
use ProBiz\Fulfillment\Ticket\TicketFactory;
use ProBiz\Fulfillment\Ticket\TicketPDF;
use ProBiz\Fulfillment\Ticket\TicketURL;
use QupTools\Jobs\Messages\Attachment;

class TicketMailRequest extends MailRequest
{
    /**
     * @var \ProBiz\Fulfillment\Ticket\Ticket
     */
    private $ticket;
    /**
     * @var integer
     */
    private $fulfillmentOrderId;

    /**
     * TicketMailRequest constructor.
     *
     * @param integer $fulfillmentOrderId
     * @param array $placeholders
     */
    public function __construct($fulfillmentOrderId, $placeholders = [])
    {
        parent::__construct($placeholders);

        $this->fulfillmentOrderId = $fulfillmentOrderId;
    }

    /**
     *
     */
    public function determineTicket()
    {
        sleep(2);

        $this->ticket = TicketFactory::loadForFulfillmentOrderID($this->fulfillmentOrderId);
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        $this->determineTicket();

        if ($this->ticket instanceof TicketPDF) {
            return 'emails.ticket_pdf';
        }
        if ($this->ticket instanceof TicketURL) {
            $this->placeholders["ticket_url"] = $this->ticket->getURL();
            return 'emails.ticket_url';
        }

        return '';
    }

    /**
     * Handles the attachments.
     *
     * @param $message
     */
    public function handleAttachments($message)
    {
        parent::handleAttachments($message);

        if ($this->ticket instanceof TicketURL) {
            return;
        }

        $disk  = \Storage::disk('tickets');
        $ticket = $this->ticket;
        $fileName = $ticket->getTicket();

        if (!$disk->has($fileName)) {
            $disk  = \Storage::disk('tickets_legacy');
            if (!$disk->has($fileName)) {
                // exception shiz
                return;
            }
        }

        $message->attachData($disk->get($fileName), 'tickets_' . $this->ticket->getFulfillmentOrderID() . '.pdf', ['mime' => 'application/pdf']);
    }

    /**
     * Returns the attachments.
     *
     * @return Attachment[] The attachments.
     * @throws \RuntimeException Thrown in case no ticket could be determined.
     */
    public function getAttachments()
    {
        $this->determineTicket();

        if (empty($this->ticket)) {
            throw new \RuntimeException('No tickets could be determined.');
        }
        if ($this->ticket instanceof TicketURL) {
            return [];
        }

        $disk_name = 'tickets';
        $disk  = \Storage::disk($disk_name);
        $file_name = $this->ticket->getTicket();

        if (!$disk->has($file_name)) {
            $disk_name = 'tickets_legacy';
            $disk  = \Storage::disk($disk_name);
            if (!$disk->has($file_name)) {
                // exception shiz
                return [];
            }
        }

        return [
            new Attachment(
                $disk_name,
                $file_name,
                'tickets_' . $this->ticket->getFulfillmentOrderID() . '.pdf',
                true,
                ['mime' => 'application/pdf']
            ),
        ];
    }

    /**
     * Returns the description of this task.
     *
     * @return string The description.
     */
    protected function getDescription()
    {
        return 'Mail ticket(s) for fulfillment order '.$this->fulfillmentOrderId;
    }
}