<?php

namespace Qup\Checkout\Order\Mails\Listeners;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use ProBiz\Cache\ObjectCacheController;
use ProBiz\Core\Site\SubSiteFactory;
use ProBiz\Fulfillment\FulfillmentOrderFactory;
use ProBiz\Fulfillment\FulfillmentOrderStatus;
use ProBiz\Fulfillment\Ticket\TicketFactory;
use ProBiz\Fulfillment\Ticket\TicketURL;
use ProBiz\Order\OrderLine;
use ProBiz\Order\OrderLineHandler;
use ProBiz\Order\OrderMail;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Queue\Dispatcher;
use QupTools\Jobs\Messages\SendMail;
use QupTools\Jobs\Messages\Attachment;
use QupTools\Providers\QupToolsProvider;

class SendTicketMail
{
    use DispatchesJobs;


    /**
     * SendOrderConfirmationMail constructor.
     */
    public function __construct()
    {

    }

    /**
     *
     * @param OrderInstance $order
     */
    public function handle(OrderInstance $order)
    {
        $info = $order->orderInfo;
        $lines = $order->orderLines;
        $site_code = SubSiteFactory::getActiveSiteCode();
        $config_prefix = 'mail.tickets.' . $site_code . '.';
        $phone = trans($config_prefix .'phone');
        
        $placeholders = $info->getPlaceholders();
        $placeholders['phone'] = $phone;
        $placeholders['phone_tel'] = preg_replace('/\D+/','', $phone);
        $placeholders['email'] = trans($config_prefix .'from.address');
        $placeholders['sender'] = trans($config_prefix .'from.name');
        $placeholders['subject'] = trans($config_prefix .'subject');
        $placeholders['order_id'] = $order->getFrontendID();

        foreach ($lines as $line) {
            if (!OrderLineHandler::isEffectivelyActive($line, $lines)) {
                continue;
            }
            if ($line->getType() !== OrderLine::TYPE_PRODUCT) {
                continue;
            }

            $shopId = $line->getShopID();
            break;
        }

        try {
            $shop = ObjectCacheController::get(ObjectCacheController::ITEM_SHOP, $shopId);
            $shop_title = $shop->getTitle();
        } catch (\Exception $e) {
            $shop_title = '';
            Log::error('Shop ID='.$shopId.' could not be loading while preparing OrderConfirmationMail for order ID='.$order->getId().' with exception '.$e->getMessage());
        }

        $placeholders['shop_title'] = $shop_title;

        // @todo Get locale from order info
        switch ($site_code) {
            case 'DAGR':
                $locale = 'da_DK';
                break;
            default:
                $locale = 'nl_NL';
                break;
        }

        // Set locale
        $old_locale = App::getLocale();
        App::setLocale($locale.'.utf8');

        $ticketOrders = 0;
        foreach ($order->fulfillmentOrders as $fulfillmentOrder) {
            // This one is just for handling Ticket fulfillment orders
            if ($fulfillmentOrder->getType() !== FulfillmentOrderFactory::TYPE_TICKET) {
                continue;
            }

            if ($fulfillmentOrder->getStatus() == FulfillmentOrderStatus::CANCELLED) {
                // we should only send tickets for non-cancelled fulfillment orders
                continue;
            }

            $ticketOrders += 1;

            // Create job
            try {
                $ticket = TicketFactory::loadForFulfillmentOrderID($fulfillmentOrder->getID());

                if ($ticket instanceof TicketURL){
                    $sendMailType = SendMail::TYPE_TICKETS_URL;
                    $placeholders['ticket_url'] = $ticket->getURL();
                } else {
                    $sendMailType = SendMail::TYPE_TICKETS_PDF;
                }

                $message = new SendMail(
                    $sendMailType,
                    $site_code,
                    $locale,
                    trans($config_prefix .'subject'),
                    $placeholders,
                    $info->getBillingEmail(),
                    $info->getBillingConstructedFullName(),
                    trans($config_prefix .'from.address'),
                    trans($config_prefix .'from.name')
                );

                $attachments = $this->getAttachments($fulfillmentOrder->getID());
                foreach ($attachments as $attachment) {
                    $message->addAttachment($attachment);
                }

                $dispatcher = App::make(Dispatcher::class);
                $dispatcher->dispatch($message, QupToolsProvider::CHANNEL_SEND_MAIL);
            } catch (\Exception $e) {
                Log::error('TicketMailRequest could not be dispatched for order ID='.$order->getId().' and FO ID='.$fulfillmentOrder->getID().' with exception '.$e->getMessage());
                continue;
            }
        }

        if (empty($ticketOrders)) {
            Log::alert('No non-cancelled ticket fulfillment orders found for order ID='.$order->getId());
        }

        // Restore locale
        App::setLocale($old_locale);
    }

    /**
     * Returns the attachments.
     *
     * @param integer $fulfillmentOrderId
     * @return Attachment[] The attachments.
     * @throws \RuntimeException Thrown in case no ticket could be determined.
     */
    protected function getAttachments($fulfillmentOrderId)
    {
        sleep(2);

        // Get ticket
        $ticket = TicketFactory::loadForFulfillmentOrderID($fulfillmentOrderId);
        if (empty($ticket)) {
            throw new \RuntimeException('No tickets could be determined.');
        }
        if ($ticket instanceof TicketUR) {
            return [];
        }

        $disk_name = 'tickets';
        $disk  = \Storage::disk($disk_name);
        $file_name = $ticket->getTicket();

        if (!$disk->has($file_name)) {
            $disk_name = 'tickets_legacy';
            $disk  = \Storage::disk($disk_name);
            if (!$disk->has($file_name)) {
                // exception shiz
                return [];
            }
        }

        return [
            new Attachment(
                $disk_name,
                $file_name,
                'tickets_' . $ticket->getFulfillmentOrderID() . '.pdf',
                true,
                ['mime' => 'application/pdf']
            ),
        ];
    }
}