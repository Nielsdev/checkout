<?php

namespace Qup\Checkout\Order\Mails\Listeners;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\App;
use ProBiz\Cache\ObjectCacheController;
use ProBiz\Core\Site\SubSiteFactory;
use ProBiz\Order\OrderLine;
use ProBiz\Order\OrderLineHandler;
use ProBiz\Order\OrderMail;
use ProBiz\Order\OrderTotals;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\MailClient\Request\MailMessage;
use Qup\MailClient\Request\MailRequest;
use Qup\Queue\Dispatcher;
use Qup\TicketClient\Transformers\TicketConfig;
use QupTools\Jobs\Messages\SendMail;
use QupTools\Providers\QupToolsProvider;

class SendOrderConfirmationMail
{
    use DispatchesJobs;

    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * SendOrderConfirmationMail constructor.
     *
     * @param OrderInstance $order
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     *
     */
    public function handle()
    {
        $lines = $this->order->orderLines;
        $info = $this->order->orderInfo;
        $infoPlaceholders = $info->getPlaceholders();
        $siteCode = SubSiteFactory::getActiveSiteCode();

        switch ($siteCode) {
            case 'DAGR':
                $locale = 'da_DK.UTF-8';
                break;
            default:
                $locale = 'nl_NL.UTF-8';
                break;
        }

        // Set locale
        $old_locale = App::getLocale();
        App::setLocale($locale);


        $printMandatory = false;
        $productLines = [];
        foreach ($lines as $line) {
            if (!OrderLineHandler::isEffectivelyActive($line, $lines)) {
                continue;
            }
            if ($line->getType() !== OrderLine::TYPE_PRODUCT) {
                continue;
            }

            $shopId = $line->getShopID();
            $date = $line->getProductDate()->format('D M Y');

            $productLines[] = [
                'shopid' => $line->getShopID(),
                'unitPrice' => money_format('%i',$line->getUnitPriceInc()),
                'quantity' => $line->getQuantity(),
                'subTotal' => money_format('%i',($line->getUnitPriceInc() * $line->getQuantity())),
                'title' => preg_replace('/ - NORANK$/', '', $line->getProductTitle()),
                'date' => $line->getProductDate()->format('D. j F Y')
            ];

            $config = new TicketConfig();
            $config->createFromOrderLine($line);

            if(!$printMandatory) {
                $printMandatory = $config->printMandatory;
            }
        }

        try {
            $shop = ObjectCacheController::get(ObjectCacheController::ITEM_SHOP, $shopId);
            $shop_title = $shop->getTitle();
        } catch (\Exception $e) {
            $shop_title = '';
            \Log::error('Shop ID='.$shopId.' could not be loaded while preparing OrderConfirmationMail for order ID='.$this->order->getId().' with exception '.$e->getMessage());
        }

        $totals = new OrderTotals($this->order);
        $placeholders['lines'] = $productLines;
        $placeholders['booking_costs_total'] = (empty($totals->getTotal(OrderTotals::TYPE_BOOKING)) ? null : money_format('%i',$totals->getTotal(OrderTotals::TYPE_BOOKING)));
        $placeholders['insurance_costs_total'] = (empty($totals->getTotal(OrderTotals::TYPE_INSURANCES)) ? null : money_format('%i',$totals->getTotal(OrderTotals::TYPE_INSURANCES)));
        $placeholders['discount_total'] = (empty($totals->getTotal(OrderTotals::TYPE_DISCOUNTS)) ? null : money_format('%i',$totals->getTotal(OrderTotals::TYPE_DISCOUNTS)));
        $placeholders['grand_total'] = money_format('%i',$totals->getTotal(OrderTotals::TYPE_GRAND));
        $placeholders['shop_title'] = $shop_title;
        $placeholders['order_id'] = $this->order->getFrontendID();
        $placeholders['date'] = $date;
        $placeholders['assets_url'] = config('mail.assets_base_url');
        $placeholders['resend_url'] = config('mail.resend_url');

        $placeholders['print_mandatory'] = $printMandatory;

        $config_prefix = 'mail.order.confirmation.';
        $phone = trans($config_prefix .'phone');

        $placeholders['phone'] = $phone;
        $placeholders['phone_tel'] = preg_replace('/\D+/','', $phone);
        $placeholders['email'] = trans($config_prefix .'from.address');
        $placeholders['sender'] = trans($config_prefix .'from.name');
        $placeholders['subject'] = trans($config_prefix .'subject');


        $placeholders = array_merge($infoPlaceholders, $placeholders);


        try {
            $message = new MailMessage(
                MailMessage::TYPE_ORDER_CONFIRMATION,
                $siteCode,
                $locale,
                trans($config_prefix .'subject'),
                $placeholders,
                $info->getBillingEmail(),
                $info->getBillingConstructedFullName(),
                trans($config_prefix .'from.address'),
                trans($config_prefix .'from.name')
            );

            $message->setOrderID($this->order->getId());

            $dispatcher = App::make(Dispatcher::class);
            $dispatcher->dispatch($message, MailRequest::CHANNEL_SEND_MAIL);
        } catch (\Exception $e) {
            \Log::error('OrderMailRequest could not be dispatched for order ID='.$this->order->getId().' with exception '.$e->getMessage());
            App::setLocale($old_locale);
            return;
        }

        // Restore locale
        App::setLocale($old_locale);
    }
}