<?php

namespace Qup\Checkout\Order\Requirements;

use Qup\Checkout\Order\Contracts\OrderOpen;
use Qup\Checkout\Order\OrderEvents;

class OrderOpenRequirements extends OrderRequirements
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_OPEN;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_OPEN;
}