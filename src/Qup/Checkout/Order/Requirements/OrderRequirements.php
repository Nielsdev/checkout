<?php

namespace Qup\Checkout\Order\Requirements;

use \Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Qup\Checkout\Order\OrderEvents;

abstract class OrderRequirements
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_GLOBAL;
    protected $after_event = OrderEvents::BEFORE_STATUS_CHANGE_GLOBAL;

    /**
     * @var DispatcherContract
     */
    private $dispatcher;

    /**
     * OrderRequirements constructor.
     *
     * @param DispatcherContract $dispatcher
     */
    public function __construct(DispatcherContract $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $this->registerEvent($events, $this->after_event);
        $this->registerEvent($events, $this->before_event);
    }

    /**
     * Register Event
     *
     * @param $events
     * @param $event
     */
    public function registerEvent($events, $event)
    {
        $requirements = config($event, []);

        foreach ($requirements as $requirementClass) {
            $events->listen($event, $requirementClass);
        }
    }
}