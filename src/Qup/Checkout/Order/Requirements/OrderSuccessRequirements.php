<?php

namespace Qup\Checkout\Order\Requirements;

use Qup\Checkout\Order\OrderEvents;

class OrderSuccessRequirements extends OrderRequirements
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_SUCCESS;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_SUCCESS;
}