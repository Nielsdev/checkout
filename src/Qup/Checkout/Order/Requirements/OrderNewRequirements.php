<?php

namespace Qup\Checkout\Order\Requirements;

use Qup\Checkout\Order\Contracts\OrderNew;
use Qup\Checkout\Order\OrderEvents;

class OrderNewRequirements extends OrderRequirements
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_NEW;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_NEW;
}