<?php

namespace Qup\Checkout\Order\Requirements;

use Qup\Checkout\Order\OrderEvents;

class OrderFailedRequirements extends OrderRequirements
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_FAILED;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_FAILED;
}