<?php

namespace Qup\Checkout\Order\Requirements;

use Qup\Checkout\Order\Contracts\ShoppingCart;
use Qup\Checkout\Order\OrderEvents;

class ShoppingCartRequirements extends OrderRequirements
{
    /**
     * @var string
     */
    protected $before_event = OrderEvents::BEFORE_STATUS_CHANGE_CART;
    protected $after_event = OrderEvents::AFTER_STATUS_CHANGE_CART;
}