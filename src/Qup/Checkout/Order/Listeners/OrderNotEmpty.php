<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\OrderLine;
use ProBiz\Order\OrderLineHandler;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

class OrderNotEmpty
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderNotEmpty constructor.
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * Handles the event
     */
    public function handle()
    {
        $order = $this->order;
        $productLines = $order->orderLines->getAllWith('getType', OrderLine::TYPE_PRODUCT);

        foreach ($productLines as $productLine) {
            if (OrderLineHandler::isEffectivelyActive($productLine, $order->orderLines)) {
                return;
            }
        }

        throw new NotInPrerequisiteStateException(
            sprintf('The order does not have valid productLines (Triggered by Event; %s)', get_class($this))
        );
    }
}