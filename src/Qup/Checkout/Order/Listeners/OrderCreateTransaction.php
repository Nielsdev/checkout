<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\Transaction\TransactionHandler;
use Qup\Checkout\Contracts\CheckoutInfo;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

class OrderCreateTransaction extends OrderHasSucceededTransaction
{
    /**
     * @var CheckoutInfo
     */
    private $checkoutInfo;

    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderCreateTransaction constructor.
     *
     * @param OrderInstance $order
     * @param CheckoutInfo $checkoutInfo
     */
    public function __construct(OrderInstance $order, CheckoutInfo $checkoutInfo)
    {
        parent::__construct($order);

        $this->order = $order;
        $this->checkoutInfo = $checkoutInfo;
    }

    /**
     * Handle.
     *
     * @throws NotInPrerequisiteStateException
     */
    public function handle()
    {
        try {
            parent::handle();

            // Do not alter if a succeeded transaction exists.
            // @todo This method has to be changed when we allow multiple transactions.
            return;
        } catch (NotInPrerequisiteStateException $ex) {
            // Let's continue...
        }

        if (empty($this->checkoutInfo->getPaymentPlatformId()) || empty($this->checkoutInfo->getPaymentMethodId())) {
            throw new NotInPrerequisiteStateException('CheckoutInfo lacks required information. (Triggered by Event; '.get_class($this).')');
        }

        $transactionId = TransactionHandler::handlePayableTransaction($this->order, $this->checkoutInfo->getPaymentPlatformId(), $this->checkoutInfo->getPaymentMethodId(), $this->checkoutInfo->getPaymentAlias());

        $this->checkoutInfo->setActiveTransactionId($transactionId);
    }
}