<?php

namespace Qup\Checkout\Order\Listeners;

use Illuminate\Validation\Validator;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;
use Qup\Checkout\Order\Contracts\OrderInstance;

class OrderHasEmail
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderNotEmpty constructor.
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * Handles the event
     */
    public function handle()
    {
        $info = $this->order->orderInfo;
        $validator = $this->createEmailValidator(['email' => $info->getBillingEmail()]);

        if ($validator->passes()) {
            return;
        }

        throw new NotInPrerequisiteStateException(
            sprintf('The order does not have a valid email address (Triggered by Event; %s)', get_class($this))
        );
    }

    /**
     * @return Validator
     */
    private function createEmailValidator($data)
    {
       return \Validator::make($data, [
           'email' => 'required|email'
       ]);
    }
}