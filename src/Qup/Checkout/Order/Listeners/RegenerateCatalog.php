<?php

namespace Qup\Checkout\Order\Listeners;

use Illuminate\Http\Request;
use MrTicket\Product\DateTimeRankSubProduct;
use ProBiz\Cache\ObjectCacheController;
use \Qup\Checkout\Catalog\Contracts\CartCatalog;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

class RegenerateCatalog
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * RegenerateContext constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request, CartCatalog $catalog)
    {
        $this->request = $request;
        $this->catalog = $catalog;
    }

    /**
     * @throws NotInPrerequisiteStateException
     */
    public function handle()
    {
        if ($this->catalog->isInitialized()) {
            return;
        }

        $tickets = $this->request->get('tickets');
        $firstProduct = empty($tickets) ? [] : reset($tickets);

        if (!empty($firstProduct['id'])) {
            try {
                $product = ObjectCacheController::get(ObjectCacheController::ITEM_DATETIMERANKSUBPRODUCT, $firstProduct['id']);

                $mainProductId = $product->getShopLocationMainProductID();

                $date = ($product->getDate() == null) ? null : $product->getDate()->format();
                $time = ($product->getTime() == null) ? null : $product->getTime();

                $this->catalog->setData([
                    'shopLocationMainProducts' => [$mainProductId],
                    'date' => $date,
                    'time' => $time
                ]);

                // Success!
                return;
            } catch (\Exception $ex) {
                // Noop, as an exception is thrown below.
            }
        }

        throw new NotInPrerequisiteStateException(
            sprintf('The order does not have a valid context and an attempt to regenerate it failed. (Triggered by Event; %s)', get_class($this))
        );
    }
}