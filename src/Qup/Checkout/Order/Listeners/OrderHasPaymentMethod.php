<?php

namespace Qup\Checkout\Order\Listeners;

use Qup\Checkout\Contracts\CheckoutInfo;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;


class OrderHasPaymentMethod extends OrderHasCompletedTransaction
{
    /**
     * @var CheckoutInfo
     */
    private $info;

    /**
     * OrderHasPaymentMethod constructor.
     *
     * @param CheckoutInfo $info
     * @param OrderInstance $order
     */
    public function __construct(CheckoutInfo $info, OrderInstance $order)
    {
        parent::__construct($order);

        $this->info = $info;
    }

    /**
     * Handles the event
     */
    public function handle()
    {
        try {
            parent::handle();
            return;
        }
        catch (NotInPrerequisiteStateException $ex) {
            // No completed transactions
            // We should rely on info now.
        }

        if (!empty($this->info->getPaymentMethodId())) {
            return;
        }

        throw new NotInPrerequisiteStateException(
            sprintf('No payment method was supplied (Triggered by Event; %s)', get_class($this))
        );
    }
}