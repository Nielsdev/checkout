<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\OrderLine;
use ProBiz\Order\OrderLineHandler;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

class OrderHasCorrectDiscount
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderHasCorrectDiscount constructor.
     *
     * @param OrderInstance $order
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * Detects invalid discount applications.
     * @throws NotInPrerequisiteStateException
     */
    public function handle()
    {
        $discountQuantity = 0;
        $productQuantity = 0;

        foreach ($this->order->orderLines as $orderLine) {
            /** @var OrderLine $orderLine **/
            if (!in_array($orderLine->getType(), [OrderLine::TYPE_DISCOUNT, OrderLine::TYPE_PRODUCT])) {
                continue;
            }

            if (!OrderLineHandler::isEffectivelyActive($orderLine, $this->order->orderLines)) {
                continue;
            }

            if ($orderLine->getType() == OrderLine::TYPE_DISCOUNT) {
                $discountQuantity += $orderLine->getQuantity();
            }

            if ($orderLine->getType() == OrderLine::TYPE_PRODUCT) {
                $productQuantity += $orderLine->getQuantity();
            }
        }

        // This is good.
        if ($productQuantity >= $discountQuantity) {
            return;
        }

        // He has gotten too much discount, let's not allow this order.
        $this->order->abandon();

        throw new NotInPrerequisiteStateException('The order has incorrect discount, abandon!');
    }
}
