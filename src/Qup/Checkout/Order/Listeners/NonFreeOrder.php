<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\OrderTotals;
use ProBiz\Payment\PaymentMethodFactory;
use ProBiz\Payment\PaymentPlatformFactory;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;
use Qup\Checkout\Contracts\CheckoutInfo;

class NonFreeOrder
{
    /**
     * @var CheckoutInfo
     */
    private $checkoutInfo;

    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * NonFreeOrder constructor.
     *
     * @param OrderInstance $order
     * @param CheckoutInfo $checkoutInfo
     */
    public function __construct(OrderInstance $order, CheckoutInfo $checkoutInfo)
    {
        $this->order = $order;
        $this->checkoutInfo = $checkoutInfo;
    }

    /**
     * Detects free payment method when not allowed
     * @throws NotInPrerequisiteStateException
     */
    public function handle()
    {
        if ($this->order->totals->getTotal(OrderTotals::TYPE_GRAND, OrderTotals::TAX_INC, null, 2) == 0.00) {
            return;
        }

        if ($this->checkoutInfo->getPaymentPlatformId()!=PaymentPlatformFactory::PSP_VIRTUAL) {
            return;
        }
        if ($this->checkoutInfo->getPaymentMethodId()!=PaymentMethodFactory::METHOD_VIRTUAL_FREE) {
            return;
        }

        // consumer needs to pay for this order, but currently virtual-free selected
        throw new NotInPrerequisiteStateException('The order is not free (Triggered by Event; '.get_class($this).')');
    }
}
