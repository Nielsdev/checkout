<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\Transaction\Transaction;
use ProBiz\Order\Transaction\TransactionStatus;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

/**
 * @todo rename this class to reflect the current logic
 */
class OrderHasNoCompletedTransaction
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderHasNoCompletedTransaction constructor.
     *
     * @param OrderInstance $order
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * @throws NotInPrerequisiteStateException
     */
    public function handle()
    {
        $requirements = [];
        $requirements[] = ['getType', Transaction::TYPE_DEBIT];
        $requirements[] = ['getStatus', TransactionStatus::getSucceededStatuses()];

        $completedTransactions = $this->order->transactions->getAllWithAll($requirements, true);

        if (!count($completedTransactions)) {
            return;
        }

        throw new NotInPrerequisiteStateException('Order has completed transactions. (Triggered by Event; '.get_class($this).')');
    }
}