<?php

namespace Qup\Checkout\Order\Listeners;

use \Qup\Checkout\Contracts\CheckoutInfo;
use \Qup\Checkout\Order\Contracts\OrderInstance;

class BindOrderToCheckoutInfo
{
    /**
     * @var CheckoutInfo
     */
    private $checkoutInfo;

    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * RegenerateContext constructor.
     *
     * @param CheckoutInfo $info
     * @param OrderInstance $order
     */
    public function __construct(CheckoutInfo $info, OrderInstance $order)
    {
        $this->checkoutInfo = $info;
        $this->order = $order;
    }

    /**
     * Escalates the new order to the CheckoutInfo
     */
    public function handle()
    {
        $orderId = $this->order->getId();

        $this->checkoutInfo->setOrderId($orderId);
    }
}