<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\OrderTotals;
use ProBiz\Payment\PaymentMethodFactory;
use ProBiz\Payment\PaymentPlatformFactory;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Contracts\CheckoutInfo;

class FreeOrder
{
    /**
     * @var CheckoutInfo
     */
    private $checkoutInfo;

    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * FreeOrder constructor.
     *
     * @param OrderInstance $order
     * @param CheckoutInfo $checkoutInfo
     */
    public function __construct(OrderInstance $order, CheckoutInfo $checkoutInfo)
    {
        $this->order = $order;
        $this->checkoutInfo = $checkoutInfo;
    }

    /**
     * Overrides the payment method
     */
    public function handle()
    {
        if ($this->order->totals->getTotal(OrderTotals::TYPE_GRAND, OrderTotals::TAX_INC, null, 2) != 0.00) {
            return;
        }

        $this->checkoutInfo->setPaymentPlatformId(PaymentPlatformFactory::PSP_VIRTUAL);
        $this->checkoutInfo->setPaymentMethodId(PaymentMethodFactory::METHOD_VIRTUAL_FREE);
    }
}
