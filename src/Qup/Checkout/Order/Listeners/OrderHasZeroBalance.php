<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\OrderTotals;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\OrderHasZeroBalanceException;

class OrderHasZeroBalance
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderNotEmpty constructor.
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * Handles the event
     */
    public function handle()
    {
        $order = $this->order;

        if (round($order->totals->getBalance(OrderTotals::BALANCE_ORDER), 2) == 0.00) {
            return;
        }

        throw new OrderHasZeroBalanceException(
            sprintf('The order does not have zero balance (Triggered by Event; %s)', get_class($this))
        );
    }
}