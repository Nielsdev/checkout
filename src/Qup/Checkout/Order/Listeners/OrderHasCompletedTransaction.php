<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\Transaction\Transaction;
use ProBiz\Order\Transaction\TransactionStatus;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

class OrderHasCompletedTransaction
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderCreateTransaction constructor.
     *
     * @param OrderInstance $order
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * @throws NotInPrerequisiteStateException
     */
    public function handle()
    {
        $requirements = [];
        $requirements[] = ['getType', Transaction::TYPE_DEBIT];
        $requirements[] = ['getStatus', TransactionStatus::getCompletedStatuses()];

        $completedTransactions = $this->order->transactions->getAllWithAll($requirements, true);

        if (count($completedTransactions)) {
            return;
        }

        throw new NotInPrerequisiteStateException('Order has no completed transactions. (Triggered by Event; '.get_class($this).')');
    }
}