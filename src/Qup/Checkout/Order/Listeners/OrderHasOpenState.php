<?php

namespace Qup\Checkout\Order\Listeners;

use ProBiz\Order\OrderStatus;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;

class OrderHasOpenState
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * OrderNotEmpty constructor.
     */
    public function __construct(OrderInstance $order)
    {
        $this->order = $order;
    }

    /**
     * Handles the event
     */
    public function handle()
    {
        $order = $this->order;

        if ($order instanceof \ProBiz\Order\OrderOpen && $order->getStatus(OrderStatus::TYPE_ORDER) == OrderStatus::OPEN) {
            return;
        }

        throw new NotInPrerequisiteStateException(
            sprintf('The order is not in open state (Triggered by Event; %s)', get_class($this))
        );
    }
}