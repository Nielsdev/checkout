<?php

namespace Qup\Checkout\Order;

use \ProBiz\Order\OrderStatus;
use ProBiz\Order\ShoppingCart;
use ProBiz\Order\OrderBase;

/**
 * Order Factory
 */
abstract class OrderFactory
{
    /**
     * @var array order status to class mapping
     */
    private static $aStatusClassMap = [
        OrderStatus::CART=>'\ProBiz\Order\ShoppingCart',
        OrderStatus::CREATED=>'\ProBiz\Order\OrderNew',
        OrderStatus::FAILED=>'\ProBiz\Order\OrderFailed',
        OrderStatus::OPEN=>'\ProBiz\Order\OrderOpen',
        OrderStatus::SUCCEEDED=>'\ProBiz\Order\OrderSucceeded',
    ];

    /**
     * Create an OrderBase instance of the supplied orderstatus.
     * 
     * @param integer $iStatus
     * @return OrderBase
     */
    public static function createFromOrderStatus($iStatus)
    {
        // Determine classname
        $sClassName = self::getClassForStatus($iStatus);

        // Create class
        $oReflect = new \ReflectionClass($sClassName);
        return $oReflect->newInstance();
    }

    /**
     * Get the class for the supplied order status.
     * 
     * @param integer $iStatus
     * @return string Classname
     * @throws \InvalidArgumentException
     */
    public static function getClassForStatus($iStatus)
    {
        // Validate type
        if (!isset(self::$aStatusClassMap[$iStatus])) {
            throw new \InvalidArgumentException('Unknown statustype '.$iStatus);
        }

        return self::$aStatusClassMap[$iStatus];
    }

    /**
     * Creates a new instance of Order
     * 
     * @return ShoppingCart
     */
    public static function createNew()
    {
        $order = self::createFromOrderStatus(OrderStatus::CART);
        $order->save();

        return $order;
    }

    /**
     * Binds supplied order to the ServiceContainer
     * 
     * @param OrderBase $order
     */
    public static function bindOrder(OrderBase $order)
    {
        
    }
}