<?php

namespace Qup\Checkout\Listeners;

use ProBiz\Core\EmailAddressBase;
use ProBiz\IO\Mail\Subscription\LogMutation;
use ProBiz\IO\Mail\Subscription\SubscriptionHandler;
use ProBiz\Order\OrderInfo;
use Qup\Checkout\Models\Email;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Repositories\CheckoutSubscriptionRepository;

/**
 * Class HandleEmailSubscriptions
 * @package Qup\Checkout\Listeners
 */
class HandleEmailSubscriptions
{
    /**
     * @var CheckoutSubscriptionRepository
     */
    private $checkoutSubscriptions;
    /**
     * @var OrderInfo
     */
    private $orderInfo;

    /**
     * HandleEmailSubscriptions constructor.
     *
     * @param CheckoutSubscriptionRepository $checkoutSubscriptions
     * @param OrderInstance $order
     */
    public function __construct(CheckoutSubscriptionRepository $checkoutSubscriptions, OrderInstance $order)
    {
        $this->checkoutSubscriptions = $checkoutSubscriptions;
        $this->orderInfo = $order->orderInfo;
    }

    /**
     * Capture the customer's email address and store their chosen subscriptions
     */
    public function handle()
    {
        $emailAddress = $this->orderInfo->getBillingEmail();

        if (empty($emailAddress)) {
            return;
        }

        $confirmedSubscriptionIds = $this->checkoutSubscriptions->getSubscribedSubscriptions();
        if (empty($confirmedSubscriptionIds)) {
            return;
        }

        $email = Email::firstOrCreate([
            'email'        => $emailAddress,
            'site_id'      => $this->orderInfo->getSiteID(),
            'origin_id'    => EmailAddressBase::ORIGIN_SHOP,
            'bounce_count' => 0,
            'options'      => 0]);
        $this->handleEmailSubscriptions($email->id, $confirmedSubscriptionIds);
    }

    /**
     * @param integer $emailId
     * @param integer[] $confirmedSubscriptionIds
     */
    private function handleEmailSubscriptions($emailId, $confirmedSubscriptionIds)
    {
        foreach ($confirmedSubscriptionIds as $subscriptionId) {
            SubscriptionHandler::subscribeEmailAddressTo($emailId, $subscriptionId, LogMutation::SOURCE_CHECKOUT);
        }
    }
}