<?php

namespace Qup\Checkout\Listeners;

use ProBiz\Order\OrderLine;
use ProBiz\Order\OrderLineHandler;
use Qup\Checkout\Catalog\Contracts\CartCatalog;
use Qup\Checkout\Events\CatalogChanged;
use Qup\Checkout\Order\Contracts\OrderInstance;

class RemoveUnrelatedProductsFromOrder
{
    /**
     * @var OrderInstance
     */
    private $order;

    /**
     * @var CartCatalog
     */
    private $catalog;

    /**
     * RemoveUnrelatedProductsFromOrder constructor.
     *
     * @param OrderInstance $order
     */
    public function __construct(OrderInstance $order, CartCatalog $catalog)
    {
        $this->order = $order;
        $this->catalog = $catalog;
    }

    /**
     * Handles the catalogChanged event
     */
    public function handle()
    {
        $this->handleOrderLinesRemoval();
    }

    /**
     * Handles the orderLines removal.
     *
     * @param $catalog
     */
    private function handleOrderLinesRemoval()
    {
        $cartProducts = $this->catalog->cartProducts();

        $productIds = [];
        foreach ($cartProducts as $cartProduct) {
            $productIds[] = $cartProduct->id;
        }

        $removalLines = $this->getOrderLineRemovalIDs($productIds);

        $this->releaseOrderLines($removalLines);
    }

    /**
     * Returns the effective product lines of this order.
     *
     * @param array $catalogIds
     * @return array
     */
    private function getOrderLineRemovalIDs($catalogIds)
    {
        $orderLines = $this->order->orderLines;
        $productLines = $orderLines->getAllWith('getType', OrderLine::TYPE_PRODUCT);

        $removalLines = [];
        foreach ($productLines as $productLine) {
            // Inactive lines are no bother to us.
            if (!OrderLineHandler::isEffectivelyActive($productLine, $orderLines)) {
                continue;
            }

            // The following lines are still valid for this context.
            if (in_array($productLine->getDateTimeRankSubProductId(), $catalogIds)) {
                continue;
            }

            // Now, if we get here, we know the line needs to die
            $removalLines   = array_merge($removalLines, $orderLines->getAllWith('getAppliedToOrderLineID', $productLine->getID()));
            $removalLines   = array_merge($removalLines, $orderLines->getAllWith('getParentOrderLineID', $productLine->getID()));
            $removalLines[] = $productLine;
        }

        return $removalLines;
    }

    /**
     * Releases and removes supplied orderLines
     *
     * @param $orderLineIDs
     */
    private function releaseOrderLines($removalLines)
    {
        $orderLines = $this->order->orderLines;
        foreach ($removalLines as $orderLine) {
            OrderLineHandler::handleOrderLineRelease($orderLine);

            $orderLines->remove($orderLine);
        }
    }
}