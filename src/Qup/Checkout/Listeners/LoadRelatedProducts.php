<?php

namespace Qup\Checkout\Listeners;

use MrTicket\Product\AvailabilityCheck;
use MrTicket\Product\ShopLocationMainProduct;
use Qup\Checkout\Events\ShopLocationMainProductsLoaded;

class LoadRelatedProducts
{
    /**
     * @var AvailabilityCheck
     */
    private $availabilityCheck;

    /**
     * Creates a new instance
     *
     * @param AvailabilityCheck $availabilityCheck
     */
    public function __construct(AvailabilityCheck $availabilityCheck)
    {
        $this->availabilityCheck = $availabilityCheck;
    }

    /**
     * Handles the related products upon catalog shopLocationMainProduct add
     */
    public function handle(ShopLocationMainProductsLoaded $trigger)
    {
        $collection = $trigger->collection;

        foreach ($collection as $shopLocationMainProduct)
        {
            foreach ($shopLocationMainProduct->relatedProducts as $relatedProduct) {
                $shopLocationMainProduct = new ShopLocationMainProduct();
                $shopLocationMainProduct->loadFromModel($relatedProduct);

                if (!$this->availabilityCheck->isShopLocationMainProductAvailable($shopLocationMainProduct)) {
                    continue;
                }

                $relatedProducts[$relatedProduct->id] = $relatedProduct;
                $collection->add($relatedProduct);
            }
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(\Qup\Checkout\Events\ShopLocationMainProductsLoaded::class, '\Qup\Checkout\Listeners\LoadRelatedProducts@handle');
    }
}