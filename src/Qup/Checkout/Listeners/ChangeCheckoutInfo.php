<?php

namespace Qup\Checkout\Listeners;

use Qup\Checkout\Events\CatalogChanged;
use Qup\Checkout\CheckoutInfo;
use \Qup\Checkout\Contracts as Instances;

class ChangeCheckoutInfo
{
    /**
     * @var CheckoutInfo
     */
    private $checkoutInfo;

    /**
     * Creates an instance of ChangeOrderInfo
     * @param \Qup\Checkout\Contracts\CheckoutInfo $checkoutInfo
     */
    public function __construct(Instances\CheckoutInfo $checkoutInfo)
    {
        $this->checkoutInfo = $checkoutInfo;
    }

    /**
     * Handles the event
     *
     * @param CatalogChanged
     * @todo Make more abstract to recycle this Listener
     */
    public function handle(CatalogChanged $event)
    {
        $checkoutInfo = $this->checkoutInfo;
        $catalog = $event->catalog;

        $checkoutInfo
            ->setVisitDate($catalog->getVisitDate())
            ->setVisitTime($catalog->getVisitTime())
            ->setShopLocationMainProductIds($catalog->getShopLocationMainProductIds())
        ;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(\Qup\Checkout\Events\CatalogChanged::class, '\Qup\Checkout\Listeners\ChangeCheckoutInfo@handle');
    }
}