<?php
/**
 * Order subscription handler
 *
 * @package Checkout\Listeners
 * @version P3
 * @copyright Copyright (c) 2016 MrTicket - Amsterdam {@link mailto:info@mrticket.nl info@mrticket.nl}
 * @author Martijn Polder ({@link mailto:martijn@queueup.eu martijn@queueup.eu})
 */

namespace Qup\Checkout\Listeners;

use ProBiz\IO\Mail\Subscription\LogMutation;
use ProBiz\IO\Mail\Subscription\SubscriptionHandler;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Repositories\CheckoutSubscriptionRepository;

/**
 * Class HandleOrderSubscriptions
 */
class HandleOrderSubscriptions
{
    /**
     * @var CheckoutSubscriptionRepository
     */
    private $checkoutSubscriptions;

    /**
     * @var integer
     */
    private $orderId;

    /**
     * HandleOrderSubscriptions constructor.
     * @param CheckoutSubscriptionRepository $checkoutSubscriptions
     * @param OrderInstance $order
     */
    public function __construct(CheckoutSubscriptionRepository $checkoutSubscriptions, OrderInstance $order)
    {
        $this->checkoutSubscriptions = $checkoutSubscriptions;
        $this->orderId = $order->getID();
    }

    /**
     * Store the subscriptions subscribed to
     */
    public function handle()
    {
        $confirmedSubscriptionIds = $this->checkoutSubscriptions->getSubscribedSubscriptions();

        $this->handleOrderSubscriptions($this->orderId, $confirmedSubscriptionIds);
    }

    /**
     * @param int $orderId
     * @param int[] $confirmedSubscriptionIds
     */
    private function handleOrderSubscriptions($orderId, $confirmedSubscriptionIds)
    {
        foreach ($confirmedSubscriptionIds as $subscriptionId) {
            SubscriptionHandler::subscribeOrderTo($orderId, $subscriptionId, LogMutation::SOURCE_CHECKOUT);
        }
    }
}