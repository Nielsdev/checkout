<?php
/**
 * Created by IntelliJ IDEA.
 * User: sansing
 * Date: 2/9/17
 * Time: 11:50 AM
 */

namespace Qup\Checkout\Listeners;

use ProBiz\Locale\Language;
use ProBiz\Order\OrderBase;
use Qup\MailClient\Request\MailMessage;
use Symfony\Component\Process\Exception\InvalidArgumentException;

class SendMailBuilder
{
    private $site;

    private $type;
    private $locale;
    private $fromAddress = '';
    private $fromName = '';

    private $recipientAddress;
    private $recipientName = '';

    private $subject;
    private $placeholders;

    /**
     * Use the values from the order to fill in most of the mail
     * parameters
     *
     * @param $order OrderBase
     * @return $this
     */
    public function useOrder($order)
    {
        $billing = $order->orderInfo;

        if ($billing->getBillingLangID()) {
            $lang = new Language();
            $lang->load($billing->getBillingLangID());
            $this->locale = $lang->getCode() . '.' . $lang->getCharSet();
        }

        $this->recipientAddress = $billing->getBillingEmail();
        $this->recipientName = $billing->getBillingConstructedFullName();

        return $this;
    }

    /**
     * If the order does not contain a language ID, use the
     * following default language.
     *
     * @param $locale
     * @return $this
     */
    public function useDefaultLanguage($locale)
    {
        if (!$this->locale) {
            $this->locale = $locale;
        }

        return $this;
    }

    /**
     * Sets the sender information uses the translation file
     * to determine the actual information
     *
     * @param $address string Translation key for the sender address
     * @param $name string Translation key for the sender name
     * @return $this
     */
    public function useSender($address, $name)
    {
        $this->fromAddress = trans($address, [], 'messages', $this->locale);
        $this->fromName = trans($name, [], 'messages', $this->locale);

        return $this;
    }

    /**
     * Set the site ID for the mail message
     *
     * @param $site
     * @return $this
     */
    public function useSite($site)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * Use this mail template
     *
     * @param $template
     * @return $this
     */
    public function useTemplate($template)
    {
        $this->type = $template;
        return $this;
    }

    /**
     * Use the following mail template, is translated using Symfony's trans function
     *
     * @param $subjectTemplate
     * @return $this
     */
    public function useSubject($subjectTemplate)
    {
        $this->subject = trans($subjectTemplate, [], 'messages', $this->locale);
        return $this;
    }

    /**
     * Set the following placeholders for the mail template
     *
     * @param $placeholders
     * @return $this
     */
    public function usePlaceHolders($placeholders)
    {
        $this->placeholders = $placeholders;
        return $this;
    }

    /**
     * Constuct the SendMail message
     *
     * @return MailMessage
     */
    public function build()
    {
        return new MailMessage(
            $this->type,
            $this->site,
            $this->locale,
            $this->subject,
            $this->placeholders,
            $this->recipientAddress,
            $this->recipientName,
            $this->fromAddress,
            $this->fromName
        );
    }
}