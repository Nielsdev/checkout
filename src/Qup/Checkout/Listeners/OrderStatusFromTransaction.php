<?php

namespace Qup\Checkout\Listeners;

use Illuminate\Container\Container;
use ProBiz\Order\OrderNew;
use ProBiz\Order\ShoppingCart;
use ProBiz\Order\Transaction\TransactionStatus;
use Qup\Checkout\Events\OrderStatusChangeAfterTransactionFailed;
use \Qup\Checkout\Events\TransactionStatusChanged;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Order\Exceptions\NotInPrerequisiteStateException;
use Qup\Checkout\Order\Contracts;
use Illuminate\Events\Dispatcher;
use Qup\Checkout\Order\Exceptions\OrderHasZeroBalanceException;

class OrderStatusFromTransaction
{
    /**
     * @var Container
     */
    private $serviceContainer;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * OrderStatusFromTransaction constructor.
     *
     * @param Container $serviceContainer
     */
    public function __construct(Container $serviceContainer, Dispatcher $dispatcher)
    {
        $this->serviceContainer = $serviceContainer;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Handles the event
     *
     * @param TransactionStatusChanged $event
     */
    public function handle(TransactionStatusChanged $event)
    {
        if (in_array($event->transaction->getStatus(), TransactionStatus::getSucceededStatuses())) {
            try {
                $this->serviceContainer->call([$this, 'handleOrder']);
            }
            catch (OrderHasZeroBalanceException $zbEx) {
                $this->dispatcher->fire(new OrderStatusChangeAfterTransactionFailed($zbEx, $event->order, $event->transaction));
            }
            catch (NotInPrerequisiteStateException $ex) {
                \Log::debug($ex);
            }
            catch (\Exception $ex) {
                \Log::error($ex);
            }
        }

        if (in_array($event->transaction->getStatus(), TransactionStatus::getFailedStatuses())) {
            // We don't handle this, conversion in a later point should handle failed.
            // It could be that the customer simply "returns" to the overview.
            // Then the order should still be available for him.
        }
    }

    /**
     * Attempts to handle the order.
     *
     * @param OrderInstance $order
     * @param Container $container
     * @throws NotInPrerequisiteStateException
     * @return void
     */
    public function handleOrder(OrderInstance $order, Container $container)
    {
        if ($order instanceof ShoppingCart) {
            $resolver = $container->make(Contracts\OrderNew::class);
            $order = $resolver->instance();
        }

        if ($order instanceof OrderNew) {
            $container->make(Contracts\OrderOpen::class);
        }

        $container->make(Contracts\OrderSucceeded::class);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(TransactionStatusChanged::class, '\Qup\Checkout\Listeners\OrderStatusFromTransaction@handle');
    }
}