<?php

namespace Qup\Checkout\Listeners;

use \Qup\Checkout\Events\CatalogChanged;
use \Qup\Checkout\CheckoutInfo;

class ChangePaymentMethods
{
    /**
     * @var CheckoutInfo
     */
    private $checkoutInfo;

    /**
     * Creates an instance of ChangePaymentMethods
     */
    public function __construct(Instances\CheckoutInfo $checkoutInfo)
    {
        $this->checkoutInfo = $checkoutInfo;
    }

    /**
     * Banana banana banana
     * @todo 
     * @param CatalogChanged
     */
    public function handle(CatalogChanged $event)
    {
        // This hook will handle conditional payment methods
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(\Qup\Checkout\Events\CatalogChanged::class, '\Qup\Checkout\Listeners\ChangePaymentMethods@handle');
    }
}