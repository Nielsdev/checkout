<?php

namespace Qup\Checkout\Listeners;

use MrTicket\Product\AvailabilityCheck;
use Illuminate\Database\Eloquent\Collection;
use MrTicket\Product\ShopLocationMainProduct;
use MrTicket\Product\DateTimeRankSubProduct;
use Qup\Checkout\Events\ShopLocationMainProductsLoaded;
use Qup\Checkout\Events\DateTimeRankSubProductsLoaded;

class ProductAvailabilityChecker
{
    /**
     * @var AvailabilityCheck
     */
    private $availabilityCheck;

    /**
     * Creates a new instance of AvailabilityChecker
     * 
     * @param AvailabilityCheck $availabilityCheck
     */
    public function __construct(AvailabilityCheck $availabilityCheck)
    {
        $this->availabilityCheck = $availabilityCheck;
    }

    /**
     * Validates supplied collection of ShopLocationMainProducts
     * 
     * @param ObjectCollection $collection
     */
    public function handleShopLocationMainProducts(ShopLocationMainProductsLoaded $trigger)
    {
        $collection = $trigger->collection;

        $availabilityCheck = $this->availabilityCheck;

        foreach ($collection as $key => $shopLocationMainProductEloquent) {
            $shopLocationMainProduct = new ShopLocationMainProduct();
            $shopLocationMainProduct->loadFromModel($shopLocationMainProductEloquent);

            if (!$availabilityCheck->isShopLocationMainProductAvailable($shopLocationMainProduct)) {
                $collection->forget($key);
            }
        }
    }

    /**
     * Validates supplied collection of DateTimeRankSubProducts
     * 
     * @param ObjectCollection $collection
     */
    public function handleDateTimeRankSubProducts(DateTimeRankSubProductsLoaded $trigger)
    {
        $collection = $trigger->collection;

        $availabilityCheck = $this->availabilityCheck;

        foreach ($collection as $key => $dateTimeRankSubProductEloquent) {

            $dateTimeRankSubProduct = new DateTimeRankSubProduct();
            $dateTimeRankSubProduct->loadFromModel($dateTimeRankSubProductEloquent);

            //if (!$availabilityCheck->isDateTimeRankSubProductAvailable($dateTimeRankSubProduct)) {
            //    $collection->forget($key);
            //}
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(\Qup\Checkout\Events\ShopLocationMainProductsLoaded::class, '\Qup\Checkout\Listeners\ProductAvailabilityChecker@handleShopLocationMainProducts');
        $events->listen(\Qup\Checkout\Events\DateTimeRankSubProductsLoaded::class, '\Qup\Checkout\Listeners\ProductAvailabilityChecker@handleDateTimeRankSubProducts');
    }
}