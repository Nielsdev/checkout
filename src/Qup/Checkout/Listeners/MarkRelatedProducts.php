<?php

namespace Qup\Checkout\Listeners;

use MrTicket\Product\AvailabilityCheck;
use MrTicket\Product\ShopLocationMainProduct;
use ProBiz\Cache\ObjectCacheController;
use Qup\Checkout\Events\DateTimeRankSubProductsLoaded;

class MarkRelatedProducts
{
    /**
     * Creates a new instance
    public function __construct()
    {

    }

    /**
     * Marks sub products as "related"
     */
    public function handle(DateTimeRankSubProductsLoaded $trigger)
    {
        $collection = $trigger->collection;

        foreach ($collection as $dateTimeRankSubProduct)
        {
            $mainProductId = $dateTimeRankSubProduct->shop_location_main_product_id;
            $mainProduct = ObjectCacheController::get(ObjectCacheController::ITEM_SHOPLOCATIONMAINPRODUCT, $mainProductId);

            if ($mainProduct->optionsBits->checkOptions(ShopLocationMainProduct::OPTION_CROSSSELL)) {
                $dateTimeRankSubProduct->related = true;
            }
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(\Qup\Checkout\Events\DateTimeRankSubProductsLoaded::class, '\Qup\Checkout\Listeners\MarkRelatedProducts@handle');
    }
}