<?php

namespace Qup\Checkout\Listeners;

use Illuminate\Events\Dispatcher;
use ProBiz\Order\OrderNew;
use ProBiz\Order\OrderOpen;
use ProBiz\Order\OrderSucceeded;
use ProBiz\Order\ShoppingCart;
use ProBiz\Order\Transaction\TransactionSucceeded;
use Qup\Checkout\Events\OrderStatusChangeAfterTransactionFailed;
use Qup\Checkout\Order\Contracts\OrderInstance;
use Qup\Checkout\Repositories\TransactionRepository;

class RefundOrder
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * RefundTransaction constructor.
     *
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Triggers when the order status changed fails, after a successful transaction.
     *
     * @param OrderStatusChangeAfterTransactionFailed $orderStatusChangeAfterTransactionFailed
     */
    public function handle(OrderStatusChangeAfterTransactionFailed $orderStatusChangeAfterTransactionFailed)
    {
        $order = $orderStatusChangeAfterTransactionFailed->order;

        $this->validateOrder($order);

        $transaction = $orderStatusChangeAfterTransactionFailed->transaction;

        if ($transaction instanceof TransactionSucceeded) {
            try {
                $this->transactionRepository->refundTransaction($order, $transaction);
            }
            catch (\RuntimeException $ex) {
                \Log::error('Unable to refund transaction: ' . $ex->getMessage());
            }
        }

        // Fail the order
        $this->failOrder($order);
    }

    /**
     * Fails the supplied order.
     *
     * @param OrderInstance $order
     */
    protected function failOrder(OrderInstance $order)
    {
        if ($order instanceof OrderOpen || $order instanceof OrderNew || $order instanceof ShoppingCart) {
            $order->abandon('Automatically abandoned by RefundOrder');
        }
    }

    /**
     * Returns whether or not the order is valid for this event.
     *
     * @param OrderInstance $order
     * @throws \InvalidArgumentException
     */
    protected function validateOrder(OrderInstance $order)
    {
        if ($order instanceof OrderSucceeded) {
            throw new \InvalidArgumentException('Supplied order status is succeeded, will not refund.');
        }

        if (round($order->getGrandTotal(), 2) <= 0) {
            throw new \InvalidArgumentException('Order does not have a balance to refund');
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        // load the PaymentPlatforms first!
        $events->listen(OrderStatusChangeAfterTransactionFailed::class, RefundOrder::class . '@handle');
    }
}
