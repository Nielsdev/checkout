<?php

namespace Qup\Checkout\Listeners;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use ProBiz\Core\Site\SubSiteFactory;
use ProBiz\Order\OrderBase;
use Qup\Checkout\Events\OrderStatusChangeAfterTransactionFailed;
use Qup\MailClient\Request\MailMessage;
use Qup\MailClient\Request\MailRequest;
use Qup\Queue\Dispatcher as QupDispatcher;

class OrderStatusFailureMailNotifications
{
    const MAIL_TYPE = MailMessage::TYPE_TRANSACTION_INSUFFICIENT;
    const MAIL_SUBJECT = 'mail.order.transaction_insufficient.subject';

    private $dispatcher;
    const MAIL_FROM_ADDRESS = 'mail.order.transaction_insufficient.from.address';
    const MAIL_FROM_NAME = 'mail.order.transaction_insufficient.from.name';

    /**
     * OrderStatusFailureMailNotifications constructor.
     */
    public function __construct()
    {

    }

    /**
     * Handles a case where the amount of a received transaction does not match the
     * total amount on the order (so the balance is not zero), and sends a email to
     * the receiver of the order telling him that the order will be cancelled.
     *
     * @param OrderStatusChangeAfterTransactionFailed $event
     */
    public function handle(OrderStatusChangeAfterTransactionFailed $event)
    {
        Log::info("Changing order status failed for order: " . $event->order->getID() . ", and transaction: " . $event->transaction->getID());

        /**
         * @var $order OrderBase
         */
        $order = $event->order;


        $fromAddress = trans(self::MAIL_FROM_ADDRESS);
        $fromName = trans(self::MAIL_FROM_NAME);

        /**
         * @todo fix this.
         */
        if (empty($this->dispatcher)) {
            $this->dispatcher = \App::make(QupDispatcher::class);
        }


        $builder = new SendMailBuilder();
        $mail = $builder->useDefaultLanguage(App::getLocale())
                        ->useSender($fromAddress, $fromName)
                        ->useOrder($order)
                        ->useSite(SubSiteFactory::getActiveSiteCode())
                        ->useTemplate(self::MAIL_TYPE)
                        ->useSubject(self::MAIL_SUBJECT)
                        ->usePlaceHolders([
                            'ordernaw_billing_fullname' => $order->orderInfo->getBillingConstructedFullName(false),
                            'email' => $fromAddress,
                            'frontend_id' => $order->getFrontendID(),
                            'phone' => trans('mail.order.transaction_insufficient.phone'),
                            'assets_url' => config('mail.assets_base_url')
                        ])
                        ->build();

        $this->dispatcher->dispatch($mail, MailRequest::CHANNEL_SEND_MAIL);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(OrderStatusChangeAfterTransactionFailed::class, '\Qup\Checkout\Listeners\OrderStatusFailureMailNotifications@handle');
    }
}