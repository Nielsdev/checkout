<?php

namespace Qup\Checkout\Listeners;

use Qup\Checkout\Events\CartContentsChanged;
use \Qup\Checkout\Events\CheckoutInfoChanged;
use ProBiz\Order\OrderLineHandler;

class HandleInsuranceCosts
{
    /**
     * Handles the event
     *
     * @param CatalogChanged
     * @todo Make more abstract to recycle this Listener
     */
    public function handle($event)
    {
        $info = $event->checkoutInfo;
        $cart = $event->order;

        $insured = $info->getInsurance();

        OrderLineHandler::handleInsuranceCosts($cart, $insured);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(\Qup\Checkout\Events\CheckoutInfoChanged::class, '\Qup\Checkout\Listeners\HandleInsuranceCosts@handle');
        $events->listen(\Qup\Checkout\Events\CartContentsChanged::class, '\Qup\Checkout\Listeners\HandleInsuranceCosts@handle');
    }
}