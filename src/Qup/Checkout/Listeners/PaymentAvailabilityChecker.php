<?php


namespace Qup\Checkout\Listeners;

use ProBiz\Cache\ObjectCacheController;
use ProBiz\Core\Setting\SettingsFactory;
use ProBiz\Payment\PaymentMethodSettings;
use ProBiz\Payment\PaymentPlatformFactory;
use ProBiz\Payment\PaymentPlatformSettings;
use Qup\Checkout\Events\PaymentMethodsLoaded;
use Qup\Checkout\Events\PaymentPlatformsLoaded;

class PaymentAvailabilityChecker
{
    /**
     * Handles availability check for payment methods.
     *
     * @param PaymentMethodsLoaded
     */
    public function handlePaymentMethodAvailability(PaymentMethodsLoaded $paymentMethodsLoaded)
    {
        $methods = $paymentMethodsLoaded->methods;
        $methodSettings = ObjectCacheController::get(ObjectCacheController::ITEM_SETTINGS, SettingsFactory::TYPE_PMETHOD);

        foreach ($methods as $method) {
            $available = $methodSettings->getValueIfSet(PaymentMethodSettings::ITEM_ACTIVE, PaymentMethodSettings::getCategoryForMethod($method->getMethod()), false);

            if (!$available) {
                continue;
            }

            $visible = $methodSettings->getValueIfSet(PaymentMethodSettings::ITEM_ACTIVE, PaymentMethodSettings::getCategoryForMethod($method->getMethod()), false);

            if (!$visible) {
                continue;
            }

            $paymentMethodsLoaded->collection->add($method);
        }
    }

    /**
     * Handles availability check for payment platforms.
     *
     * @param PaymentPlatformsLoaded
     */
    public function handlePaymentPlatformAvailability(PaymentPlatformsLoaded $paymentPlatformsLoaded)
    {
        $platforms = $paymentPlatformsLoaded->platforms;
        $platformSettings = ObjectCacheController::get(ObjectCacheController::ITEM_SETTINGS, SettingsFactory::TYPE_PPLATFORM);

        foreach ($platforms as $platform) {
            $available = $platformSettings->getValueIfSet(PaymentPlatformSettings::ITEM_ACTIVE, PaymentPlatformSettings::getCategoryForPSP($platform->psp), false);

            if (!$available) {
                continue;
            }

            // load the ProBiz PaymentPlatformImplementation
            $paymentPlatformsLoaded->collection->add(ObjectCacheController::get(ObjectCacheController::ITEM_PAYMENT_PLATFORM, $platform->psp));
        }
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        // load the PaymentPlatforms first!
        $events->listen(\Qup\Checkout\Events\PaymentPlatformsLoaded::class, '\Qup\Checkout\Listeners\PaymentAvailabilityChecker@handlePaymentPlatformAvailability');
        $events->listen(\Qup\Checkout\Events\PaymentMethodsLoaded::class, '\Qup\Checkout\Listeners\PaymentAvailabilityChecker@handlePaymentMethodAvailability');
    }
}