<?php

namespace Qup\Checkout\Facades;

use Illuminate\Support\Facades\Facade;

class CartCatalog extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'catalog';
    }
}