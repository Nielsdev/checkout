|  branch | quality |
|-----|:-----:|
|[Master](https://bitbucket.org/qup/checkout/branch/master)|[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/qup/checkout/badges/quality-score.png?b=master&s=e9f8f73aec990f0c418b725993e37519f46a9a1c)](https://scrutinizer-ci.com/b/qup/checkout/?branch=master)|
|[Develop](https://bitbucket.org/qup/checkout/branch/develop)|[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/qup/checkout/badges/quality-score.png?b=develop&s=e9f8f73aec990f0c418b725993e37519f46a9a1c)](https://scrutinizer-ci.com/b/qup/checkout/?branch=develop)|

# INSTALL #
To install Checkout to your queueup project please add the following to your composer.json;

```
#!json
{
  "require-dev": {
    "qup/checkout": "dev-master"
  },
  "repositories": [
    {
      "type": "vcs",
      "url":  "git@bitbucket.org:qup/checkout.git"
    }
  ]
}
```


# USAGE #

Extend the available controllers to add checkout functionality.

# Running Tests #

 To run the tests, use the phpunit executable provided in the vendor folder:

    $ ./vendor/bin/phpunit ./tests/

 To be able to run the tests you will need to have a running instance of
 `jumbo_dagjeuit_api` on your local machine (preferably using port 8080).
 You can also configure the target host by changing the `TEST_TARGET_HOST`
 environment variable to point to your desired target host.
