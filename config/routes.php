<?php

Route::group(['prefix' => 'cartcatalog/'], function ($app) {
    Route::get('/product/{product_id}', [
    'uses' => '\\' . Qup\Checkout\Controllers\CartCatalogController::class . '@getCartCatalog'
    ])->where(['product_id' => '[0-9]+']);

    Route::get('/product/{product_id}/locale/{locale}', [
    'uses' => '\\' . Qup\Checkout\Controllers\CartCatalogController::class . '@getCartCatalog'
    ])->where(['product_id' => '[0-9]+']);

    Route::get('/product/{product_id}/date/{date}', [
    'uses' => '\\' . Qup\Checkout\Controllers\CartCatalogController::class . '@getCartCatalog'
    ])->where(['product_id' => '[0-9]+']);

    Route::get('/product/{product_id}/date/{date}/locale/{locale}', [
    'uses' => '\\' . Qup\Checkout\Controllers\CartCatalogController::class . '@getCartCatalog'
    ])->where(['product_id' => '[0-9]+']);

    Route::get('/product/{product_id}/date/{date}/time/{time}', [
    'uses' => '\\' . Qup\Checkout\Controllers\CartCatalogController::class . '@getCartCatalog'
    ])->where(['product_id' => '[0-9]+']);

    Route::get('/product/{product_id}/date/{date}/time/{time}/locale/{locale}', [
    'uses' => '\\' . Qup\Checkout\Controllers\CartCatalogController::class . '@getCartCatalog'
    ])->where(['product_id' => '[0-9]+']);
});